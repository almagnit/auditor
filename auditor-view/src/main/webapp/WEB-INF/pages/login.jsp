<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>auditorGC</title>
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="span4 offset4 well" style = "margin-top: 15%;">
<%--
            <legend style="text-align: center">auditor</legend>
--%>
<%--
            <div class="alert alert-error">
                <a class="close" data-dismiss="alert" href="#">×</a>Incorrect Username or Password!
            </div>
--%>
            <form method="POST" action="j_spring_security_check" accept-charset="UTF-8">
                <input type="text" id="username" class="span4" name="j_username" placeholder="Username" value="">
                <input type="password" id="password" class="span4" name="j_password" placeholder="Password" value="">
<%--
                <label class="checkbox">
                    <input type="checkbox" name="remember" value="1"> Remember Me
                </label>
--%>
                <button type="submit" name="submit" class="btn btn-info btn-block">Sign in</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>