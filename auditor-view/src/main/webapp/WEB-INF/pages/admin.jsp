<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
    <meta http-equiv="X-UA-Compatible" charset="UTF-8">
    <meta name='gwt:property' content='locale=ru'>
    <title>auditorGC</title>
    <style>
        a {
            font-size: smaller;
            color: blue;
            text-decoration: none;
        }
    </style>
    <script type="text/javascript" language="javascript" src="resources/gwt/admin/org.auditor.gwt.admin.Admin.nocache.js"></script>
    <script type="text/javascript" language="javascript" src="js/ZeroClipboard.js"></script>
</head>
<body>
<iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1' style="position:absolute;width:0;height:0;border:0"></iframe>

<!-- RECOMMENDED if your web app will not function without JavaScript enabled -->
<noscript>
    <div style="width: 22em; position: absolute; left: 50%; margin-left: -11em; color: red; background-color: white; border: 1px solid red; padding: 4px; font-family: sans-serif">
        Your web browser must have JavaScript enabled
        in order for this application to display correctly.
    </div>
</noscript>

</body>
</html>
