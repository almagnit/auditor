package org.auditor.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author almagnit@gmail.com
 *
 */


@Controller
public class CommonController {

    @RequestMapping("/")
    public ModelAndView common(HttpServletRequest req, HttpServletResponse resp){
        if(req.isUserInRole("ROLE_ADMIN") || req.isUserInRole("ROLE_MONITOR"))return new ModelAndView("admin.jsp");
        return new ModelAndView("login.jsp");
    }

    @Secured(value = {"ROLE_ADMIN", "ROLE_MONITOR"})
    @RequestMapping("/monitoring")
    public String  monitor(HttpServletRequest req, HttpServletResponse resp){
        return "/monitoring";
    }

    @RequestMapping("/login")
    public ModelAndView login(HttpServletRequest req, HttpServletResponse resp){
        return new ModelAndView("login.jsp");
    }

    @Secured(value = {"ROLE_ADMIN", "ROLE_MONITOR"})
    @RequestMapping("/logout")
    public ModelAndView logout(HttpServletRequest req, HttpServletResponse resp){
        SecurityContextHolder.getContext().setAuthentication(null);
        return new ModelAndView("login.jsp");
    }

    @Secured(value = {"ROLE_ADMIN", "ROLE_MONITOR"})
    @RequestMapping("/admin")
    public ModelAndView admin(HttpServletRequest req, HttpServletResponse resp){
        return new ModelAndView("admin.jsp");
    }



}
