package org.auditor.gwt.admin.client.view.widget.editor.security;

import org.auditor.gwt.admin.client.view.widget.editor.AbstractEditor;
import org.auditor.gwt.admin.shared.proxy.security.UserProxy;
import org.auditor.gwt.admin.shared.request.security.UserRequestFactory;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.web.bindery.requestfactory.gwt.client.RequestFactoryEditorDriver;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;

/**
 * User: almagnit
 */

public class UserEditor extends AbstractEditor<UserProxy, UserEditor, UserRequestFactory> {
    public interface UserDriver extends RequestFactoryEditorDriver<UserProxy, UserEditor> {}


    @Path("username")
    TextField username = new TextField();
    @Path("password")
    PasswordField password = new PasswordField();
    @Path("enabled")
    CheckBox enabled = new CheckBox();
    @Path("authority.authority")
    TextField authorityTitle = new TextField();
    @Path("authority.username")
    TextField authorityUsername = username;

    public UserEditor() {
        initEditor();
    }

    @Override
    protected void initRequestFactory() {
        service = GWT.create(UserRequestFactory.class);
        driver = GWT.create(UserDriver.class);
        service.initialize(new SimpleEventBus(), transport);
        driver.initialize(service, this);
    }

    @Override
    protected void initUI() {
        addField(createFieldLabel(username, "Username"));
        addField(createFieldLabel(password, "Password"));
        addField(createFieldLabel(authorityTitle, "Authority"));
        addField(createFieldLabel(enabled, "Enabled"));
    }

    @Override
    protected void receiveData(){
    }

    @Override
    protected void initHandlers() {

    }

    @Override
    protected void initValidators() {

    }

    @Override
    public void clear() {

    }
}
