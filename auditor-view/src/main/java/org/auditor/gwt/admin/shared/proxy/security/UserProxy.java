package org.auditor.gwt.admin.shared.proxy.security;

import org.auditor.entity.security.User;
import org.auditor.gwt.admin.server.GWTSpringEntityLocator;
import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;

/**
 * @author almagnit@gmail.com
 *
 */

@ProxyFor(value = User.class, locator = GWTSpringEntityLocator.class)
public interface UserProxy extends CommonProxy {

    public String getUsername();
    public void setUsername(String username);

    public String getPassword();
    public void setPassword(String username);

    public Boolean getEnabled();
    public void setEnabled(Boolean enabled);

    public AuthorityProxy getAuthority();
    public void setAuthority(AuthorityProxy authority);
}

