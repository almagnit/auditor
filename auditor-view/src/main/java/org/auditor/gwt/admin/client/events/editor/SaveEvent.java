package org.auditor.gwt.admin.client.events.editor;

import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import com.google.gwt.event.shared.GwtEvent;

/**
 * @author almagnit@gmail.com
 */

public class SaveEvent<T extends CommonProxy> extends GwtEvent<SaveEventHandler>{

    public static Type<SaveEventHandler> TYPE = new Type<SaveEventHandler>();

    T entity;

    public SaveEvent(T entity) {
        this.entity = entity;
    }

    @Override
    public Type<SaveEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(SaveEventHandler handler) {
        handler.onSave(this);
    }

    public T getEntity() {
        return entity;
    }
}