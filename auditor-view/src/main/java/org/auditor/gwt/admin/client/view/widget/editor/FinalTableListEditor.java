package org.auditor.gwt.admin.client.view.widget.editor;

//import com.github.gwtbootstrap.client.ui.Heading;
//import com.github.gwtbootstrap.client.ui.Paragraph;
import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.adapters.EditorSource;
import com.google.gwt.editor.client.adapters.ListEditor;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.user.client.ui.*;
import com.sencha.gxt.widget.core.client.form.NumberField;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor;
import com.sencha.gxt.widget.core.client.form.TextField;
import org.auditor.gwt.admin.shared.proxy.FinalTableProxy;

import java.util.ArrayList;
import java.util.List;


public class FinalTableListEditor extends TableListEditor<FinalTableProxy, FinalTableListEditor.FinalTableItemEditor> {

    private ListEditor<FinalTableProxy,FinalTableItemEditor> listEditor = ListEditor.of(new ItemEditorSource());
    private List<Integer> rowsTable = new ArrayList<Integer>();
    private String[] lesson = new String[] {"1 пара", "2 пара", "3 пара", "4 пара", "5 пара"};
    AppResources appResources = GWT.create(AppResources.class);

    @Override
    public ListEditor<FinalTableProxy, FinalTableItemEditor> asEditor() {
        return listEditor;
    }

    public void init(){
        flexTable.clear();
        flexTable.removeAllRows();
        rowsTable.clear();
        initFlexTable();
    }

    public FinalTableListEditor(){
        super(5);
        appResources.flexTableStyles().ensureInjected();
        initFlexTable();
    }

    private void initFlexTable(){
        flexTable.setText(0, 0, " ");
        flexTable.getFlexCellFormatter().setColSpan(0, 0, 5);
        flexTable.setStyleName(appResources.flexTableStyles().table());
        for(int i=0; i<5; i++){
            flexTable.getFlexCellFormatter().setStyleName(1, i, appResources.flexTableStyles().header());
            flexTable.setWidget(1,i,new Label(lesson[i]));
        }
        for(int i=0; i<5; i++){
            rowsTable.add(2);
        }

    }

    @Override
    protected FinalTableItemEditor createItemEditor(int index) {
        FinalTableItemEditor itemEditor = new FinalTableItemEditor();
        return itemEditor;
    }

    private class ItemEditorSource extends EditorSource<FinalTableItemEditor> {

        @Override
        public FinalTableItemEditor create(final int index) {

            FinalTableItemEditor item = new FinalTableItemEditor();

            if(listEditor != null || listEditor.getList().size() != 0){
                for(int i=0; i<5; i++){
                    if(listEditor.getList().get(index).getLesson() == i+1){
                        flexTable.getFlexCellFormatter().setStyleName(rowsTable.get(i), i, appResources.flexTableStyles().cell());
                        VerticalPanel verticalPanel = new VerticalPanel();
                        Label teacher = new Label(listEditor.getList().get(index).getTeacher().getName());
                        Label audience = new Label(listEditor.getList().get(index).getAudience().getNameAudience());
                        Label group = new Label(listEditor.getList().get(index).getGroup().getGroupName());
                        teacher.setStyleName(appResources.flexTableStyles().teacher());
                        audience.setStyleName(appResources.flexTableStyles().audience());
                        group.setStyleName(appResources.flexTableStyles().group());
                        verticalPanel.add(teacher);
                        verticalPanel.add(audience);
                        verticalPanel.add(group);
                        flexTable.setWidget(rowsTable.get(i), i, verticalPanel);

                        rowsTable.set(i, rowsTable.get(i) + 1);

                        break;
                    }
                }
            }
            return item;
        }

        @Override
        public void setIndex(FinalTableItemEditor item, int index) {
            container.insert(item, index);
        }

    }

    protected class FinalTableItemEditor implements Editor<FinalTableProxy>, IsWidget {

        HorizontalPanel horizontalPanel = new HorizontalPanel();

        NumberField<Integer> lesson =
                new NumberField<Integer>(new NumberPropertyEditor.IntegerPropertyEditor());

        @Path(value = "group.groupName")
        TextField groupName = new TextField();

        @Editor.Path(value = "teacher.name")
        TextField teacherName = new TextField();

        @Path(value = "audience.numberAudience")
        NumberField<Integer> audienceNumber =
                new NumberField<Integer>(new NumberPropertyEditor.IntegerPropertyEditor());

        NumberField<Integer> priority =
                new NumberField<Integer>(new NumberPropertyEditor.IntegerPropertyEditor());

        public FinalTableItemEditor(){
            horizontalPanel.add(new Label("Пара "));

            horizontalPanel.add(lesson);
            horizontalPanel.add(new Label("Группа "));
            horizontalPanel.add(groupName);
            horizontalPanel.add(new Label("Преподаватель "));
            horizontalPanel.add(teacherName);
            horizontalPanel.add(new Label("Аудитория "));
            horizontalPanel.add(audienceNumber);
            horizontalPanel.add(new Label("Приоритет "));
            horizontalPanel.add(priority);

        }

        @Override
        public Widget asWidget() {
            return horizontalPanel;
        }
    }

    public interface AppResources extends ClientBundle {
        public interface FlexTableCssResource extends CssResource {
            String table();
            String header();
            String cell();
            String teacher();
            String audience();
            String group();
        }

        @Source("org/auditor/gwt/admin/client/css/style.css")
        public FlexTableCssResource flexTableStyles();
    }
}
