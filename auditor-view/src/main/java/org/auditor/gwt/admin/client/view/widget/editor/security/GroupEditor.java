package org.auditor.gwt.admin.client.view.widget.editor.security;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.web.bindery.requestfactory.gwt.client.RequestFactoryEditorDriver;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.form.TextField;
import org.auditor.gwt.admin.client.events.editor.DeleteEvent;
import org.auditor.gwt.admin.client.view.widget.editor.AbstractEditor;
import org.auditor.gwt.admin.shared.proxy.security.GroupProxy;
import org.auditor.gwt.admin.shared.request.security.GroupRequestFactory;

public class GroupEditor extends AbstractEditor<GroupProxy, GroupEditor, GroupRequestFactory> {

    public interface GroupDriver extends RequestFactoryEditorDriver<GroupProxy, GroupEditor> {
    }

    @Path("groupName")
    TextField groupName = new TextField();

    public GroupEditor() {
        initEditor();
    }


    @Override
    public void delete(GroupProxy entity) {
        if (entity.getTimetables().size() != 0) {
            new AlertMessageBox("Невозможно удалить", "Группа находится в расписании").show();
        } else {
            if (entity.getReplacements().size() != 0) {
                new AlertMessageBox("Невозможно удалить", "Группа находится в заменах").show();
            } else {
                updateContext();
                editedEntity = entity;
                context.delete(editedEntity).fire(new Receiver<Void>() {
                    @Override
                    public void onSuccess(Void response) {
                        fireEvent(new DeleteEvent(editedEntity));
                        editedEntity = null;
                    }
                });
            }
        }
    }

    @Override
    protected void initRequestFactory() {
        service = GWT.create(GroupRequestFactory.class);
        driver = GWT.create(GroupDriver.class);
        service.initialize(new SimpleEventBus(), transport);
        driver.initialize(service, this);
    }

    @Override
    protected void initUI() {
        addField(createFieldLabel(groupName, "Группа"));
    }

    @Override
    protected void receiveData() {

    }

    @Override
    protected void initHandlers() {

    }

    @Override
    protected void initValidators() {

    }

    @Override
    public void clear() {

    }
}
