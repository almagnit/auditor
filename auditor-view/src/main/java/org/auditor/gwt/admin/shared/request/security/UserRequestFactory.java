package org.auditor.gwt.admin.shared.request.security;

import org.auditor.gwt.admin.server.GWTSpringServiceLocator;
import org.auditor.gwt.admin.shared.proxy.security.UserPagingProxy;
import org.auditor.gwt.admin.shared.proxy.security.UserProxy;
import org.auditor.gwt.admin.shared.request.CommonRequestContext;
import org.auditor.gwt.admin.shared.request.CommonRequestFactory;
import org.auditor.service.security.UserService;
import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;

import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */
public interface UserRequestFactory extends CommonRequestFactory {

    @Service(value = UserService.class, locator = GWTSpringServiceLocator.class)
    public interface UserRequest extends CommonRequestContext<UserProxy> {
        Request<UserPagingProxy> list(int offset, int limit, List<? extends FilterConfig> filterConfig, List<? extends SortInfo> sortInfo);
        Request<List<UserProxy>> list();
        Request<UserProxy> update(UserProxy entity);
        Request<UserProxy> add(UserProxy entity);
        Request<UserProxy> create();
        Request<UserProxy> find(String value);
        Request<UserProxy> find(Long value);
        Request<Void> delete(UserProxy entity);
        Request<Void> delete(Long id);
    }

    UserRequest context();
}
