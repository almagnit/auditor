package org.auditor.gwt.admin.client.view.widget.section.security;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.*;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.LiveGridView;
import com.sencha.gxt.widget.core.client.grid.filters.GridFilters;
import org.auditor.gwt.admin.client.property.security.GroupProperties;
import org.auditor.gwt.admin.client.view.widget.editor.security.GroupEditor;
import org.auditor.gwt.admin.client.view.widget.section.SimpleSection;
import org.auditor.gwt.admin.shared.proxy.security.GroupProxy;
import org.auditor.gwt.admin.shared.request.security.GroupRequestFactory;

import java.util.ArrayList;
import java.util.List;

public class GroupSection implements IsWidget {

    Grid<GroupProxy> contentGrid = null;
    GroupProperties properties = GWT.create(GroupProperties.class);
    List<ColumnConfig<GroupProxy, ?>> columns =
            new ArrayList<ColumnConfig<GroupProxy, ?>>();
    GroupEditor editor = new GroupEditor();
    SimpleSection<GroupProxy, GroupEditor> groupSection =
            new SimpleSection<GroupProxy, GroupEditor>(editor);

    public GroupSection(){
        initGrid();
    }

    private void initGrid(){
        columns.add(new ColumnConfig<GroupProxy, String>(properties.groupName(), 100, "Группа"));

        RequestFactoryProxy<FilterPagingLoadConfig, PagingLoadResult<GroupProxy>> proxy =
                new RequestFactoryProxy<FilterPagingLoadConfig, PagingLoadResult<GroupProxy>>() {
                    @Override
                    public void load(FilterPagingLoadConfig loadConfig, Receiver<? super PagingLoadResult<GroupProxy>> receiver) {
                        GroupRequestFactory.GroupRequest req = editor.getService().context();
                        List<SortInfo> sortInfo = createRequestSortInfo(req, loadConfig.getSortInfo());
                        List<FilterConfig> filterConfig = createRequestFilterConfig(req, loadConfig.getFilters());
                        req.list(loadConfig.getOffset(), loadConfig.getLimit(), filterConfig, sortInfo).to(receiver);
                        req.fire();
                    }
                };

        final PagingLoader<FilterPagingLoadConfig, PagingLoadResult<GroupProxy>> loader =
                new PagingLoader<FilterPagingLoadConfig, PagingLoadResult<GroupProxy>>(proxy) {
                    @Override
                    protected FilterPagingLoadConfig newLoadConfig() {
                        return new FilterPagingLoadConfigBean();
                    }
                };

        contentGrid = new Grid<GroupProxy>(new ListStore<GroupProxy>(properties.key()), new ColumnModel<GroupProxy>(columns)) {
            @Override
            protected void onAfterFirstAttach() {
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        loader.load(0, ((LiveGridView)contentGrid.getView()).getCacheSize());
                    }
                });
            }
        };

        loader.setRemoteSort(true);
        loader.addLoadHandler(new LoadResultListStoreBinding<FilterPagingLoadConfig, GroupProxy, PagingLoadResult<GroupProxy>>(contentGrid.getStore()));
        GridFilters<GroupProxy> filters = new GridFilters<GroupProxy>(loader);
        filters.initPlugin(contentGrid);
        contentGrid.setLoader(loader);
        groupSection.setColumns(columns);
        groupSection.setContentGrid(contentGrid);
    }

    @Override
    public Widget asWidget() {
        return groupSection.asWidget();
    }
}
