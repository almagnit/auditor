package org.auditor.gwt.admin.client.view.widget.editor;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * @author almagnit@gmail.com
 */

public interface CommonEditor<T> extends Editor<T>, IsWidget {

    public T create();

    public void save();

    public void update();

    public void delete(T entity);

    public void clear();

}
