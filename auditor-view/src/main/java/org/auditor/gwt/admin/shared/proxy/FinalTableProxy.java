package org.auditor.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.auditor.entity.FinalTable;
import org.auditor.gwt.admin.server.GWTSpringEntityLocator;
import org.auditor.gwt.admin.shared.proxy.security.GroupProxy;

@ProxyFor(value = FinalTable.class, locator = GWTSpringEntityLocator.class)
public interface FinalTableProxy extends CommonProxy {

    public Integer getLesson();
    public void setLesson(Integer lesson);

    public GroupProxy getGroup();
    public void setGroup(GroupProxy group);

    public TeacherProxy getTeacher();
    public void setTeacher(TeacherProxy teacher);

    public AudienceProxy getAudience();
    public void setAudience(AudienceProxy audience);

    public Integer getPriority();
    public void setPriority(Integer priority);

    public CalendarProxy getCalendar();
    public void setCalendar(CalendarProxy calendar);


}
