package org.auditor.gwt.admin.server;

import com.google.web.bindery.requestfactory.server.ExceptionHandler;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: almagnit
 * Date: 06.11.13
 * Time: 13:24
 * To change this template use File | Settings | File Templates.
 */
public class LoquaciousExceptionHandler implements ExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(LoquaciousExceptionHandler.class);

    @Override
    public ServerFailure createServerFailure( Throwable throwable ) {
        LOG.error( "Server error", throwable );
        throwable.printStackTrace();
        return new ServerFailure( throwable.getMessage(), throwable.getClass().getName(), null, true );
    }


}