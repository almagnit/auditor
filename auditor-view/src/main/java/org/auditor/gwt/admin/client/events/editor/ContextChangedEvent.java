package org.auditor.gwt.admin.client.events.editor;

import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import org.auditor.gwt.admin.shared.request.CommonRequestContext;
import com.google.gwt.event.shared.GwtEvent;

/**
 * @author almagnit@gmail.com
 */

public class ContextChangedEvent<T extends CommonProxy> extends GwtEvent<ContextChangedEventHandler>{

    public static Type<ContextChangedEventHandler> TYPE = new Type<ContextChangedEventHandler>();

    protected CommonRequestContext<T> context;

    public ContextChangedEvent(CommonRequestContext<T> context) {
        this.context = context;
    }

    @Override
    public Type<ContextChangedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(ContextChangedEventHandler handler) {
        handler.beforeContextChanged(this);
    }

    public CommonRequestContext<T> getContext() {
        return context;
    }
}