package org.auditor.gwt.admin.client.events.editor;

import com.google.gwt.event.shared.GwtEvent;

/**
 * @author almagnit@gmail.com
 */

public class BeforeSaveEvent extends GwtEvent<BeforeSaveEventHandler>{

    public static Type<BeforeSaveEventHandler> TYPE = new Type<BeforeSaveEventHandler>();

    @Override
    public Type<BeforeSaveEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(BeforeSaveEventHandler handler) {
        handler.beforeSave(this);
    }
}