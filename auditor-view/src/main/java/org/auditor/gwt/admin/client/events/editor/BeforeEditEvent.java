package org.auditor.gwt.admin.client.events.editor;

import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import com.google.gwt.event.shared.GwtEvent;

/**
 * @author almagnit@gmail.com
 */

public class BeforeEditEvent<T extends CommonProxy> extends GwtEvent<BeforeEditEventHandler>{

    public static Type<BeforeEditEventHandler> TYPE = new Type<BeforeEditEventHandler>();

    T entity;

    public BeforeEditEvent(T entity) {
        this.entity = entity;
    }

    @Override
    public Type<BeforeEditEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(BeforeEditEventHandler handler) {
        handler.onBeforeEdit(this);
    }

    public T getEntity() {
        return entity;
    }
}