package org.auditor.gwt.admin.client.property.security;


import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import org.auditor.gwt.admin.shared.proxy.TimetableProxy;

public interface TimetableProperties extends PropertyAccess<TimetableProxy> {

    @Editor.Path("id")
    com.sencha.gxt.data.shared.ModelKeyProvider<TimetableProxy> key();

    @Editor.Path("id")
    ValueProvider<TimetableProxy, Long> id();

    @Editor.Path("teacher.name")
    ValueProvider<TimetableProxy, String> teacherName();

    @Editor.Path("lesson")
    ValueProvider<TimetableProxy, Integer> lesson();

    @Editor.Path("day")
    ValueProvider<TimetableProxy, Integer> day();

    @Editor.Path("subject")
    ValueProvider<TimetableProxy, String> subject();

    @Editor.Path("group.groupName")
    ValueProvider<TimetableProxy, String> groupName();


}
