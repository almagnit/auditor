package org.auditor.gwt.admin.client.presenter;

import org.auditor.gwt.admin.client.AdminEventBus;
import org.auditor.gwt.admin.client.view.Root;
import com.google.gwt.user.client.ui.IsWidget;
import com.mvp4g.client.annotation.Presenter;
import com.mvp4g.client.presenter.BasePresenter;

/**
 * @author almagnit@gmail.com
 *
 */


@Presenter( view = Root.class )
public class RootPresenter extends BasePresenter<RootPresenter.RootInterface, AdminEventBus> {

    public void onStart(){
        view.init();
    }

    public void onChangeNavigatePanel(IsWidget navigatePanel){
        view.setNavigatePanel(navigatePanel);
    }

    public void onChangeDetailsPanel(IsWidget navigatePanel){
        view.setNavigatePanel(navigatePanel);
    }

    public void onChangeContentPanel(IsWidget contentPanel){
        view.setContentPanel(contentPanel);
    }

    public static interface RootInterface extends IsWidget {

        public void init();
        public void setNavigatePanel(IsWidget navigatePanel);
        public void setDetailsPanel(IsWidget detailsPanel);
        public void setContentPanel(IsWidget contentPanel);
    }
}
