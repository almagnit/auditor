package org.auditor.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import org.auditor.gwt.admin.server.GWTSpringServiceLocator;
import org.auditor.gwt.admin.shared.proxy.CalendarPagingProxy;
import org.auditor.gwt.admin.shared.proxy.CalendarProxy;
import org.auditor.service.CalendarService;

import java.util.List;

public interface CalendarRequestFactory extends CommonRequestFactory {

    @Service(value = CalendarService.class, locator = GWTSpringServiceLocator.class)
    public interface CalendarRequest extends CommonRequestContext<CalendarProxy>{
        Request<CalendarPagingProxy> list(int offset, int limit, List<? extends FilterConfig> filterConfig, List<? extends SortInfo> sortInfo);
        Request<List<CalendarProxy>> list();
        Request<CalendarProxy> update(CalendarProxy entity);
        Request<CalendarProxy> add(CalendarProxy entity);
        Request<CalendarProxy> create();
        Request<CalendarProxy> find(String value);
        Request<CalendarProxy> find(Long value);
        Request<Void> delete(CalendarProxy entity);
        Request<Void> delete(Long id);
    }

    CalendarRequest context();

}
