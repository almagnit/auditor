package org.auditor.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.auditor.entity.Teacher;
import org.auditor.gwt.admin.server.GWTSpringEntityLocator;

import java.util.List;

/**
 * Created by almagnit on 15.01.14.
 */

@ProxyFor(value = Teacher.class, locator = GWTSpringEntityLocator.class)
public interface TeacherProxy extends CommonProxy {

    public String getName();
    public void setName(String name);

    public List<PriorityProxy> getPriorities();
    public void setPriorities(List<PriorityProxy> priorities);

    public List<TimetableProxy> getTimetables();
    public void setTimetables(List<TimetableProxy> timetables);

    public List<ReplacementProxy> getReplacements();
    public void setReplacements(List<ReplacementProxy> replacements);

}
