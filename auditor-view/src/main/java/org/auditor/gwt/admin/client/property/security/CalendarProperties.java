package org.auditor.gwt.admin.client.property.security;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import org.auditor.gwt.admin.shared.proxy.CalendarProxy;

import java.util.Date;

/**
 * Created by almagnit on 21.02.14.
 */
public interface CalendarProperties  extends CommonProperties<CalendarProxy> {



    LabelProvider<CalendarProxy> dateLabel = new LabelProvider<CalendarProxy>() {
        @Override
        public String getLabel(CalendarProxy item) {
            return item.getDate().getDate()+". "+item.getDate().getMonth()+". "+ (1900+item.getDate().getYear());
        }
    };


    @Editor.Path("date")
    ValueProvider<CalendarProxy, Date> date();

}
