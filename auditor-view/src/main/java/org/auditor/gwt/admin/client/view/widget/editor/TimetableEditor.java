package org.auditor.gwt.admin.client.view.widget.editor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.text.shared.AbstractSafeHtmlRenderer;
import com.google.gwt.user.client.Event;
import com.google.web.bindery.requestfactory.gwt.client.RequestFactoryEditorDriver;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.StringLabelProvider;
import com.sencha.gxt.widget.core.client.form.NumberField;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;
import org.auditor.gwt.admin.client.property.security.GroupProperties;
import org.auditor.gwt.admin.client.property.security.TeacherProperties;
import org.auditor.gwt.admin.client.templates.ComboBoxTemplates;
import org.auditor.gwt.admin.client.view.widget.component.ComboBox;
import org.auditor.gwt.admin.shared.proxy.TeacherProxy;
import org.auditor.gwt.admin.shared.proxy.TimetableProxy;
import org.auditor.gwt.admin.shared.proxy.security.GroupProxy;
import org.auditor.gwt.admin.shared.request.TeacherRequestFactory;
import org.auditor.gwt.admin.shared.request.TimetableRequestFactory;
import org.auditor.gwt.admin.shared.request.security.GroupRequestFactory;

import java.util.ArrayList;
import java.util.List;

public class TimetableEditor extends AbstractEditor<TimetableProxy, TimetableEditor, TimetableRequestFactory>  {

    public interface TimetableDriver extends RequestFactoryEditorDriver<TimetableProxy, TimetableEditor> {}

    @Path("lesson")
    NumberField<Integer> lesson = new NumberField<Integer>(new NumberPropertyEditor.IntegerPropertyEditor());

    @Path("day")
    NumberField<Integer> day = new NumberField<Integer>(new NumberPropertyEditor.IntegerPropertyEditor());

    @Path("group")
    ComboBox<GroupProxy> groups;

    @Path("teacher")
    ComboBox<TeacherProxy> teachers;

    @Path("subject")
    SimpleComboBox<String> subject = new SimpleComboBox<String>(new StringLabelProvider<String>()){
        @Override
        protected void onBlur(Event be) {
            String value = getCell().getText(getElement());
            super.onBlur(be);
            setValue(value);
        }
    };

    ArrayList<String> list = new ArrayList<String>();

    public TimetableEditor(){
        initEditor();
    }

    @Override
    public void clear() {

    }

    @Override
    protected void initRequestFactory() {
        service = GWT.create(TimetableRequestFactory.class);
        driver = GWT.create(TimetableDriver.class);
        service.initialize(new SimpleEventBus(), transport);
        driver.initialize(service, this);
    }

    @Override
    protected void initUI() {
        addField(createFieldLabel(teachers, "Преподаватель"));
        addField(createFieldLabel(lesson, "Пара"));
        addField(createFieldLabel(day, "День"));
        addField(createFieldLabel(subject, "Предмет"));
        addField(createFieldLabel(groups, "Группа"));
    }

    @Override
    protected void receiveData() {

        list.add("Англійська мова");
        list.add("Історія України");
        list.add("Основи патентознавства");
        list.add("Архітектура компютерів");
        list.add("Бази даних");
        list.add("Роз-ка застос.кл-серв.архіт.");
        list.add("Світова література");
        list.add("Укр.мова за проф.спрям");
        list.add("Українська мова");
        list.add("Основи інформатики");
        list.add("Астрономія");
        list.add("Фізика");
        list.add("Екологія");
        list.add("Прикладне програмне забезпечення");
        list.add("Основи програмної інженерії");
        list.add("Математичний аналіз");
        list.add("Математика");
        list.add("Українська література");
        list.add("Осн.констр і технологія вир");
        list.add("Інженерна та комп графіка");
        list.add("Комп’ютерна графіка");
        list.add("Контроль якості РЕА");
        list.add("Будова і обслуговування верстатів");
        list.add("Верстати з ПУ і РТК");
        list.add("ТКМ");
        list.add("Основи технології машинобудування");
        list.add("ООМ та інструмент");
        list.add("Економічний аналіз");
        list.add("Менеджмент");
        list.add("Економічна теорія");
        list.add("Маркетинг");
        list.add("Економіка промисловості");
        list.add("Економіка,орг.і план.в-ва");
        list.add("Організація комп’ютерних мереж");
        list.add("Основи інтернет");
        list.add("Інформаційні технології");
        list.add("ОС");
        list.add("Інтернет-технології");
        list.add("Будова та експлуат. ЕОМ");
        list.add("Нарисна геометрія");
        list.add("Електропривод");
        list.add("Будова систем ПУ");
        list.add("Осн.дискретної автоматики");
        list.add("Інформаційні устрої верстатів");
        list.add("Біологія");
        list.add("Хімія");
        list.add("компютерна схемотехніка");
        list.add("ЕОМ і мікропроцесори");
        list.add("САПР");
        list.add("Вища математика");
        list.add("Дискретна математика");
        list.add("Радіопередавальні пристрої");
        list.add("Технологія в-ва РЕА");
        list.add("Проектування радіопр. пристрої");
        list.add("Сигнали та процеси в РТ");
        list.add("Схемотехніка рад. пристроїв");
        list.add("Проектування РЕА");
        list.add("Основи ремонту РЕА");
        list.add("Обєктно орієн.програм");
        list.add("Основи программування");
        list.add("Фізичне виховання");
        list.add("Фізкультура і здоров’я");
        list.add("Захист вітчизни");
        list.add("Промислова електроніка");
        list.add("Основи теорії кіл");
        list.add("Матеріалознавство");
        list.add("Електронні прилади");
        list.add("Теор.осн.електротехніки");
        list.add("Фінансовий облік");
        list.add("Податкова система");
        list.add("Контроль і ревізія");
        list.add("Основи телебачення");
        list.add("Облік в бюдж.орган.");
        list.add("Інформаційні системи в обліку");
        list.add("Основи аудиту");
        list.add("WEB-технології");
        list.add("Конструювання програмного забезпечення");
        list.add("WEB-дизайн");
        list.add("Основи автоматизаціївиробництва і ПР");
        list.add("Технічна механіка");
        list.add("Соціологія");
        list.add("Всесвітня історія");
        list.add("Основи охорони праці");
        list.add("Безпека життєдіяльності");
        list.add("МСП");
        list.add("Ціноутворення");
        list.add("Основи підприємництва");
        list.add("Страхування");
        list.add("РПС");
        list.add("Фінанси підприємств");
        list.add("Економіка підприємств");
        subject.add(list);
        subject.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        subject.setTypeAhead(true);

        //Teacher ComboBox
        TeacherProperties properties = GWT.create(TeacherProperties.class);
        final ListStore<TeacherProxy> listTeachers = new ListStore<TeacherProxy>(properties.uniquekey);
        TeacherRequestFactory teacherService = GWT.create(TeacherRequestFactory.class);
        teacherService.initialize(new SimpleEventBus());
        teacherService.context().list().fire(new Receiver<List<TeacherProxy>>() {
            @Override
            public void onSuccess(List<TeacherProxy> response) {
                listTeachers.addAll(response);
            }
        });
        teachers = new ComboBox<TeacherProxy>(listTeachers, properties.nameLabel(), new AbstractSafeHtmlRenderer<TeacherProxy>() {
            @Override
            public SafeHtml render(TeacherProxy object) {
                final ComboBoxTemplates comboBoxTemplates = GWT.create(ComboBoxTemplates.class);
                return comboBoxTemplates.teacher(object.getName());
            }
        });
        teachers.setEmptyText("Выберите преподавателя ...");
        teachers.setWidth(150);
        teachers.setTypeAhead(true);
        teachers.setTriggerAction(ComboBoxCell.TriggerAction.ALL);

        //Group ComboBox
        GroupProperties groupProperties = GWT.create(GroupProperties.class);
        final ListStore<GroupProxy> listGroup = new ListStore<GroupProxy>(groupProperties.key());
        GroupRequestFactory groupService = GWT.create(GroupRequestFactory.class);
        groupService.initialize(new SimpleEventBus());
        groupService.context().list().fire(new Receiver<List<GroupProxy>>() {
            @Override
            public void onSuccess(List<GroupProxy> response) {
                listGroup.addAll(response);
            }
        });
        groups = new ComboBox<GroupProxy>(listGroup, groupProperties.nameLabel(), new AbstractSafeHtmlRenderer<GroupProxy>() {
            @Override
            public SafeHtml render(GroupProxy object) {
                final ComboBoxTemplates comboBoxGroupTemplates = GWT.create(ComboBoxTemplates.class);
                return comboBoxGroupTemplates.group(object.getGroupName());
            }
        });
        groups.setEmptyText("Выберите группу ...");
        groups.setWidth(150);
        groups.setTypeAhead(true);
        groups.setTriggerAction(ComboBoxCell.TriggerAction.ALL);

    }

    @Override
    protected void initHandlers() {

    }

    @Override
    protected void initValidators() {

    }
}
