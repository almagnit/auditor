package org.auditor.gwt.admin.client.view.widget.editor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.SimpleBeanEditorDriver;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.container.FlowLayoutContainer;
import org.auditor.gwt.admin.shared.proxy.CalendarProxy;


public class CalendarEditor implements IsWidget, Editor<CalendarProxy> {

    FlowLayoutContainer container = new FlowLayoutContainer();
    FinalTableListEditor finalTables = new FinalTableListEditor();

    interface CalendarDriver extends SimpleBeanEditorDriver<CalendarProxy, CalendarEditor>{}
    private static final CalendarDriver driver = GWT.create(CalendarDriver.class);

    public CalendarEditor(){
        container.add(finalTables);
        driver.initialize(this);
    }

    public void edit(CalendarProxy calendar) {
        finalTables.init();
        driver.edit(calendar);
    }

    @Override
    public Widget asWidget() {
        return container;
    }
}
