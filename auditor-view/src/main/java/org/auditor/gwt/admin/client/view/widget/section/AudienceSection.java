package org.auditor.gwt.admin.client.view.widget.section;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.text.shared.AbstractSafeHtmlRenderer;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.core.client.Style;
import com.sencha.gxt.core.client.dom.ScrollSupport;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.Store;
import com.sencha.gxt.data.shared.TreeStore;
import com.sencha.gxt.dnd.core.client.*;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.menu.Item;
import com.sencha.gxt.widget.core.client.menu.Menu;
import com.sencha.gxt.widget.core.client.menu.MenuItem;
import com.sencha.gxt.widget.core.client.tree.Tree;
import com.sencha.gxt.widget.core.client.treegrid.TreeGrid;
import org.auditor.gwt.admin.client.property.security.CommonProperties;
import org.auditor.gwt.admin.client.property.security.TeacherProperties;
import org.auditor.gwt.admin.client.templates.ComboBoxTemplates;
import org.auditor.gwt.admin.client.view.widget.component.ComboBox;
import org.auditor.gwt.admin.client.view.widget.editor.AudienceEditor;
import org.auditor.gwt.admin.shared.proxy.AudienceProxy;
import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import org.auditor.gwt.admin.shared.proxy.PriorityProxy;
import org.auditor.gwt.admin.shared.proxy.TeacherProxy;
import org.auditor.gwt.admin.shared.request.AudienceRequestFactory;
import org.auditor.gwt.admin.shared.request.TeacherRequestFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AudienceSection implements IsWidget {


    public static Integer LABEL_NORMAL_SIZE = 175;

    CommonProperties props = GWT.create(CommonProperties.class);
    VerticalLayoutContainer treeContainer = new VerticalLayoutContainer();
    List<ColumnConfig<CommonProxy, ?>> treeColumns = new LinkedList<ColumnConfig<CommonProxy, ?>>();
    ColumnModel<CommonProxy> treeColumnModel = new ColumnModel<CommonProxy>(treeColumns);
    TreeStore<CommonProxy> treeStore = new TreeStore<CommonProxy>(props.uniquekey);
    TreeGrid<CommonProxy> treeGrid;
    TreeGridDropTarget<CommonProxy> dndTarget;
    TreeGridDragSource<CommonProxy> dndSource;
    Menu contextMenu = new Menu();
    @Editor.Ignore
    Window editorWindowAudience = new Window();
    @Editor.Ignore
    Window editorWindowPriority = new Window();
    @Editor.Ignore
    com.sencha.gxt.widget.core.client.button.TextButton saveAudience =
            new com.sencha.gxt.widget.core.client.button.TextButton("Соханить");
    @Editor.Ignore
    com.sencha.gxt.widget.core.client.button.TextButton savePriority =
            new com.sencha.gxt.widget.core.client.button.TextButton("Соханить");
    AudienceEditor audienceEditor = new AudienceEditor();
    @Editor.Path("teacher")
    ComboBox<TeacherProxy> teachers;
    TeacherProperties teacherProperties = com.google.gwt.core.client.GWT.create(TeacherProperties.class);
    ListStore<TeacherProxy> teachersStore = new ListStore<TeacherProxy>(teacherProperties.uniquekey);
    TeacherRequestFactory teacherService = com.google.gwt.core.client.GWT.create(TeacherRequestFactory.class);

    @Editor.Ignore
    protected VerticalLayoutContainer fieldsPanel = new VerticalLayoutContainer();
    protected VerticalLayoutContainer.VerticalLayoutData verticalData = new VerticalLayoutContainer.VerticalLayoutData(-1d, -1d, new Margins(0, 0, 5, 0));

    public AudienceSection() {
        initTree();
        initHadlers();
        initComboBox();
        //Editor window audience
        editorWindowAudience.setBorders(true);
        editorWindowAudience.setModal(true);
        editorWindowAudience.addButton(saveAudience);
        editorWindowAudience.add(audienceEditor);
        //Editor Priority
        fieldsPanel.setAdjustForScroll(true);
        fieldsPanel.getScrollSupport().setScrollMode(ScrollSupport.ScrollMode.AUTOY);
        addField(createFieldLabel(teachers, "Преподаватели"));
        editorWindowPriority.setBorders(true);
        editorWindowPriority.setModal(true);
        editorWindowPriority.addButton(savePriority);
        editorWindowPriority.add(fieldsPanel);
    }

    private void initComboBox(){
        //Teacher ComboBox
        teacherService.initialize(new SimpleEventBus());
        teacherService.context().list().fire(new Receiver<List<TeacherProxy>>() {
            @Override
            public void onSuccess(List<TeacherProxy> response) {
                teachersStore.addAll(response);
            }
        });
        teachers = new ComboBox<TeacherProxy>(teachersStore, teacherProperties.nameLabel(), new AbstractSafeHtmlRenderer<TeacherProxy>() {
            @Override
            public SafeHtml render(TeacherProxy object) {
                final ComboBoxTemplates comboBoxTemplates = com.google.gwt.core.client.GWT.create(ComboBoxTemplates.class);
                return comboBoxTemplates.teacher(object.getName());
            }
        });
        teachersStore.addFilter(new Store.StoreFilter<TeacherProxy>() {
            @Override
            public boolean select(Store<TeacherProxy> store, TeacherProxy parent, TeacherProxy item) {
                CommonProxy select = treeGrid.getSelectionModel().getSelectedItem();
                if (select instanceof PriorityProxy) {
                    select = ((PriorityProxy) select).getAudience();
                }
                for (CommonProxy priority : treeStore.getChildren(select)) {
                    if (((PriorityProxy) priority).getTeacher().getId().equals(item.getId())) return false;
                }
                return true;
            }
        });
        teachers.setEmptyText("Выберите преподавателя ...");
        teachers.setWidth(150);
        teachers.setTypeAhead(true);
        teachers.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
    }

    public void initTree() {
        refreshTree();
        treeColumns.add(new ColumnConfig<CommonProxy, String>(props.label, 500));
        treeGrid = new TreeGrid<CommonProxy>(treeStore, treeColumnModel, treeColumns.get(0));
        treeGrid.getTreeView().setEmptyText("Добавьте аудитории");
        treeGrid.getSelectionModel().setSelectionMode(Style.SelectionMode.SINGLE);
        dndSource = new TreeGridDragSource<CommonProxy>(treeGrid);
        dndSource.setTreeGridSource(DND.TreeSource.LEAF);
        dndTarget = new TreeGridDropTarget<CommonProxy>(treeGrid);
        dndTarget.setAllowDropOnLeaf(false);
        dndTarget.setOperation(DND.Operation.MOVE);
        dndTarget.setFeedback(DND.Feedback.INSERT);
        dndTarget.setAllowSelfAsSource(true);
        treeContainer.add(treeGrid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        MenuItem addAudience = new MenuItem();
        addAudience.setText("Добавить аудиторию");
        addAudience.addSelectionHandler(new SelectionHandler<Item>() {
            @Override
            public void onSelection(SelectionEvent<Item> event) {
                audienceEditor.create();
                audienceEditor.clear();
                editorWindowAudience.show();
            }
        });
//        contextMenu.add(addAudience);
        MenuItem addPriority = new MenuItem();
        addPriority.setText("Добавить преподавателя");
        addPriority.addSelectionHandler(new SelectionHandler<Item>() {
            @Override
            public void onSelection(SelectionEvent<Item> event) {
                if(treeGrid.getSelectionModel().getSelectedItem() == null){
                    new MessageBox("", "Выделите аудиторию").show();
                }else{
                    teachersStore.setEnableFilters(false);
                    teachersStore.setEnableFilters(true);
                    editorWindowPriority.show();
                }
            }
        });
        contextMenu.add(addPriority);
        MenuItem delete = new MenuItem();
        delete.setText("Удалить преподавателя");
        delete.addSelectionHandler(new SelectionHandler<Item>() {
            @Override
            public void onSelection(SelectionEvent<Item> event) {
                CommonProxy select = treeGrid.getSelectionModel().getSelectedItem();
                if (select instanceof AudienceProxy) {
/*
                    audienceEditor.delete((AudienceProxy) select);
                    treeStore.removeChildren(select);
                    treeStore.remove(select);
*/
                } else {
                    AudienceProxy audienceProxy = ((AudienceProxy)treeStore.getParent(select));
                    ((AudienceRequestFactory.AudienceRequest)audienceEditor.getContext()).deletePriority((PriorityProxy) select).fire();
                    ((AudienceProxy)treeStore.getParent(select)).getPriorities().remove(select);
                    treeStore.remove(select);
                    for(PriorityProxy item : audienceProxy.getPriorities()){
                        updatePriority(item, 1000 - treeStore.indexOf(item));
                    }
                    audienceEditor.getContext().add(audienceProxy).fire();
                }
                treeGrid.getTreeView().refresh(true);
            }
        });
        contextMenu.add(delete);
        treeGrid.setContextMenu(contextMenu);
    }

    public void initHadlers() {
        dndTarget.addDragMoveHandler(new DndDragMoveEvent.DndDragMoveHandler() {
            @Override
            public void onDragMove(DndDragMoveEvent event) {
                CommonProxy node = ((TreeStore.TreeNode<CommonProxy>) ((ArrayList) event.getData()).get(0)).getData();
                if (!(node instanceof PriorityProxy)) {
                    event.setCancelled(true);
                } else {
                    Tree.TreeNode<CommonProxy> targetItem = dndTarget.getWidget().findNode(event.getDragMoveEvent().getNativeEvent().getEventTarget().<Element>cast());
                    if (targetItem == null || treeStore.getParent(targetItem.getModel()) == null) {
                        event.setCancelled(true);
                    } else {
                        event.setCancelled(!treeStore.getParent(node).getKey().equals(treeStore.getParent(targetItem.getModel()).getKey()));
                    }
                }
            }
        });
        dndTarget.addDragEnterHandler(new DndDragEnterEvent.DndDragEnterHandler() {
            @Override
            public void onDragEnter(DndDragEnterEvent event) {
                CommonProxy node = (((TreeStore.TreeNode<CommonProxy>) ((ArrayList) event.getDragSource().getData()).get(0)).getData());
                if (!(node instanceof PriorityProxy)) {
                    event.setCancelled(true);
                }
            }
        });
        dndTarget.addDropHandler(new DndDropEvent.DndDropHandler() {
            @Override
            public void onDrop(DndDropEvent event) {
                CommonProxy proxy = ((TreeStore.TreeNode<CommonProxy>) ((ArrayList) event.getData()).get(0)).getData();
                if(proxy instanceof PriorityProxy){
                    AudienceProxy audienceProxy = ((AudienceProxy)treeStore.getParent(proxy));
                    for(PriorityProxy item : audienceProxy.getPriorities()){
                        updatePriority(item, 1000 - treeStore.indexOf(item));
                    }
                        audienceEditor.getContext().add(audienceProxy).fire();
                }
            }
        });
        saveAudience.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                audienceEditor.save();
                refreshTree();
                editorWindowAudience.hide();
            }
        });

        savePriority.addSelectHandler(new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                int priority;
                AudienceRequestFactory.AudienceRequest context = (AudienceRequestFactory.AudienceRequest) audienceEditor.getContext();
                final PriorityProxy newPriority = context.create(PriorityProxy.class);
                final AudienceProxy audienceProxy;
                newPriority.setKey(Document.get().createUniqueId());
                newPriority.setTeacher(teachers.getCurrentValue());
                CommonProxy select = treeGrid.getSelectionModel().getSelectedItem();
                if (select instanceof AudienceProxy) {
                    audienceProxy = (AudienceProxy)select;
                    priority = 1000 - treeStore.getChildCount(select);
                } else {
                    audienceProxy = ((PriorityProxy)select).getAudience();
                    priority = 1000 - treeStore.getChildCount(treeStore.getParent(select));
                }
                newPriority.setAudience(audienceProxy);
                newPriority.setPriority(priority);
                newPriority.setKey(Document.get().createUniqueId());
                audienceProxy.getPriorities().add(newPriority);
                context.add(audienceProxy).fire(new Receiver<AudienceProxy>() {
                    @Override
                    public void onSuccess(AudienceProxy response) {
                        treeStore.add(audienceProxy, newPriority);
                    }
                });
                teachers.reset();
                editorWindowPriority.hide();
            }
        });

    }

    public void updatePriority(PriorityProxy priorityProxy, int priority){
        if(AutoBeanUtils.getAutoBean(priorityProxy).isFrozen()){
            GWT.log("frozen");
            priorityProxy = audienceEditor.getContext().edit(priorityProxy);
        }
        priorityProxy.setVersion((int) (Math.random() * 1000));
        priorityProxy.setPriority(priority + priorityProxy.getVersion());
    }

    public void loadTreeData(List<AudienceProxy> audienceList) {
        audienceEditor.updateContext();
        for (AudienceProxy audience : audienceList) {
            audience = audienceEditor.getContext().edit(audience);
            audience.setKey(Document.get().createUniqueId());
            treeStore.add(audience);
            if (audience.getPriorities().size() > 0) {
                for (PriorityProxy teacher : audience.getPriorities()) {
                    teacher = audienceEditor.getContext().edit(teacher);
                    teacher.setKey(Document.get().createUniqueId());
                    treeStore.add(audience, teacher);
                }
            }
        }
    }

    private void refreshTree() {
        treeStore.clear();
        audienceEditor.getContext().list().fire(new Receiver<List<AudienceProxy>>() {
            @Override
            public void onSuccess(List<AudienceProxy> response) {
                loadTreeData(response);
            }
        });
    }

    private void addField(FieldLabel field){
        field.setLabelWidth(LABEL_NORMAL_SIZE);
        fieldsPanel.add(field, verticalData);
    }

    private FieldLabel createFieldLabel(Widget field, String label){
        FieldLabel fieldLabel = new FieldLabel(field, label);
        fieldLabel.setLabelWidth(LABEL_NORMAL_SIZE);
        return fieldLabel;
    }

    @Override
    public Widget asWidget() {
        return treeContainer;
    }
}
