package org.auditor.gwt.admin.client.view.widget.editor;

import com.google.web.bindery.requestfactory.shared.impl.AbstractRequestContext;
import org.auditor.gwt.admin.client.events.editor.*;
import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import org.auditor.gwt.admin.shared.request.CommonRequestContext;
import org.auditor.gwt.admin.shared.request.CommonRequestFactory;
import org.auditor.gwt.admin.shared.request.CustomRequestTransport;
import com.google.gwt.editor.client.Editor;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.requestfactory.gwt.client.RequestFactoryEditorDriver;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.RequestTransport;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.sencha.gxt.core.client.dom.ScrollSupport;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.form.FieldLabel;

/**
 * @author almagnit@gmail.com
 */

public abstract class AbstractEditor<T extends CommonProxy, E extends Editor<? super T>, F extends CommonRequestFactory> implements CommonEditor<T>, HasHandlers {
    public static Integer LABEL_SHORT2_SIZE = 80;
    public static Integer FIELD_LONG2_SIZE = 250;
    public static Integer LABEL_SHORT1_SIZE = 100;
    public static Integer FIELD_LONG1_SIZE = 230;

    public static Integer LABEL_NORMAL_SIZE = 175;

    protected F service;
    protected CommonRequestContext<T> context;
    protected RequestFactoryEditorDriver<T, E> driver;
    protected RequestTransport transport = new CustomRequestTransport();
    protected T editedEntity;
    private HandlerManager handlerManager;

    @Editor.Ignore
    protected VerticalLayoutContainer fieldsPanel = new VerticalLayoutContainer();
    protected VerticalLayoutContainer.VerticalLayoutData verticalData = new VerticalLayoutContainer.VerticalLayoutData(-1d, -1d, new Margins(0, 0, 5, 0));


    protected AbstractEditor() {
        handlerManager = new HandlerManager(this);
        fieldsPanel.setAdjustForScroll(true);
        fieldsPanel.getScrollSupport().setScrollMode(ScrollSupport.ScrollMode.AUTOY);
    }

    protected void initEditor(){
        receiveData();
        initUI();
        initValidators();
        initHandlers();
        initRequestFactory();
    }

    abstract protected void initRequestFactory();
    abstract protected void initUI();
    abstract protected void receiveData();
    abstract protected void initHandlers();
    abstract protected void initValidators();
    protected boolean isValid(){
        return true;
    }

    @Override
    public void fireEvent(GwtEvent<?> event) {
        handlerManager.fireEvent(event);
    }

    public void addBeforeSaveEventHandler(BeforeSaveEventHandler handler){
        handlerManager.addHandler(BeforeSaveEvent.TYPE, handler);
    }

    public void addSaveEventHandler(SaveEventHandler handler){
        handlerManager.addHandler(SaveEvent.TYPE, handler);
    }

    public void addUpdateEventHandler(UpdateEventHandler handler){
        handlerManager.addHandler(UpdateEvent.TYPE, handler);
    }

    public void addCreateEventHandler(CreateEventHandler handler){
        handlerManager.addHandler(CreateEvent.TYPE, handler);
    }

    public void addBeforeEditEventHandler(BeforeEditEventHandler handler){
        handlerManager.addHandler(BeforeEditEvent.TYPE, handler);
    }

    public void addEditEventHandler(EditEventHandler handler){
        handlerManager.addHandler(EditEvent.TYPE, handler);
    }

    public void addDeleteEventHandler(DeleteEventHandler handler){
        handlerManager.addHandler(DeleteEvent.TYPE, handler);
    }

    public void addContextChangeHandler(ContextChangedEventHandler handler){
        handlerManager.addHandler(ContextChangedEvent.TYPE, handler);
    }

    @Override
    public Widget asWidget() {
        return fieldsPanel;
    }

    protected void addField(FieldLabel field){
        field.setLabelWidth(LABEL_NORMAL_SIZE);
        fieldsPanel.add(field, verticalData);
    }

    protected void addField(FieldLabel field, VerticalLayoutContainer panel){
        field.setLabelWidth(LABEL_NORMAL_SIZE);
        panel.add(field, verticalData);
    }

    protected void addField(FieldLabel field, Integer width){
        field.setLabelWidth(width);
        fieldsPanel.add(field, verticalData);
    }


    protected FieldLabel createFieldLabel(Widget field, String label){
        FieldLabel fieldLabel = new FieldLabel(field, label);
        fieldLabel.setLabelWidth(LABEL_NORMAL_SIZE);
        return fieldLabel;
    }

    protected FieldLabel createFieldLabel(Widget field, String label, Integer width){
        FieldLabel fieldLabel = new FieldLabel(field, label);
        fieldLabel.setLabelWidth(width);
        return fieldLabel;
    }

    protected void addWidget(Widget widget) {
        fieldsPanel.add(widget);
    }

    @Override
    public T create() {
        updateContext();

        service.context().create().fire(new Receiver<T>() {
            @Override
            public void onFailure(ServerFailure error) {
                new AlertMessageBox("CREATING ERROR", error.getMessage()).show();
            }

            @Override
            public void onSuccess(T response) {
                editedEntity = response;
                driver.edit(editedEntity, context);
                fireEvent(new CreateEvent<T>(response));
            }
        });
        return editedEntity;
    }

    public void edit(T entity){
        updateContext();
        editedEntity = entity;
        fireEvent(new BeforeEditEvent<T>(editedEntity));
        driver.edit(editedEntity, context);
        fireEvent(new EditEvent<T>(entity));
    }

    protected void unsaveAlert() {
        final AlertMessageBox alert = new AlertMessageBox("Внимание", "Необходимые поля не были заполнены");
        alert.addHideHandler(new HideEvent.HideHandler() {
            @Override
            public void onHide(HideEvent event) {
                alert.hide();
            }
        });
        alert.show();
    }

    public void save() {
        if (!isValid()) {
            unsaveAlert();
            return;
        }

        fireEvent(new BeforeSaveEvent());
        context = (CommonRequestContext) driver.flush();

        if (getContext().isChanged() && !driver.hasErrors()) {
            getContext().add(editedEntity).fire(new Receiver<T>() {
                @Override
                public void onFailure(ServerFailure error) {
                    new AlertMessageBox("SAVING ERROR", error.getMessage()).show();
                }
                @Override
                public void onSuccess(T response) {
                    editedEntity = response;
                    fireEvent(new SaveEvent<T>(editedEntity));
                }
            });
        } else {
            fireEvent(new SaveEvent<T>(null));
        }
    }

    public void update() {
        context = (CommonRequestContext) driver.flush();
        if (getContext().isChanged() && !driver.hasErrors()) {
            getContext().update(editedEntity).fire(new Receiver<T>() {
                @Override
                public void onFailure(ServerFailure error) {
                    new AlertMessageBox("SAVING ERROR", error.getMessage()).show();
                }
                @Override
                public void onSuccess(T response) {
                    editedEntity = response;
                    fireEvent(new UpdateEvent<T>(editedEntity));
                }
            });
        }
    }

    @Override
    public void delete(T entity) {
        editedEntity = entity;
        getContext().delete(editedEntity).fire(new Receiver<Void>() {
            @Override
            public void onSuccess(Void response) {
                fireEvent(new DeleteEvent(editedEntity));
                editedEntity = null;
            }
        });
    }

    public F getService() {
        return service;
    }

    public void setService(F service) {
        this.service = service;
    }

    public CommonRequestContext<T> getContext() {
        if(((AbstractRequestContext)context).isLocked()){
            updateContext();
        }
        return context;
    }

    public void setRequestContext(CommonRequestContext<T> ctx) {
        context = ctx;
        fireEvent(new ContextChangedEvent<T>(context));
    }

    public void updateContext(){
        setRequestContext(service.context());
    }
}
