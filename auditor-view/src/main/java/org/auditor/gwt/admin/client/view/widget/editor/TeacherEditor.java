package org.auditor.gwt.admin.client.view.widget.editor;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.web.bindery.requestfactory.gwt.client.RequestFactoryEditorDriver;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.form.TextField;
import org.auditor.gwt.admin.client.events.editor.DeleteEvent;
import org.auditor.gwt.admin.shared.proxy.TeacherProxy;
import org.auditor.gwt.admin.shared.request.TeacherRequestFactory;

/**
 * Teacher: almagnit
 */

public class TeacherEditor extends AbstractEditor<TeacherProxy, TeacherEditor, TeacherRequestFactory> {

    public interface TeacherDriver extends RequestFactoryEditorDriver<TeacherProxy, TeacherEditor> {
    }


    @Path("name")
    TextField name = new TextField();


    public TeacherEditor() {
        initEditor();
    }

    @Override
    public void delete(TeacherProxy entity) {
        if (entity.getTimetables().size() != 0) {
            new AlertMessageBox("Невозможно удалить", "Преподаватель находится в расписании").show();
        } else {
            if (entity.getReplacements().size() != 0) {
                new AlertMessageBox("Невозможно удалить", "Преподаватель находится в заменах").show();
            } else {
                updateContext();
                editedEntity = entity;
                context.delete(editedEntity).fire(new Receiver<Void>() {
                    @Override
                    public void onSuccess(Void response) {
                        fireEvent(new DeleteEvent(editedEntity));
                        editedEntity = null;
                    }
                });
            }
        }
    }

    @Override
    protected void initRequestFactory() {
        service = GWT.create(TeacherRequestFactory.class);
        driver = GWT.create(TeacherDriver.class);
        service.initialize(new SimpleEventBus(), transport);
        driver.initialize(service, this);
    }

    @Override
    protected void initUI() {
        addField(createFieldLabel(name, "ФИО"));
    }

    @Override
    protected void receiveData() {
    }

    @Override
    protected void initHandlers() {

    }

    @Override
    protected void initValidators() {

    }

    @Override
    public void clear() {

    }
}
