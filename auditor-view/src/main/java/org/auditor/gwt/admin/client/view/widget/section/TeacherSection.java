package org.auditor.gwt.admin.client.view.widget.section;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.*;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.LiveGridView;
import com.sencha.gxt.widget.core.client.grid.filters.GridFilters;
import org.auditor.gwt.admin.client.property.security.TeacherProperties;
import org.auditor.gwt.admin.client.view.widget.editor.TeacherEditor;
import org.auditor.gwt.admin.shared.proxy.TeacherProxy;
import org.auditor.gwt.admin.shared.request.TeacherRequestFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Teacher: almagnit
 */

public class TeacherSection implements IsWidget {

    Grid<TeacherProxy> contentGrid = null;
    TeacherProperties props = GWT.create(TeacherProperties.class);
    List<ColumnConfig<TeacherProxy, ?>> columns = new ArrayList<ColumnConfig<TeacherProxy, ?>>();
    TeacherEditor editor = new TeacherEditor();
    SimpleSection<TeacherProxy, TeacherEditor> teacherSection = new SimpleSection<TeacherProxy, TeacherEditor>(editor);

    public TeacherSection() {
        initGrid();
    }

    public Widget asWidget() {
        return teacherSection.asWidget();
    }

    public void initGrid(){
        columns.add(new ColumnConfig<TeacherProxy, String>(props.name(), 100, "ФИО"));

        RequestFactoryProxy<FilterPagingLoadConfig, PagingLoadResult<TeacherProxy>> proxy =
                new RequestFactoryProxy<FilterPagingLoadConfig, PagingLoadResult<TeacherProxy>>() {
                    @Override
                    public void load(FilterPagingLoadConfig loadConfig, Receiver<? super PagingLoadResult<TeacherProxy>> receiver) {

                        TeacherRequestFactory.TeacherRequest req = editor.getService().context();

                        List<SortInfo> sortInfo = createRequestSortInfo(req, loadConfig.getSortInfo());
                        List<FilterConfig> filterConfig = createRequestFilterConfig(req, loadConfig.getFilters());
                        req.list(loadConfig.getOffset(), loadConfig.getLimit(), filterConfig, sortInfo).to(receiver);

                        req.fire();

                    }
                };
        final PagingLoader<FilterPagingLoadConfig, PagingLoadResult<TeacherProxy>> loader =
                new PagingLoader<FilterPagingLoadConfig, PagingLoadResult<TeacherProxy>>(proxy) {
                    @Override
                    protected FilterPagingLoadConfig newLoadConfig() {
                        return new FilterPagingLoadConfigBean();
                    }
                };



        contentGrid = new Grid<TeacherProxy>(new ListStore<TeacherProxy>(props.uniquekey), new ColumnModel<TeacherProxy>(columns)) {
            @Override
            protected void onAfterFirstAttach() {
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        loader.load(0, ((LiveGridView)contentGrid.getView()).getCacheSize());
                    }
                });
            }
        };
        loader.setRemoteSort(true);
        loader.addLoadHandler(new LoadResultListStoreBinding<FilterPagingLoadConfig, TeacherProxy, PagingLoadResult<TeacherProxy>>(contentGrid.getStore()));
        GridFilters<TeacherProxy> filters = new GridFilters<TeacherProxy>(loader);
        filters.initPlugin(contentGrid);
        contentGrid.setLoader(loader);
        teacherSection.setColumns(columns);
        teacherSection.setContentGrid(contentGrid);
    }
}
