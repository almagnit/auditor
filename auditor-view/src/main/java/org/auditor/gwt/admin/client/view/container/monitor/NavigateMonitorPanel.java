package org.auditor.gwt.admin.client.view.container.monitor;

import org.auditor.gwt.admin.client.presenter.NavigatePanelPresenter;
import org.auditor.gwt.admin.client.view.container.NavigatePanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.sencha.gxt.core.client.util.ToggleGroup;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.container.AccordionLayoutContainer;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;

/**
 * @author almagnit@gmail.com
 *
 */
public class NavigateMonitorPanel extends VBoxLayoutContainer implements NavigatePanel {

    private final String BUTTON_HEIGHT = "30";

    private NavigatePanelPresenter presenter;

    private ContentPanel contentPanel = new ContentPanel();
    private BoxLayoutContainer.BoxLayoutData contentPanelLayoutData = new BoxLayoutContainer.BoxLayoutData();
    private AccordionLayoutContainer accordionContainer = new AccordionLayoutContainer();
    private AccordionLayoutContainer.AccordionLayoutAppearance accordionAppearance = GWT.<AccordionLayoutContainer.AccordionLayoutAppearance> create(AccordionLayoutContainer.AccordionLayoutAppearance.class);
    private ContentPanel projectPanel = new ContentPanel(accordionAppearance);
    private VBoxLayoutContainer projectContainer = new VBoxLayoutContainer();

    private ToggleGroup buttons;
    private ToggleButton visits;

    public void setPresenter(NavigatePanelPresenter presenter) {
        this.presenter = presenter;
    }

    public NavigatePanelPresenter getPresenter() {
        return presenter;
    }

    public void init(){
        buttons = new ToggleGroup();
        contentPanelLayoutData.setFlex(1);
        this.add(contentPanel, contentPanelLayoutData);
        contentPanel.add(accordionContainer, contentPanelLayoutData);
        accordionContainer.add(projectPanel);
        projectPanel.setHeadingText("PROJECT");
        projectPanel.add(projectContainer);
        contentPanel.setResize(true);
        projectContainer.setVBoxLayoutAlign(VBoxLayoutContainer.VBoxLayoutAlign.STRETCH);
        this.setVBoxLayoutAlign(VBoxLayoutContainer.VBoxLayoutAlign.STRETCH);
        initProjectButtons();
        initHandlers();
    }

    private void initProjectButtons(){
        visits = new ToggleButton("Visits");
        initProjectButton(visits);
    }

    private void initProjectButton(ToggleButton button){
        initButton(button);
        projectContainer.add(button);
    }

    private void initButton(ToggleButton button){
        button.setHeight(BUTTON_HEIGHT);
        button.setAllowDepress(false);
        buttons.add(button);
    }

    private void initHandlers(){
        visits.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if (event.getValue()) {
                    presenter.getEventBus().loadUsers();
                }
            }
        });
    }

}
