package org.auditor.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.auditor.entity.Priority;
import org.auditor.gwt.admin.server.GWTSpringEntityLocator;

@ProxyFor(value = Priority.class, locator = GWTSpringEntityLocator.class)
public interface PriorityProxy extends CommonProxy {

    public TeacherProxy getTeacher();
    public void setTeacher(TeacherProxy teacher);

    public AudienceProxy getAudience();
    public void setAudience(AudienceProxy audience);

    public Integer getPriority();
    public void setPriority(Integer priority);

}
