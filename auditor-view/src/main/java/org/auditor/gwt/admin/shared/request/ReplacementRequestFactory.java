package org.auditor.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import org.auditor.gwt.admin.server.GWTSpringServiceLocator;
import org.auditor.gwt.admin.shared.proxy.ReplacementPagingProxy;
import org.auditor.gwt.admin.shared.proxy.ReplacementProxy;
import org.auditor.service.ReplacementService;

import java.util.List;

public interface ReplacementRequestFactory extends CommonRequestFactory {

    @Service(value = ReplacementService.class, locator = GWTSpringServiceLocator.class)
    public interface ReplacementRequest extends CommonRequestContext<ReplacementProxy>{
        Request<ReplacementPagingProxy> list(int offset, int limit, List<? extends FilterConfig> filterConfig, List<? extends SortInfo> sortInfo);
        Request<List<ReplacementProxy>> list();
        Request<ReplacementProxy> update(ReplacementProxy entity);
        Request<ReplacementProxy> add(ReplacementProxy entity);
        Request<ReplacementProxy> create();
        Request<ReplacementProxy> find(String value);
        Request<ReplacementProxy> find(Long value);
        Request<Void> delete(ReplacementProxy entity);
        Request<Void> delete(Long id);
    }

    ReplacementRequest context();

}
