package org.auditor.gwt.admin.client.property.security;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import org.auditor.gwt.admin.shared.proxy.AudienceProxy;

public interface AudienceProperties extends CommonProperties<AudienceProxy> {

    @Editor.Path("numberAudience")
    ValueProvider<AudienceProxy, Integer> numberAudience();

    @Editor.Path("nameAudience")
    ValueProvider<AudienceProxy, String> nameAudience();

    @Editor.Path("nameAudience")
    LabelProvider<AudienceProxy> nameLabel();
}
