package org.auditor.gwt.admin.client.view.container;

import org.auditor.gwt.admin.client.presenter.NavigatePanelPresenter;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * @author almagnit@gmail.com
 *
 */
public interface NavigatePanel extends IsWidget {

    public void setPresenter(NavigatePanelPresenter presenter);

    public void init();


}
