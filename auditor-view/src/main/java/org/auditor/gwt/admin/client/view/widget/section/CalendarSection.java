package org.auditor.gwt.admin.client.view.widget.section;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.core.client.dom.ScrollSupport;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import org.auditor.gwt.admin.client.property.security.CalendarProperties;
import org.auditor.gwt.admin.client.view.widget.editor.CalendarEditor;
import org.auditor.gwt.admin.shared.proxy.CalendarProxy;
import org.auditor.gwt.admin.shared.request.CalendarRequestFactory;

import java.util.List;

public class CalendarSection implements IsWidget{

    CalendarEditor editor = new CalendarEditor();
    VerticalLayoutContainer editorContainer = new VerticalLayoutContainer();
    CalendarProperties properties = GWT.create(CalendarProperties.class);
    ListStore<CalendarProxy> listStore = new ListStore<CalendarProxy>(properties.uniquekey);
    ComboBox<CalendarProxy> date = new ComboBox<CalendarProxy>(listStore, properties.dateLabel);
    VBoxLayoutContainer dateContainer = new VBoxLayoutContainer();

    public CalendarSection(){
        init();
    }

    private void init(){
        dateContainer.add(new Label("Дата : "));
        dateContainer.setVBoxLayoutAlign(VBoxLayoutContainer.VBoxLayoutAlign.RIGHT);
        CalendarRequestFactory calendarService = GWT.create(CalendarRequestFactory.class);
        calendarService.initialize(new SimpleEventBus());
        date.setTypeAhead(true);
        date.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        date.setEditable(false);
        date.addSelectionHandler(new SelectionHandler<CalendarProxy>() {
            @Override
            public void onSelection(SelectionEvent<CalendarProxy> event) {
                editor.edit(event.getSelectedItem());
            }
        });
        calendarService.context().list().fire(new Receiver<List<CalendarProxy>>() {
            @Override
            public void onSuccess(List<CalendarProxy> response) {
                listStore.replaceAll(response);
                date.select(1);
                editor.edit(response.get(0));
            }
        });
        editorContainer.getScrollSupport().setScrollMode(ScrollSupport.ScrollMode.AUTOY);
        dateContainer.add(date, new BoxLayoutContainer.BoxLayoutData(new Margins(0, 25, 0, 0)));
        editorContainer.add(dateContainer, new VerticalLayoutContainer.VerticalLayoutData(1,25));
        editorContainer.add(editor, new VerticalLayoutContainer.VerticalLayoutData(1,1));
    }

    @Override
    public Widget asWidget() {
        return editorContainer;
    }
}
