package org.auditor.gwt.admin.shared.request.security;

import org.auditor.gwt.admin.server.GWTSpringServiceLocator;
import org.auditor.service.security.SecuritySessionService;
import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.RequestFactory;
import com.google.web.bindery.requestfactory.shared.Service;

/**
 * @author almagnit@gmail.com
 *
 */
public interface SecuritySessionRequestFactory extends RequestFactory {

    @Service(value = SecuritySessionService.class, locator = GWTSpringServiceLocator.class)
    public interface SecuritySessionRequest extends RequestContext {
        Request<String> getUsername();
        Request<String> getUserRole();
    }

    SecuritySessionRequest context();

}