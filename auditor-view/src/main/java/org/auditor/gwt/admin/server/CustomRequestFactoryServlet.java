package org.auditor.gwt.admin.server;

import com.google.web.bindery.requestfactory.server.RequestFactoryServlet;

/**
 * Created with IntelliJ IDEA.
 * User: almagnit
 * Date: 06.11.13
 * Time: 13:08
 * To change this template use File | Settings | File Templates.
 */
public class CustomRequestFactoryServlet extends RequestFactoryServlet{

    public CustomRequestFactoryServlet() {
        super( new LoquaciousExceptionHandler() );
    }

    public CustomRequestFactoryServlet(LoquaciousExceptionHandler loquaciousExceptionHandler, GWTSpringServiceLayerDecorator gwtSpringServiceLayerDecorator) {
        super(loquaciousExceptionHandler, gwtSpringServiceLayerDecorator);
    }
}
