package org.auditor.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import org.auditor.gwt.admin.server.GWTSpringServiceLocator;
import org.auditor.gwt.admin.shared.proxy.TimetablePagingProxy;
import org.auditor.gwt.admin.shared.proxy.TimetableProxy;
import org.auditor.service.TimetableService;

import java.util.List;


public interface TimetableRequestFactory extends CommonRequestFactory {

    @Service(value = TimetableService.class, locator = GWTSpringServiceLocator.class)
    public interface TimetableRequest extends CommonRequestContext<TimetableProxy>{
        Request<TimetablePagingProxy> list(int offset, int limit, List<? extends FilterConfig> filterConfig, List<? extends SortInfo> sortInfo);
        Request<List<TimetableProxy>> list();
        Request<TimetableProxy> update(TimetableProxy entity);
        Request<TimetableProxy> add(TimetableProxy entity);
        Request<TimetableProxy> create();
        Request<TimetableProxy> find(String value);
        Request<TimetableProxy> find(Long value);
        Request<Void> delete(TimetableProxy entity);
        Request<Void> delete(Long id);
    }

    TimetableRequest context();

}
