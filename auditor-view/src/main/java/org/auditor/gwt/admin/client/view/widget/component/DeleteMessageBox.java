package org.auditor.gwt.admin.client.view.widget.component;

import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;

/**
 * User: almagnit
 */

public class DeleteMessageBox extends MessageBox {

    TextButton delete = new TextButton("Удалить");
    TextButton cancel = new TextButton("Отмена");
    SelectEvent.SelectHandler defaultHandler = new SelectEvent.SelectHandler() {
        @Override
        public void onSelect(SelectEvent event) {
            hide();
        }
    };

    public DeleteMessageBox(String title, String message){
        this(title, message, false);
    }

    public DeleteMessageBox(String title, String message, boolean showAtOnce) {
        super(title, message);
        setPredefinedButtons();
        delete.addSelectHandler(defaultHandler);
        cancel.addSelectHandler(defaultHandler);
        setIcon(ICONS.question());
        delete.setItemId("delete");
        cancel.setItemId("cancel");
        setFocusWidget(delete);
        addButton(delete);
        addButton(cancel);
        if(showAtOnce)show();
    }

    public void addOnDeleteHandler(SelectEvent.SelectHandler handler){
        delete.addSelectHandler(handler);
    }

    public void addOnCancelHandler(SelectEvent.SelectHandler handler){
        cancel.addSelectHandler(handler);
    }

}
