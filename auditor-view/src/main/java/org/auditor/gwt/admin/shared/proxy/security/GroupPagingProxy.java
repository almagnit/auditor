package org.auditor.gwt.admin.shared.proxy.security;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import org.auditor.service.Paging;

import java.util.List;

@ProxyFor(value = Paging.ForGroup.class)
public interface GroupPagingProxy extends ValueProxy, PagingLoadResult<GroupProxy> {

    @Override
    public List<GroupProxy> getData();

}
