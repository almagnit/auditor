package org.auditor.gwt.admin.client.view.widget.section;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.*;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.LiveGridView;
import com.sencha.gxt.widget.core.client.grid.filters.GridFilters;
import org.auditor.gwt.admin.client.property.security.TimetableProperties;
import org.auditor.gwt.admin.client.view.widget.editor.TimetableEditor;
import org.auditor.gwt.admin.shared.proxy.TimetableProxy;
import org.auditor.gwt.admin.shared.request.TimetableRequestFactory;

import java.util.ArrayList;
import java.util.List;

public class TimetableSection implements IsWidget {

    Grid<TimetableProxy> contentGrid = null;
    TimetableProperties props = GWT.create(TimetableProperties.class);
    List<ColumnConfig<TimetableProxy, ?>> columns = new ArrayList<ColumnConfig<TimetableProxy, ?>>();
    TimetableEditor editor = new TimetableEditor();
    SimpleSection<TimetableProxy, TimetableEditor> timetableSection = new SimpleSection<TimetableProxy, TimetableEditor>(editor);

    public TimetableSection(){
        initGrid();
    }

    private void initGrid() {
        columns.add(new ColumnConfig<TimetableProxy, String>(props.teacherName(), 100, "Имя преподавателя"));
        columns.add(new ColumnConfig<TimetableProxy, Integer>(props.lesson(), 40, "Пара"));
        columns.add(new ColumnConfig<TimetableProxy, Integer>(props.day(), 40, "День"));
        columns.add(new ColumnConfig<TimetableProxy, String>(props.subject(), 100, "Предмет"));
        columns.add(new ColumnConfig<TimetableProxy, String>(props.groupName(), 100, "Группа"));
        RequestFactoryProxy<FilterPagingLoadConfig, PagingLoadResult<TimetableProxy>> proxy =
                new RequestFactoryProxy<FilterPagingLoadConfig, PagingLoadResult<TimetableProxy>>() {
                    @Override
                    public void load(FilterPagingLoadConfig loadConfig, Receiver<? super PagingLoadResult<TimetableProxy>> receiver) {
                        TimetableRequestFactory.TimetableRequest req = editor.getService().context();
                        List<SortInfo> sortInfo = createRequestSortInfo(req, loadConfig.getSortInfo());
                        List<FilterConfig> filterConfig = createRequestFilterConfig(req, loadConfig.getFilters());
                        req.list(loadConfig.getOffset(), loadConfig.getLimit(), filterConfig, sortInfo).to(receiver);
                        req.fire();
                    }
                };
        final PagingLoader<FilterPagingLoadConfig, PagingLoadResult<TimetableProxy>> loader =
                new PagingLoader<FilterPagingLoadConfig, PagingLoadResult<TimetableProxy>>(proxy) {
                    @Override
                    protected FilterPagingLoadConfig newLoadConfig(){
                        return new FilterPagingLoadConfigBean();
                    }
                };
        contentGrid = new Grid<TimetableProxy>(new ListStore<TimetableProxy>(props.key()), new ColumnModel<TimetableProxy>(columns)){
            @Override
            protected void onAfterFirstAttach(){
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand(){

                    @Override
                    public void execute() {
                        loader.load(0, ((LiveGridView)contentGrid.getView()).getCacheSize());
                    }
                });
            }
        };

        loader.setRemoteSort(true);
        loader.addLoadHandler(new LoadResultListStoreBinding<FilterPagingLoadConfig, TimetableProxy, PagingLoadResult<TimetableProxy>>(contentGrid.getStore()));
        GridFilters<TimetableProxy> filters = new GridFilters<TimetableProxy>(loader);
        filters.initPlugin(contentGrid);
        contentGrid.setLoader(loader);
        timetableSection.setColumns(columns);
        timetableSection.setContentGrid(contentGrid);
        contentGrid.getView().setAutoFill(true);
    }

    @Override
    public Widget asWidget() {
        return timetableSection.asWidget();
    }
}
