package org.auditor.gwt.admin.client.property.security;

import org.auditor.gwt.admin.shared.proxy.security.UserProxy;
import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

/**
 * @author almagnit@gmail.com
 *
 */
public interface UserProperties extends PropertyAccess<UserProxy> {

    @Editor.Path("username")
    com.sencha.gxt.data.shared.ModelKeyProvider<UserProxy> key();

    @Editor.Path("username")
    ValueProvider<UserProxy, String> username();
    @Editor.Path("password")
    ValueProvider<UserProxy, String> password();
    @Editor.Path("enabled")
    ValueProvider<UserProxy, Boolean> enabled();

    @Editor.Path("authority.authority")
    ValueProvider<UserProxy, String> authority();

}
