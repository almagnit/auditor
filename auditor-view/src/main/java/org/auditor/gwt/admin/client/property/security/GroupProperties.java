package org.auditor.gwt.admin.client.property.security;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import org.auditor.gwt.admin.shared.proxy.security.GroupProxy;

public interface GroupProperties extends PropertyAccess<GroupProxy> {

    @Editor.Path("id")
    com.sencha.gxt.data.shared.ModelKeyProvider<GroupProxy> key();

    @Editor.Path("id")
    ValueProvider<GroupProxy, Long> id();

    @Editor.Path("groupName")
    ValueProvider<GroupProxy, String> groupName();

    @Editor.Path("groupName")
    LabelProvider<GroupProxy> nameLabel();

}
