package org.auditor.gwt.admin.shared.proxy.security;

import org.auditor.entity.security.Authority;
import org.auditor.gwt.admin.server.GWTSpringEntityLocator;
import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;

/**
 * @author almagnit@gmail.com
 *
 */

@ProxyFor(value = Authority.class, locator = GWTSpringEntityLocator.class)
public interface AuthorityProxy extends CommonProxy {

    public String getUsername();
    public void setUsername(String username);

    public String getAuthority();
    public void setAuthority(String authority);

}