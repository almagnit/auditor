package org.auditor.gwt.admin.client.events.editor;

import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import com.google.gwt.event.shared.GwtEvent;

/**
 * @author almagnit@gmail.com
 */

public class EditEvent<T extends CommonProxy> extends GwtEvent<EditEventHandler>{

    public static Type<EditEventHandler> TYPE = new Type<EditEventHandler>();

    T entity;

    public EditEvent(T entity) {
        this.entity = entity;
    }

    @Override
    public Type<EditEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EditEventHandler handler) {
        handler.onEdit(this);
    }

    public T getEntity() {
        return entity;
    }
}