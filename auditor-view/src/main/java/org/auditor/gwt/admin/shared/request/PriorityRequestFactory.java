package org.auditor.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import org.auditor.gwt.admin.server.GWTSpringServiceLocator;
import org.auditor.gwt.admin.shared.proxy.PriorityPagingProxy;
import org.auditor.gwt.admin.shared.proxy.PriorityProxy;
import org.auditor.service.PriorityService;

import java.util.List;

public interface PriorityRequestFactory extends CommonRequestFactory {

    @Service(value = PriorityService.class, locator = GWTSpringServiceLocator.class)
    public interface PriorityRequest extends CommonRequestContext<PriorityProxy>{
        Request<PriorityPagingProxy> list(int offset, int limit, List<? extends FilterConfig> filterConfig, List<? extends SortInfo> sortInfo);
        Request<List<PriorityProxy>> list();
        Request<PriorityProxy> update(PriorityProxy entity);
        Request<PriorityProxy> add(PriorityProxy entity);
        Request<Void> save(List<PriorityProxy> entities);
        Request<PriorityProxy> create();
        Request<PriorityProxy> find(String value);
        Request<PriorityProxy> find(Long value);
        Request<Void> delete(PriorityProxy entity);
        Request<Void> delete(Long id);
    }

    PriorityRequest context();

}
