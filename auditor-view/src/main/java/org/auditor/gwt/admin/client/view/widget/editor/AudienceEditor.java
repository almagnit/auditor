package org.auditor.gwt.admin.client.view.widget.editor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.web.bindery.requestfactory.gwt.client.RequestFactoryEditorDriver;
import com.sencha.gxt.widget.core.client.form.NumberField;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor;
import com.sencha.gxt.widget.core.client.form.TextField;
import org.auditor.gwt.admin.shared.proxy.AudienceProxy;
import org.auditor.gwt.admin.shared.request.AudienceRequestFactory;

public class AudienceEditor extends AbstractEditor<AudienceProxy, AudienceEditor, AudienceRequestFactory> {

    public interface AudienceDriver extends RequestFactoryEditorDriver<AudienceProxy, AudienceEditor> {}

    @Path("numberAudience")
    NumberField<Integer> numberAudience = new NumberField<Integer>(new NumberPropertyEditor.IntegerPropertyEditor());

    @Path("nameAudience")
    TextField nameAudience = new TextField();

    public AudienceEditor(){
        initEditor();
    }

    @Override
    public void clear() {
        numberAudience.clear();
        nameAudience.clear();
    }

    @Override
    protected void initRequestFactory() {
        service = GWT.create(AudienceRequestFactory.class);
        driver = GWT.create(AudienceDriver.class);
        context = service.context();
        service.initialize(new SimpleEventBus(), transport);
        driver.initialize(service, this);
    }

    @Override
    protected void initUI() {
        addField(createFieldLabel(numberAudience, "Номер аудитории"));
        addField(createFieldLabel(nameAudience, "Имя аудитории"));
    }

    @Override
    protected void receiveData() {

    }

    @Override
    protected void initHandlers() {

    }

    @Override
    protected void initValidators() {

    }
}
