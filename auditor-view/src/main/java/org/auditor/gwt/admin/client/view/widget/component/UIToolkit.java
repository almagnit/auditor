package org.auditor.gwt.admin.client.view.widget.component;

import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.sencha.gxt.cell.core.client.AbstractEventCell;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;

/**
 * User: almagnit
 */

public class UIToolkit {

    public static  <N extends Boolean, T> ColumnConfig<T, N> getBooleanColumnConfig(String trueValue, String falseValue, ValueProvider<? super T, N> valueProvider, int width, String header){
        ColumnConfig<T, N> carTypeConfig = new ColumnConfig<T, N>(valueProvider, width, header);
        carTypeConfig.setCell(new BooleanToStringCell<N>(trueValue, falseValue));
        return carTypeConfig;
    }

    private static class BooleanToStringCell<N extends Boolean> extends AbstractEventCell<N> {

        String trueValue = null;
        String falseValue = null;

        private BooleanToStringCell(String trueValue, String falseValue) {
            this.trueValue = trueValue;
            this.falseValue = falseValue;
        }

        @Override
        public void render(Context context, N value, SafeHtmlBuilder sb) {
            sb.appendEscaped(Boolean.TRUE.equals(value) ? trueValue : falseValue);
        }

    }

}
