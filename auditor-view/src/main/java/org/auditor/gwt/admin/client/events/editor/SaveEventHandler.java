package org.auditor.gwt.admin.client.events.editor;


import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import com.google.gwt.event.shared.EventHandler;

/**
 * @author almagnit@gmail.com
 */

public interface SaveEventHandler<T extends CommonProxy> extends EventHandler {

    void onSave(SaveEvent<T> event);

}
