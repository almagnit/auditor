package org.auditor.gwt.admin.client.view.widget.editor;

import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * @author almagnit@gmail.com
 */

public interface CommonSection<T extends CommonProxy> extends IsWidget {

    public T create();

    public void save();

    public void update(T entity);

    public void delete(T entity);

}
