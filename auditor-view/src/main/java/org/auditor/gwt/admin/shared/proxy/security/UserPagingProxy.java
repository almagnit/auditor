package org.auditor.gwt.admin.shared.proxy.security;

import org.auditor.service.Paging;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */
@ProxyFor(value = Paging.ForUser.class)
public interface UserPagingProxy extends ValueProxy, PagingLoadResult<UserProxy> {
    @Override
    public List<UserProxy> getData();
}