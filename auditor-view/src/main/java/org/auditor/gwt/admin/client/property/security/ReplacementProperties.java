package org.auditor.gwt.admin.client.property.security;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import org.auditor.gwt.admin.shared.proxy.ReplacementProxy;

import java.util.Date;

public interface ReplacementProperties extends PropertyAccess<ReplacementProxy> {

    @Editor.Path("id")
    com.sencha.gxt.data.shared.ModelKeyProvider<ReplacementProxy> key();

    @Editor.Path("id")
    ValueProvider<ReplacementProxy, Long> id();

    @Editor.Path("teacher.name")
    ValueProvider<ReplacementProxy, String> teacherName();

    @Editor.Path("lesson")
    ValueProvider<ReplacementProxy, Integer> lesson();

    @Editor.Path("date")
    ValueProvider<ReplacementProxy, Date> date();

    @Editor.Path("subject")
    ValueProvider<ReplacementProxy, String> subject();

    @Editor.Path("group.groupName")
    ValueProvider<ReplacementProxy, String> groupName();

}
