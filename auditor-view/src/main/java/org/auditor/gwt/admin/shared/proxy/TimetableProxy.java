package org.auditor.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.auditor.entity.Timetable;
import org.auditor.gwt.admin.server.GWTSpringEntityLocator;
import org.auditor.gwt.admin.shared.proxy.security.GroupProxy;

@ProxyFor(value = Timetable.class, locator = GWTSpringEntityLocator.class)
public interface TimetableProxy  extends CommonProxy {

    public TeacherProxy getTeacher();
    public void setTeacher(TeacherProxy teacher);

    public Integer getLesson();
    public void setLesson(Integer lesson);

    public Integer getDay();
    public void setDay(Integer day);

    public String getSubject();
    public void setSubject(String subject);

    public GroupProxy getGroup();
    public void setGroup(GroupProxy group);
}
