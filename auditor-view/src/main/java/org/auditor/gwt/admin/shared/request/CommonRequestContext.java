package org.auditor.gwt.admin.shared.request;

import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import org.auditor.service.CommonService;
import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.Service;

import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */

@Service(value = CommonService.class)
public interface CommonRequestContext<P extends CommonProxy> extends RequestContext {
    Request<List<P>> list();
    Request<P> update(P entity);
    Request<P> add(P entity);
    Request<P> create();
    Request<P> find(String value);
    Request<P> find(Long value);
    Request<Void> delete(P entity);
    Request<Void>delete(Long id);
}