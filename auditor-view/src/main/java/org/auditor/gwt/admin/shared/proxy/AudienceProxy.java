package org.auditor.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.auditor.entity.Audience;
import org.auditor.gwt.admin.server.GWTSpringEntityLocator;

import java.util.List;

@ProxyFor(value = Audience.class, locator = GWTSpringEntityLocator.class)
public interface AudienceProxy extends CommonProxy {

    public Integer getNumberAudience();
    public void setNumberAudience(Integer numberAudience);

    public String getNameAudience();
    public void setNameAudience(String nameAudience);

    public List<PriorityProxy> getPriorities();
    public void setPriorities(List<PriorityProxy> priorities);

}
