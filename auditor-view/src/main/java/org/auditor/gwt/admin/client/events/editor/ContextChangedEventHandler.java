package org.auditor.gwt.admin.client.events.editor;


import com.google.gwt.event.shared.EventHandler;

/**
 * @author almagnit@gmail.com
 */

public interface ContextChangedEventHandler extends EventHandler {

    void beforeContextChanged(ContextChangedEvent event);

}
