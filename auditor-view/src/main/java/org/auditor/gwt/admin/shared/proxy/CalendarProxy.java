package org.auditor.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.auditor.entity.Calendar;
import org.auditor.gwt.admin.server.GWTSpringEntityLocator;

import java.util.Date;
import java.util.List;

@ProxyFor(value = Calendar.class, locator = GWTSpringEntityLocator.class)
public interface CalendarProxy extends CommonProxy {

    public Date getDate();
    public void setDate(Date date);

    public List<FinalTableProxy> getFinalTables();
    public void setFinalTables(List<FinalTableProxy> finalTables);

}
