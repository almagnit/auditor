package org.auditor.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.auditor.entity.Replacement;
import org.auditor.gwt.admin.server.GWTSpringEntityLocator;
import org.auditor.gwt.admin.shared.proxy.security.GroupProxy;

import java.util.Date;

@ProxyFor(value = Replacement.class, locator = GWTSpringEntityLocator.class)
public interface ReplacementProxy extends CommonProxy {

    public TeacherProxy getTeacher();
    public void setTeacher(TeacherProxy teacher);

    public Integer getLesson();
    public void setLesson(Integer lesson);

    public Date getDate();
    public void setDate(Date date);

    public String getSubject();
    public void setSubject(String subject);

    public GroupProxy getGroup();
    public void setGroup(GroupProxy group);

}
