package org.auditor.gwt.admin.client.events.editor;


import com.google.gwt.event.shared.EventHandler;

/**
 * @author almagnit@gmail.com
 */

public interface BeforeEditEventHandler extends EventHandler {

    void onBeforeEdit(BeforeEditEvent event);

}
