package org.auditor.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import org.auditor.service.Paging;

import java.util.List;

/**
 * Created by almagnit on 15.01.14.
 */

@ProxyFor(value = Paging.ForTeacher.class)
public interface TeacherPagingProxy extends ValueProxy, PagingLoadResult<TeacherProxy> {
    @Override
    public List<TeacherProxy> getData();
}