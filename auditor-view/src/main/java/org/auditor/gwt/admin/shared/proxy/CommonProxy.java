package org.auditor.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import org.auditor.entity.CommonEntity;

/**
 * @author almagnit@gmail.com
 *
 */

@ProxyFor(CommonEntity.class)
public interface CommonProxy extends EntityProxy, ValueProxy {

    public String getKey();
    public void setKey(String key);

    public Long getId();
    public void setId(Long id);

    public String getLabel();
    public void setLabel(String label);

    public Integer getVersion();
    public void setVersion(Integer version);


}