package org.auditor.gwt.admin.client.view.widget.section;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.*;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.LiveGridView;
import com.sencha.gxt.widget.core.client.grid.filters.GridFilters;
import org.auditor.gwt.admin.client.property.security.ReplacementProperties;
import org.auditor.gwt.admin.client.view.widget.editor.ReplacementEditor;
import org.auditor.gwt.admin.shared.proxy.ReplacementProxy;
import org.auditor.gwt.admin.shared.request.ReplacementRequestFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReplacementSection implements IsWidget {

    Grid<ReplacementProxy>contentGrid = null;
    ReplacementProperties properties = GWT.create(ReplacementProperties.class);
    List<ColumnConfig<ReplacementProxy, ?>> columns =
            new ArrayList<ColumnConfig<ReplacementProxy, ?>>();
    ReplacementEditor editor = new ReplacementEditor();
    SimpleSection<ReplacementProxy, ReplacementEditor> replacementSection =
            new SimpleSection<ReplacementProxy, ReplacementEditor>(editor);

    public ReplacementSection(){
        this.initGrid();
    }

    @Override
    public Widget asWidget() {
        return replacementSection.asWidget();
    }

    private void initGrid() {
        columns.add(new ColumnConfig<ReplacementProxy, String>(properties.teacherName(), 100, "Имя преподавателя"));
        columns.add(new ColumnConfig<ReplacementProxy, Integer>(properties.lesson(), 40, "Пара"));
        columns.add(new ColumnConfig<ReplacementProxy, Date>(properties.date(), 40, "Дата"));
        columns.add(new ColumnConfig<ReplacementProxy, String>(properties.subject(), 100, "Предмет"));
        columns.add(new ColumnConfig<ReplacementProxy, String>(properties.groupName(), 100, "Группа"));

        RequestFactoryProxy<FilterPagingLoadConfig, PagingLoadResult<ReplacementProxy>> proxy =
                new RequestFactoryProxy<FilterPagingLoadConfig, PagingLoadResult<ReplacementProxy>>() {
                    @Override
                    public void load(FilterPagingLoadConfig loadConfig, Receiver<? super PagingLoadResult<ReplacementProxy>> receiver) {
                        ReplacementRequestFactory.ReplacementRequest req = editor.getService().context();
                        List<SortInfo> sortInfo = createRequestSortInfo(req, loadConfig.getSortInfo());
                        List<FilterConfig> filterConfig = createRequestFilterConfig(req, loadConfig.getFilters());
                        req.list(loadConfig.getOffset(), loadConfig.getLimit(), filterConfig, sortInfo).to(receiver);
                        req.fire();
                    }
                };
        final PagingLoader<FilterPagingLoadConfig, PagingLoadResult<ReplacementProxy>> loader =
                new PagingLoader<FilterPagingLoadConfig, PagingLoadResult<ReplacementProxy>>(proxy) {
                    @Override
                    protected FilterPagingLoadConfig newLoadConfig(){
                        return new FilterPagingLoadConfigBean();
                    }
                };
        contentGrid = new Grid<ReplacementProxy>(new ListStore<ReplacementProxy>(properties.key()), new ColumnModel<ReplacementProxy>(columns)){
            @Override
            protected void onAfterFirstAttach(){
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand(){

                    @Override
                    public void execute() {
                        loader.load(0, ((LiveGridView)contentGrid.getView()).getCacheSize());
                    }
                });
            }
        };

        loader.setRemoteSort(true);
        loader.addLoadHandler(new LoadResultListStoreBinding<FilterPagingLoadConfig, ReplacementProxy, PagingLoadResult<ReplacementProxy>>(contentGrid.getStore()));
        GridFilters<ReplacementProxy> filters = new GridFilters<ReplacementProxy>(loader);
        filters.initPlugin(contentGrid);
        contentGrid.setLoader(loader);
        replacementSection.setColumns(columns);
        replacementSection.setContentGrid(contentGrid);
    }
}
