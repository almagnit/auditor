package org.auditor.gwt.admin.client.property.security;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import org.auditor.gwt.admin.shared.proxy.TeacherProxy;

/**
 * @author almagnit@gmail.com
 */

public interface TeacherProperties extends CommonProperties<TeacherProxy> {

    @Editor.Path("name")
    ValueProvider<TeacherProxy, String> name();

    @Editor.Path("name")
    LabelProvider<TeacherProxy> nameLabel();
}
