package org.auditor.gwt.admin.client.view.widget.section;

import org.auditor.gwt.admin.client.view.widget.editor.AbstractEditor;
import org.auditor.gwt.admin.client.view.widget.editor.CommonSection;
import org.auditor.gwt.admin.shared.proxy.CommonProxy;

/**
 * @author almagnit@gmail.com
 */

public abstract class AbstractSection<T extends CommonProxy, E extends AbstractEditor> implements CommonSection<T> {

    protected E editor;

    public void setEditor(E editor) {
        this.editor = editor;
    }

    @Override
    public void delete(T entity) {
        editor.delete(entity);
    }

    public void edit(T entity){
        editor.edit(entity);
    }

    @Override
    public void update(T entity) {
        editor.update();
    }

    @Override
    public void save() {
        editor.save();
    }

    @Override
    public T create() {
        return (T) editor.create();
    }

    public void clearEditor(){
        editor.clear();
    }
}
