package org.auditor.gwt.admin.client.view.widget.editor;

import com.google.gwt.editor.client.Editor;
import com.google.gwt.editor.client.IsEditor;
import com.google.gwt.editor.client.adapters.EditorSource;
import com.google.gwt.editor.client.adapters.ListEditor;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.Composite;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import org.auditor.gwt.admin.shared.proxy.CommonProxy;

abstract public class TableListEditor<T extends CommonProxy, E extends Editor<T> & IsWidget>
        extends Composite
        implements IsEditor<ListEditor<T, E>> {

    protected ListEditor<T, E> listEditor = ListEditor.of(new ItemEditorSource());
    protected VerticalLayoutContainer container = new VerticalLayoutContainer();
    protected FlexTable flexTable = new FlexTable();
    protected Integer tableColNum;

    protected TableListEditor(int tableColNum) {
        this.tableColNum = tableColNum;
        container.add(flexTable);
        initTable();
        initWidget(container);
    }

    @Override
    public ListEditor<T, E> asEditor() {
        return listEditor;
    }

    protected void initTable(){

    }

    abstract protected E createItemEditor(int index);
    protected void placeItemEditor(E subEditor, int index){
        flexTable.setWidget(index/tableColNum, index % tableColNum, subEditor);
    }
    protected void disposeItemEditor(E subEditor){
        ((Widget)subEditor).removeFromParent();
        int rowNeeded = (int) ((listEditor.getEditors().size() + tableColNum -1) / tableColNum);
        // cleanup bottom rows
        while (flexTable.getRowCount()>rowNeeded){
            flexTable.removeRow(rowNeeded);
        }
    }

    private class ItemEditorSource extends EditorSource<E> {

        @Override
        public E create(int index) {
            E item = createItemEditor(index);
            placeItemEditor(item, index);
            return item;
        }

        @Override
        public void dispose(E subEditor) {
            super.dispose(subEditor);
            disposeItemEditor(subEditor);
        }
    }

}