package org.auditor.gwt.admin.client.view.widget.component;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Event;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.widget.core.client.form.SimpleComboBox;

/**
 * Created with IntelliJ IDEA.
 * User: Artur
 * Date: 19.10.13
 * Time: 22:51
 *
 * @author : artyrian@ya.ru
 */

/*
 *  This clas was created because there is a bug in 3.* GWT.
 *  Answer from: http://www.sencha.com/forum/showthread.php?196281-GXT-3-rc2-ComboBox-setForceSelection(false)-does-not-work/page2
 *  TODO : normalize while gwt will be fixed.
 */
//public class CustomStringComboBox<T> extends SimpleComboBox<T> {
public class CustomStringComboBox<S> extends SimpleComboBox<S> {
    //protected Converter<T, String> fromStringConverter;

    public CustomStringComboBox(LabelProvider labelProvider) {
        super(labelProvider);
    }

//    public void setFromStringConverter(Converter<T, String> fromStringConverter) {
//        this.fromStringConverter = fromStringConverter;
//    }

    @Override
    protected void onBlur(Event be) {

        String stringValue = getText(); // getCell().getText(getElement());
        super.onBlur(be);
        GWT.log("string value art " + stringValue);
        S  value = (S) stringValue;
        setValue(value, true);


//        if (fromStringConverter != null) {
//            T value = fromStringConverter.convertFieldValue(stringValue);
//            ListStore<T> store = getStore();
//            if(store.findModel(value) == null) {
//                store.add(value);
//                store.commitChanges();
//            }
//            setValue(value, true);
//            GWT.log("Changed value to " + value);
//        }
    }
}
