package org.auditor.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import org.auditor.gwt.admin.server.GWTSpringServiceLocator;
import org.auditor.gwt.admin.shared.proxy.FinalTablePagingProxy;
import org.auditor.gwt.admin.shared.proxy.FinalTableProxy;
import org.auditor.service.FinalTableService;

import java.util.List;

public interface FinalTableRequestFactory extends CommonRequestFactory {

    @Service(value = FinalTableService.class, locator = GWTSpringServiceLocator.class)
    public interface FinalTableRequest extends CommonRequestContext<FinalTableProxy>{
        Request<FinalTablePagingProxy> list(int offset, int limit, List<? extends FilterConfig> filterConfig, List<? extends SortInfo> sortInfo);
        Request<List<FinalTableProxy>> list();
        Request<FinalTableProxy> update(FinalTableProxy entity);
        Request<FinalTableProxy> add(FinalTableProxy entity);
        Request<FinalTableProxy> create();
        Request<FinalTableProxy> find(String value);
        Request<FinalTableProxy> find(Long value);
        Request<Void> delete(FinalTableProxy entity);
        Request<Void> delete(Long id);
    }

    FinalTableRequest context();

}
