package org.auditor.gwt.admin.shared.request.security;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import org.auditor.gwt.admin.server.GWTSpringServiceLocator;
import org.auditor.gwt.admin.shared.proxy.security.GroupPagingProxy;
import org.auditor.gwt.admin.shared.proxy.security.GroupProxy;
import org.auditor.gwt.admin.shared.request.CommonRequestContext;
import org.auditor.gwt.admin.shared.request.CommonRequestFactory;
import org.auditor.service.security.GroupService;

import java.util.List;

public interface GroupRequestFactory extends CommonRequestFactory {

    @Service(value = GroupService.class, locator = GWTSpringServiceLocator.class)
    public interface GroupRequest extends CommonRequestContext<GroupProxy> {
        Request<GroupPagingProxy> list(int offset, int limit, List<? extends FilterConfig> filterConfig, List<? extends SortInfo> sortInfo);
        Request<List<GroupProxy>> list();
        Request<GroupProxy> update(GroupProxy entity);
        Request<GroupProxy> add(GroupProxy entity);
        Request<GroupProxy> create();
        Request<GroupProxy> find(String value);
        Request<GroupProxy> find(Long value);
        Request<Void> delete(GroupProxy entity);
        Request<Void> delete(Long id);
    }

    GroupRequest context();

}
