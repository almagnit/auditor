package org.auditor.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import org.auditor.gwt.admin.server.GWTSpringServiceLocator;
import org.auditor.gwt.admin.shared.proxy.AudiencePagingProxy;
import org.auditor.gwt.admin.shared.proxy.AudienceProxy;
import org.auditor.gwt.admin.shared.proxy.PriorityProxy;
import org.auditor.service.AudienceService;

import java.util.List;

public interface AudienceRequestFactory extends CommonRequestFactory {

    @Service(value = AudienceService.class, locator = GWTSpringServiceLocator.class)
    public interface AudienceRequest extends CommonRequestContext<AudienceProxy>{
        Request<AudiencePagingProxy> list(int offset, int limit, List<? extends FilterConfig> filterConfig, List<? extends SortInfo> sortInfo);
        Request<List<AudienceProxy>> list();
        Request<AudienceProxy> update(AudienceProxy entity);
        Request<AudienceProxy> add(AudienceProxy entity);
        Request<AudienceProxy> create();
        Request<AudienceProxy> find(String value);
        Request<AudienceProxy> find(Long value);
        Request<Void> delete(AudienceProxy entity);
        Request<Void> delete(Long id);
        Request<Void> deletePriority(PriorityProxy entity);
    }


    AudienceRequest context();

}
