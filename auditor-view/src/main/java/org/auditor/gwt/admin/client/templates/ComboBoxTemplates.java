package org.auditor.gwt.admin.client.templates;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.sencha.gxt.core.client.XTemplates;

public interface ComboBoxTemplates extends XTemplates{

    @XTemplate("<div>{name}</div>")
    SafeHtml teacher(String name);

    @XTemplate("<div>{groupName}</div>")
    SafeHtml group(String groupName);

    @XTemplate("<div>{audienceName}</div>")
    SafeHtml audience(String audienceName);

}
