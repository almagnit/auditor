package org.auditor.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.Service;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import org.auditor.gwt.admin.server.GWTSpringServiceLocator;
import org.auditor.gwt.admin.shared.proxy.TeacherPagingProxy;
import org.auditor.gwt.admin.shared.proxy.TeacherProxy;
import org.auditor.service.TeacherService;

import java.util.List;

/**
 * Created by almagnit on 15.01.14.
 */

public interface TeacherRequestFactory extends CommonRequestFactory{

    @Service(value = TeacherService.class, locator = GWTSpringServiceLocator.class)
    public interface TeacherRequest extends CommonRequestContext<TeacherProxy> {
        Request<TeacherPagingProxy> list(int offset, int limit, List<? extends FilterConfig> filterConfig, List<? extends SortInfo> sortInfo);
        Request<List<TeacherProxy>> list();
        Request<TeacherProxy> update(TeacherProxy entity);
        Request<TeacherProxy> add(TeacherProxy entity);
        Request<TeacherProxy> create();
        Request<TeacherProxy> find(String value);
        Request<TeacherProxy> find(Long value);
        Request<Void> delete(TeacherProxy entity);
        Request<Void> delete(Long id);
    }
    @Override
    TeacherRequest context();
}
