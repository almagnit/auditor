package org.auditor.gwt.admin.client.view.container;

import org.auditor.gwt.admin.client.presenter.NavigatePanelPresenter;
import org.auditor.gwt.admin.client.view.container.admin.NavigateAdminPanel;
import org.auditor.gwt.admin.client.view.container.monitor.NavigateMonitorPanel;
import com.google.gwt.user.client.ui.Widget;
import com.mvp4g.client.view.ReverseViewInterface;

/**
 * @author almagnit@gmail.com
 *
 */
public class CommonNavigatePanel implements ReverseViewInterface<NavigatePanelPresenter>, NavigatePanelPresenter.NavigatePanelInterface {

    private NavigatePanelPresenter presenter;
    private NavigatePanel navigatePanel;

    @Override
    public void setPresenter(NavigatePanelPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public NavigatePanelPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void init(String role) {
        if (role.toString().equals("ROLE_ADMIN")) {
            navigatePanel = new NavigateAdminPanel();
        } else if (role.toString().equals("ROLE_MONITOR")) {
            navigatePanel = new NavigateMonitorPanel();
        } else {
            return;
        }
        navigatePanel.setPresenter(presenter);
        navigatePanel.init();
    }

    @Override
    public Widget asWidget() {
        return navigatePanel.asWidget();
    }
}
