package org.auditor.gwt.admin.shared.proxy.security;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import org.auditor.entity.security.Group;
import org.auditor.gwt.admin.server.GWTSpringEntityLocator;
import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import org.auditor.gwt.admin.shared.proxy.ReplacementProxy;
import org.auditor.gwt.admin.shared.proxy.TimetableProxy;

import java.util.List;

@ProxyFor(value = Group.class, locator = GWTSpringEntityLocator.class)
public interface GroupProxy extends CommonProxy {

    public String getGroupName();
    public void setGroupName(String groupName);

    public List<TimetableProxy> getTimetables();
    public void setTimetables(List<TimetableProxy> timetables);

    public List<ReplacementProxy> getReplacements();
    public void setReplacements(List<ReplacementProxy> replacements);

}
