package org.auditor.gwt.admin.client.property.security;

import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;
import org.auditor.gwt.admin.shared.proxy.AudienceProxy;
import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import org.auditor.gwt.admin.shared.proxy.PriorityProxy;

/**
 * Created by almagnit on 30.01.14.
 */
public interface CommonProperties<T extends CommonProxy>  extends PropertyAccess<T> {

    @Editor.Path("key")
    ModelKeyProvider<CommonProxy> uniquekey = new ModelKeyProvider<CommonProxy>() {
        @Override
        public String getKey(CommonProxy item) {
            if(item instanceof PriorityProxy){
                return ((PriorityProxy) item).getAudience().getKey() + "->" + ((PriorityProxy) item).getTeacher().getKey();
            }
            return item.getKey();
        }
    };

    @Editor.Path("label")
    ValueProvider<CommonProxy, String> label = new ValueProvider<CommonProxy, String>() {
        @Override
        public String getValue(CommonProxy object) {
            if(object instanceof AudienceProxy) return ((AudienceProxy) object).getNameAudience();
            if(object instanceof PriorityProxy) return ((PriorityProxy) object).getTeacher().getName();
            return object.getLabel();
        }

        @Override
        public void setValue(CommonProxy object, String value) {}

        @Override
        public String getPath() {
            return "label";
        }
    };

    @Editor.Path("key")
    ValueProvider<CommonProxy, String> key();

    @Editor.Path("id")
    ValueProvider<CommonProxy, Long> id();

}
