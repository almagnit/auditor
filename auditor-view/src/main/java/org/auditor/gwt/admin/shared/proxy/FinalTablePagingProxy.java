package org.auditor.gwt.admin.shared.proxy;

import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import org.auditor.service.Paging;

import java.util.List;

@ProxyFor(value = Paging.ForFinalTable.class)
public interface FinalTablePagingProxy extends ValueProxy, PagingLoadResult<FinalTableProxy> {

    @Override
    public List<FinalTableProxy> getData();

}
