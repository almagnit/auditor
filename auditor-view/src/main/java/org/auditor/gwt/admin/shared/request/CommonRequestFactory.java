package org.auditor.gwt.admin.shared.request;

import com.google.web.bindery.requestfactory.shared.RequestFactory;

/**
 * @author almagnit@gmail.com
 *
 */
public interface CommonRequestFactory extends RequestFactory {

    public CommonRequestContext context();

}
