package org.auditor.gwt.admin.client.view.container.admin;

import org.auditor.gwt.admin.client.presenter.NavigatePanelPresenter;
import org.auditor.gwt.admin.client.view.container.NavigatePanel;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.sencha.gxt.core.client.util.ToggleGroup;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.container.AccordionLayoutContainer;
import com.sencha.gxt.widget.core.client.container.AccordionLayoutContainer.AccordionLayoutAppearance;
import com.sencha.gxt.widget.core.client.container.VBoxLayoutContainer;

/**
 * @author almagnit@gmail.com
 *
 */
public class NavigateAdminPanel extends VBoxLayoutContainer implements NavigatePanel {

    private final String BUTTON_HEIGHT = "30";

    private NavigatePanelPresenter presenter;

    private ContentPanel contentPanel = new ContentPanel();
    private BoxLayoutData contentPanelLayoutData = new BoxLayoutData();
    private AccordionLayoutContainer accordionContainer = new AccordionLayoutContainer();
    private AccordionLayoutAppearance accordionAppearance = GWT.<AccordionLayoutAppearance> create(AccordionLayoutAppearance.class);
    private ContentPanel hrPanel = new ContentPanel(accordionAppearance);
    private VBoxLayoutContainer hrContainer = new VBoxLayoutContainer();

    private ToggleGroup buttons;
    private ToggleButton users;
    private ToggleButton teachers;
    private ToggleButton timetable;
    private ToggleButton audience;
    private ToggleButton replacement;
    private ToggleButton group;
    private ToggleButton calendar;

    public NavigateAdminPanel() {
        this.setBorders(false);
        buttons = new ToggleGroup();
        contentPanelLayoutData.setFlex(1);
        this.add(contentPanel, contentPanelLayoutData);
        contentPanel.add(accordionContainer, contentPanelLayoutData);
        accordionContainer.add(hrPanel);
        accordionContainer.setBorders(false);
        hrPanel.setHeadingText("Разделы");
        hrPanel.add(hrContainer);
        contentPanel.setResize(true);
        contentPanel.setHeaderVisible(false);
        contentPanel.setBorders(false);
        contentPanel.setBodyBorder(false);
        hrPanel.setBorders(false);
        hrPanel.setBodyBorder(false);
        hrContainer.setBorders(false);
        hrContainer.setVBoxLayoutAlign(VBoxLayoutAlign.STRETCH);
        this.setVBoxLayoutAlign(VBoxLayoutAlign.STRETCH);
    }

    public void setPresenter(NavigatePanelPresenter presenter) {
        this.presenter = presenter;
    }

    public NavigatePanelPresenter getPresenter() {
        return presenter;
    }

    public void init(){
        initButtons();
        initHandlers();
    }

    private void initButtons(){
        calendar = new ToggleButton("Планирование");
        initHrButton(calendar);
        audience = new ToggleButton("Аудитории");
        initHrButton(audience);
        replacement = new ToggleButton("Подмены");
        initHrButton(replacement);
        timetable = new ToggleButton("Расписание");
        initHrButton(timetable);
        teachers = new ToggleButton("Преподаватели");
        initHrButton(teachers);
        group = new ToggleButton("Группы");
        initHrButton(group);
//        users = new ToggleButton("Пользователи");
//        initHrButton(users);
    }

    private void initHrButton(ToggleButton button){
        initButton(button);
        hrContainer.add(button);
    }

    private void initButton(ToggleButton button){
        button.setHeight(BUTTON_HEIGHT);
        button.setAllowDepress(false);
        buttons.add(button);
    }

    private void initHandlers(){
/*
        users.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if (event.getValue()) {
                    presenter.getEventBus().loadUsers();
                }
            }
        });
*/
        teachers.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if (event.getValue()) {
                    presenter.getEventBus().loadTeachers();
                }
            }
        });
        timetable.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if(event.getValue()){
                   presenter.getEventBus().loadTimetable();
                }
            }
        });
        audience.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if(event.getValue()){
                    presenter.getEventBus().loadAudience();
                }
            }
        });
        replacement.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if(event.getValue()){
                    presenter.getEventBus().loadReplacement();
                }
            }
        });
        group.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if(event.getValue()){
                    presenter.getEventBus().loadGroup();
                }
            }
        });

        calendar.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            @Override
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                if (event.getValue()) {
                    presenter.getEventBus().loadCalendar();
                }
            }
        });
    }

}
