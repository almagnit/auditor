package org.auditor.gwt.admin.client.events.editor;

import org.auditor.gwt.admin.shared.proxy.CommonProxy;
import com.google.gwt.event.shared.GwtEvent;

/**
 * @author almagnit@gmail.com
 */

public class CreateEvent<T extends CommonProxy> extends GwtEvent<CreateEventHandler>{

    public static Type<CreateEventHandler> TYPE = new Type<CreateEventHandler>();

    T entity;

    public CreateEvent(T entity) {
        this.entity = entity;
    }

    @Override
    public Type<CreateEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CreateEventHandler handler) {
        handler.onCreate(this);
    }

    public T getEntity() {
        return entity;
    }
}