package org.auditor.gwt.admin.client.view.widget.component;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.DateBox;
import com.sencha.gxt.widget.core.client.form.DateField;
import com.sencha.gxt.widget.core.client.form.DateTimePropertyEditor;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Artur
 * Date: 27.11.13
 * Time: 15:33
 *
 * @author : artyrian@ya.ru
 */
    public class LocalDateField extends DateField {

    public LocalDateField() {
        super();
        this.setPropertyEditor(new DateTimePropertyEditor("yyyy.MM.dd"));

    }

}
