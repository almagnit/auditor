import org.auditor.dao.CalendarDAO;
import org.auditor.service.AudienceService;
import org.auditor.service.allocation.AllocationService;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by almagnit on 09.03.14.
 */


@ContextConfiguration(locations = {"classpath:/spring/spring-model.xml", "classpath:/spring-test/spring-security.xml"})
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@RunWith(SpringJUnit4ClassRunner.class)
public class AllocationTest {

    @Autowired
    AllocationService allocationService;
    @Autowired
    AudienceService audienceService;
    @Autowired
    CalendarDAO calendarDAO;

    @Test
    public void testAllocationServiceByDate() throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String dateInString = "7-03-2014";
        Date date = formatter.parse(dateInString);
//        calendarDAO.getDatesFromDate(date);
//        allocationService.allocateAllFromDate(date);
    }

    @Test
    public void generateWeekAllocations(){
        DateTime dateTime = new DateTime();
        while (dateTime.dayOfWeek().get() < 6){
            allocationService.allocateForDate(dateTime.toLocalDate().toDate());
            dateTime = dateTime.plusDays(1);
        }
    }

}
