package org.auditor.service;

import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;
import org.auditor.entity.*;
import org.auditor.entity.security.Authority;
import org.auditor.entity.security.Group;
import org.auditor.entity.security.User;

import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */
public class Paging {

    public static class ForAuthority extends PagingLoadResultBean<Authority> {
        protected ForAuthority() {}
        public ForAuthority(List<Authority> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }

    public static class ForUser extends PagingLoadResultBean<User> {
        protected ForUser() {}
        public ForUser(List<User> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }

    public static class ForGroup extends PagingLoadResultBean<Group> {
        protected ForGroup() {}
        public ForGroup(List<Group> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }

    public static class ForTeacher extends PagingLoadResultBean<Teacher> {
        protected ForTeacher() {}
        public ForTeacher(List<Teacher> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }

    public static class ForTimetable extends PagingLoadResultBean<Timetable>{
        protected ForTimetable() {}
        public ForTimetable(List<Timetable> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }

    public static class ForAudience extends PagingLoadResultBean<Audience>{
        protected ForAudience() {}
        public ForAudience(List<Audience> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }

    public static class ForReplacement extends PagingLoadResultBean<Replacement>{
        protected ForReplacement() {}
        public ForReplacement(List<Replacement> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }

    public static class ForPriority extends PagingLoadResultBean<Priority>{
        protected ForPriority() {}
        public ForPriority(List<Priority> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }

    public static class ForFinalTable extends PagingLoadResultBean<FinalTable>{
        protected ForFinalTable() {}
        public ForFinalTable(List<FinalTable> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }

    public static class ForCalendar extends PagingLoadResultBean<Calendar>{
        protected ForCalendar() {}
        public ForCalendar(List<Calendar> list, int totalLength, int offset) {
            super(list, totalLength, offset);
        }
    }
}
