package org.auditor.service;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.entity.Priority;

import java.util.List;

public interface PriorityService extends CommonService<Priority> {
    public Paging.ForPriority list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);
    public void save(List<Priority> entities);
}
