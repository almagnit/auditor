package org.auditor.service;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.entity.Calendar;

import java.util.List;

public interface CalendarService extends CommonService<Calendar> {

    public Paging.ForCalendar list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);

}
