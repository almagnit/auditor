package org.auditor.service;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.entity.FinalTable;

import java.util.List;

public interface FinalTableService extends CommonService<FinalTable>{
    public Paging.ForFinalTable list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);
}
