package org.auditor.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.dao.security.GroupDAO;
import org.auditor.entity.security.Group;
import org.auditor.service.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class GroupServiceImpl extends SecurityServiceImpl<Group> implements GroupService {

    private GroupDAO groupDAO;

    @Autowired
    public void setGroupDAO(GroupDAO groupDAO){
        this.groupDAO = groupDAO;
        setDao(groupDAO);
    }

    @Override
    public Paging.ForGroup list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return new Paging.ForGroup(groupDAO.paginate(offset, limit, filterConfig, sortInfo), groupDAO.count(), offset);
    }

    @Override
    public boolean isLive() {
        return true;
    }
}
