package org.auditor.service.security.acl;

import org.auditor.entity.CommonEntity;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;

import java.util.List;

/**
 * @author almagnit@gmail.com
 */

public interface AclSecurityUtil {

    public void addPermissions(List<? extends CommonEntity> securedList, Permission permission, Class clazz);
    public void addPermission(CommonEntity securedObject, Permission permission, Class clazz);
    public void addPermission(CommonEntity securedObject, Sid recipient, Permission permission, Class clazz);
    public void deletePermission(CommonEntity securedObject, Sid recipient, Permission permission, Class clazz);

}
