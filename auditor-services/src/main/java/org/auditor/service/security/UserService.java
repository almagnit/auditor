package org.auditor.service.security;

import org.auditor.entity.security.User;
import org.auditor.service.Paging;
import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;

import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */
public interface UserService extends SecurityService<User> {

    public Paging.ForUser list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);

    public String getCurrentUser();

    public boolean isLive();

}
