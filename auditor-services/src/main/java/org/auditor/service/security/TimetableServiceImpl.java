package org.auditor.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.dao.TimetableDAO;
import org.auditor.entity.Timetable;
import org.auditor.service.CommonServiceImpl;
import org.auditor.service.Paging;
import org.auditor.service.TimetableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class TimetableServiceImpl extends CommonServiceImpl<Timetable> implements TimetableService {

    private TimetableDAO timetableDAO;

    @Autowired
    public void setTimetableDAO(TimetableDAO timetableDAO){
        this.timetableDAO = timetableDAO;
        setDao(timetableDAO);
    }

    @Override
    public Timetable add(Timetable entity) {
        return super.add(entity);
    }

    @Override
    public void delete(Timetable entity) {
        super.delete(entity);
    }

    @Override
    public void delete(Long id) {
        super.delete(id);
    }

    @Override
    public Timetable update(Timetable entity) {
        return super.update(entity);
    }

    @Override
    public Paging.ForTimetable list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return new Paging.ForTimetable(timetableDAO.paginate(offset, limit, filterConfig, sortInfo), timetableDAO.count(), offset);
    }
}
