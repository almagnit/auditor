package org.auditor.service.security;

import org.auditor.entity.security.User;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author almagnit@gmail.com
 *
 */

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class SecuritySessionServiceImpl implements SecuritySessionService {

    public SecuritySessionServiceImpl() {
    }

    public String getUsername(){
        SecurityContext contextHolder = SecurityContextHolder.getContext();
        return ((User)contextHolder.getAuthentication().getPrincipal()).getUsername();
    }

    public String getUserRole(){
        SecurityContext contextHolder = SecurityContextHolder.getContext();
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().iterator().next().getAuthority();
    }

    public void remove(String value) {
    }

    public void remove(Integer value) {
    }

}
