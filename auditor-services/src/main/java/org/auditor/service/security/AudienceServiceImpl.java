package org.auditor.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.dao.AudienceDAO;
import org.auditor.dao.PriorityDAO;
import org.auditor.entity.Audience;
import org.auditor.entity.Priority;
import org.auditor.service.AudienceService;
import org.auditor.service.CommonServiceImpl;
import org.auditor.service.Paging;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class AudienceServiceImpl extends CommonServiceImpl<Audience> implements AudienceService {

    private AudienceDAO audienceDAO;
    @Autowired
    private PriorityDAO priorityDAO;
    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    public void setAudienceDAO(AudienceDAO audienceDAO){
        this.audienceDAO = audienceDAO;
        setDao(audienceDAO);
    }

    @Override
    public void delete(Audience bean) {
        for(Priority priority : bean.getPriorities()){
            priorityDAO.delete(priority);
        }
        audienceDAO.delete(bean);
    }

    @Override
    public void delete(Long id) {
        super.delete(id);
    }

    @Override
    public Audience add(Audience entity) {
        sessionFactory.getCurrentSession().clear();
        for(Priority priority : entity.getPriorities()){
            priority.setPriority(priority.getPriority() - priority.getVersion());
            priorityDAO.add(priority);
        }
        return entity;
    }

    @Override
    public void deletePriority(Priority entity) {
        entity.getTeacher().getPriorities().remove(entity);
        entity.getAudience().getPriorities().remove(entity);
        priorityDAO.delete(entity);
    }

    @Override
    public Paging.ForAudience list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return new Paging.ForAudience(audienceDAO.paginate(offset, limit, filterConfig, sortInfo), audienceDAO.count(), offset);
    }
}
