package org.auditor.service.security;

import org.auditor.dao.security.SecurityDAO;
import org.auditor.entity.CommonEntity;

import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */
public interface SecurityService<E extends CommonEntity>{
    void setDao(SecurityDAO<E> dao);
    List<E> list();
    List<E> list(int offset, int limit);
    E update(E entity);
    E add(E entity);
    E create();
    E find(String value);
    E find(Long value);
    void delete(E entity);
    void delete(Long id);
}

