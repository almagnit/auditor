package org.auditor.service.security.acl;

import org.auditor.entity.security.Group;
import org.auditor.entity.security.User;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

import java.util.HashSet;
import java.util.Set;

/**
 * User: almagnit
 */

public class AuditorSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

    private Object filterObject;
    private Object returnObject;
    private Object target;

    public AuditorSecurityExpressionRoot(Authentication a) {
        super(a);
    }

    public boolean hasGroup(String groupname){
        return getGroups().contains(groupname);
    }

    public Set<String> getGroups(){
        User currentUser = (User) authentication.getPrincipal();
        Set<Group> groups = new HashSet<Group>();
        Set<String> groupTitles = new HashSet<String>();
        groups.addAll(currentUser.getGroups());
        groups.addAll(currentUser.getAuthority().getGroups());
        for(Group group : groups){
            groupTitles.add(group.getGroupName());
        }
        return groupTitles;
    }

    public void setFilterObject(Object filterObject) {
        this.filterObject = filterObject;
    }

    public Object getFilterObject() {
        return filterObject;
    }

    public void setReturnObject(Object returnObject) {
        this.returnObject = returnObject;
    }

    public Object getReturnObject() {
        return returnObject;
    }

    /**
     * Sets the "this" property for use in expressions. Typically this will be the "this" property of
     * the {@code JoinPoint} representing the method invocation which is being protected.
     *
     * @param target the target object on which the method in is being invoked.
     */
    void setThis(Object target) {
        this.target = target;
    }

    public Object getThis() {
        return target;
    }
}
