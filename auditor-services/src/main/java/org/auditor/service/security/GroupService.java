package org.auditor.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.entity.security.Group;
import org.auditor.service.Paging;

import java.util.List;

public interface GroupService extends SecurityService<Group> {

    public Paging.ForGroup list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);

    public boolean isLive();

}
