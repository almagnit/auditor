package org.auditor.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.dao.PriorityDAO;
import org.auditor.entity.Priority;
import org.auditor.service.CommonServiceImpl;
import org.auditor.service.Paging;
import org.auditor.service.PriorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class PriorityServiceImpl extends CommonServiceImpl<Priority> implements PriorityService {

    private PriorityDAO priorityDAO;

    @Autowired
    public void setPriorityDAO(PriorityDAO priorityDAO){
        this.priorityDAO = priorityDAO;
        setDao(priorityDAO);
    }

    @Override
    public void save(List<Priority> entities) {
        for(Priority priority : entities){
            priorityDAO.add(priority);
        }
    }

    @Override
    public void delete(Priority entity) {
        super.delete(entity);
    }

    @Override
    public Paging.ForPriority list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return new Paging.ForPriority(priorityDAO.paginate(offset, limit, filterConfig, sortInfo), priorityDAO.count(), offset);
    }

}
