package org.auditor.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.dao.CalendarDAO;
import org.auditor.entity.Calendar;
import org.auditor.service.CalendarService;
import org.auditor.service.CommonServiceImpl;
import org.auditor.service.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CalendarServiceImpl extends CommonServiceImpl<Calendar> implements CalendarService{

    private CalendarDAO calendarDAO;

    @Autowired
    public void setCalendarDAO(CalendarDAO calendarDAO){
        this.calendarDAO = calendarDAO;
        setDao(calendarDAO);
    }

    @Override
    public Paging.ForCalendar list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return new Paging.ForCalendar(calendarDAO.paginate(offset, limit, filterConfig, sortInfo), calendarDAO.count(), offset);
    }

}
