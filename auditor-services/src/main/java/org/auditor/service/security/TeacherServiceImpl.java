package org.auditor.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.dao.TeacherDAO;
import org.auditor.entity.Teacher;
import org.auditor.service.CommonServiceImpl;
import org.auditor.service.Paging;
import org.auditor.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by almagnit on 15.01.14.
 */

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class TeacherServiceImpl extends CommonServiceImpl<Teacher> implements TeacherService {

    private TeacherDAO teacherDAO;

    @Autowired
    public void setTeacherDAO(TeacherDAO teacherDAO) {
        this.teacherDAO = teacherDAO;
        setDao(teacherDAO);
    }


    public Paging.ForTeacher list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return new Paging.ForTeacher(teacherDAO.paginate(offset, limit, filterConfig, sortInfo), teacherDAO.count(), offset);
    }


}
