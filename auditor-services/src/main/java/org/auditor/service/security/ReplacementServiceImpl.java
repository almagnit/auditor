package org.auditor.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.dao.ReplacementDAO;
import org.auditor.entity.Replacement;
import org.auditor.service.CommonServiceImpl;
import org.auditor.service.Paging;
import org.auditor.service.ReplacementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class ReplacementServiceImpl extends CommonServiceImpl<Replacement> implements ReplacementService{

    private ReplacementDAO replacementDAO;

    @Autowired
    public void setReplacementDAO(ReplacementDAO replacementDAO){
        this.replacementDAO = replacementDAO;
        setDao(replacementDAO);
    }

    @Override
    public Replacement update(Replacement entity) {
        return super.update(entity);
    }

    @Override
    public Replacement add(Replacement entity) {
        return super.add(entity);
    }

    @Override
    public void delete(Long id) {
        super.delete(id);
    }

    @Override
    public void delete(Replacement entity) {
        super.delete(entity);
    }

    @Override
    public Paging.ForReplacement list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return new Paging.ForReplacement(replacementDAO.paginate(offset, limit, filterConfig, sortInfo), replacementDAO.count(), offset);
    }
}
