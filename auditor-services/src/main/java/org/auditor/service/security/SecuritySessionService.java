package org.auditor.service.security;

/**
 * @author almagnit@gmail.com
 *
 */

public interface SecuritySessionService {

    public String getUsername();

    
    public String getUserRole();

    
    public void remove(String value);

    
    public void remove(Integer value);

}
