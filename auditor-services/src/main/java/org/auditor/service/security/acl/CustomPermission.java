package org.auditor.service.security.acl;

import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.model.Permission;

/**
 * @author almagnit@gmail.com
 */
public class CustomPermission extends BasePermission {

    
    public static final Permission ADMIN_READ = new CustomPermission(1 << 5, 'M');

    public CustomPermission(int mask, char code) {
        super(mask, code);
    }

    private static final long serialVersionUID = -7695655824830259000L;
}
