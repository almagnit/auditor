package org.auditor.service.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.dao.FinalTableDAO;
import org.auditor.entity.FinalTable;
import org.auditor.service.CommonServiceImpl;
import org.auditor.service.FinalTableService;
import org.auditor.service.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class FinalTableServiceImpl extends CommonServiceImpl<FinalTable> implements FinalTableService{

    private FinalTableDAO finalTableDAO;

    @Autowired
    public void setFinalTableDAO(FinalTableDAO finalTableDAO){
        this.finalTableDAO = finalTableDAO;
    }

    @Override
    public Paging.ForFinalTable list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return new Paging.ForFinalTable(finalTableDAO.paginate(offset, limit, filterConfig, sortInfo), finalTableDAO.count(), offset);
    }
}
