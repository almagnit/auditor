package org.auditor.service;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.entity.Timetable;

import java.util.List;

public interface TimetableService extends CommonService<Timetable> {

    public Paging.ForTimetable list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);

}
