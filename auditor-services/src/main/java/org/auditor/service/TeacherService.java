package org.auditor.service;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.entity.Teacher;

import java.util.List;

/**
 * Created by almagnit on 15.01.14.
 */

public interface TeacherService extends CommonService<Teacher> {

    public Paging.ForTeacher list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);

}
