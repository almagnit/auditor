package org.auditor.service;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.entity.Replacement;

import java.util.List;

public interface ReplacementService extends CommonService<Replacement> {

    public Paging.ForReplacement list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);

}
