package org.auditor.service;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.entity.Audience;
import org.auditor.entity.Priority;

import java.util.List;

public interface AudienceService extends CommonService<Audience> {

    public Paging.ForAudience list(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);
    public void deletePriority(Priority entity);
}
