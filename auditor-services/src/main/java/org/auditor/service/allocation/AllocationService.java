package org.auditor.service.allocation;

import org.auditor.dao.AudienceDAO;
import org.auditor.dao.CalendarDAO;
import org.auditor.dao.ReplacementDAO;
import org.auditor.dao.TimetableDAO;
import org.auditor.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by almagnit on 09.03.14.
 */

@Service
public class AllocationService {

    @Autowired
    private TimetableDAO timetableDAO;
    @Autowired
    private AudienceDAO audienceDAO;
    @Autowired
    private ReplacementDAO replacementDAO;
    @Autowired
    private CalendarDAO calendarDAO;

    public void allocateAllFromDate(Date date){
        List<Date> days = calendarDAO.getDatesFromDate(date);
        for (Date d : days){
            allocateForDate(d);
        }
    }

    public void allocateForDate(Date date){
        Calendar calendar = calendarDAO.findByDate(date);
        List<Timetable> timetables = timetableDAO.listByDate(date);
        List<Replacement> replacements = replacementDAO.listByDate(date);
        calendar.getFinalTables().clear();
        /* Apply replacements */
        for (Replacement replacement : replacements){
            for(Timetable timetable : timetables){
                if(timetable.getLesson().equals(replacement.getLesson()) && timetable.getGroup().equals(replacement.getGroup())){
                    timetable.setTeacher(replacement.getTeacher());
                }
            }
        }
        /* Create priority map */
        TreeMap<Integer, TreeMap<Integer, List<FinalTable>>> map = new TreeMap<>();
        for(int i = 1; i < 6; i++){
            map.put(i, new TreeMap<Integer, List<FinalTable>>());
        }int j = 0;
        for(Timetable timetable : timetables){
            int teacherMaxPriority = timetable.getTeacher().getPriorities().size() == 0? 0 : timetable.getTeacher().getPriorities().get(0).getPriority();
            if(!map.get(timetable.getLesson()).containsKey(teacherMaxPriority))map.get(timetable.getLesson()).put(teacherMaxPriority, new ArrayList<FinalTable>());
            FinalTable finalTable = new FinalTable();
            finalTable.setTeacher(timetable.getTeacher());
            finalTable.setLesson(timetable.getLesson());
            finalTable.setGroup(timetable.getGroup());
            map.get(timetable.getLesson()).get(teacherMaxPriority).add(finalTable);
        }
        /* Fill allocations from map */
        List<Audience> audiences = audienceDAO.list();
        for (int i=1; i<6; i++){
            List<Audience> audienceList = new ArrayList<>(audiences);
            for(Integer priority : map.get(i).descendingKeySet()){
                for (FinalTable finalTable : map.get(i).get(priority)){
                    for(Priority prior : finalTable.getTeacher().getPriorities()){
                        Audience audience = prior.getAudience();
                        if(audienceList.contains(audience)){
                            finalTable.setAudience(audience);
                            finalTable.setPriority(priority);
                            audienceList.remove(audience);
                            break;
                        }
                    }
                    if(finalTable.getAudience() == null){
                        finalTable.setAudience(audienceList.get(0));
                        finalTable.setPriority(0);
                        audienceList.remove(0);
                    }
                    finalTable.setCalendar(calendar);
                    calendar.getFinalTables().add(finalTable);
                }
            }
        }
        /* Save allocations */
        calendarDAO.add(calendar);
    }

}
