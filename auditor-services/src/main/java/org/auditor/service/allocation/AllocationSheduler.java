package org.auditor.service.allocation;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Created by almagnit on 10.03.14.
 */

@Service
@EnableScheduling
public class AllocationSheduler {

    @Autowired
    AllocationService allocationService;
    private boolean started = false;
    @Scheduled(cron = "0 0 0 * * MON")
    public void generateWeekAllocations(){
        if(started)return;
        started = true;
        DateTime dateTime = new DateTime();
        while (dateTime.dayOfWeek().get() < 6){
            allocationService.allocateForDate(dateTime.toLocalDate().toDate());
            dateTime = dateTime.plusDays(1);
        }
        started = false;
    }
}
