package org.auditor.service.allocation;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by almagnit on 09.03.14.
 */

@Aspect
@Component
public class AllocationAdvisor {

    @Autowired
    AllocationService allocationService;
    @Autowired
    SessionFactory sessionFactory;

    @After("execution(* org.auditor.service.AudienceService+.add(..)) ||" +
            "execution(* org.auditor.service.AudienceService+.delete(..)) ||" +
            "execution(* org.auditor.service.TimetableService+.add(..)) ||" +
            "execution(* org.auditor.service.TimetableService+.delete(..)) ||" +
            "execution(* org.auditor.service.TimetableService+.update(..)) ||" +
            "execution(* org.auditor.service.ReplacementService+.add(..)) ||" +
            "execution(* org.auditor.service.ReplacementService+.delete(..)) ||" +
            "execution(* org.auditor.service.ReplacementService+.update(..))")
    public void afterNestedEntitiesChange(){
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        allocationService.allocateAllFromDate(new DateTime().toLocalDate().toDate());
    }
}
