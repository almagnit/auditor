package org.auditor.service;

import org.auditor.dao.CommonDAO;
import org.auditor.entity.CommonEntity;

import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */
public interface CommonService<E extends CommonEntity> {
    void setDao(CommonDAO<E> dao);
    List<E> list();
    List<E> list(int offset, int limit);
    E update(E entity);
    E add(E entity);
    E create();
    E find(String value);
    E find(Long value);
    void delete(E entity);
    void delete(Long id);
}

