package org.auditor.dao;

import org.auditor.entity.Audience;

public interface AudienceDAO extends CommonDAO<Audience> {
}
