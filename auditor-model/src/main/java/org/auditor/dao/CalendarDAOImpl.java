package org.auditor.dao;

import org.auditor.entity.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class CalendarDAOImpl extends CommonDAOImpl<Calendar> implements CalendarDAO {

    @Autowired
    public CalendarDAOImpl(Calendar entity){
        super(entity);
    }

    public Calendar findByDate(Date date){
        Calendar calendar = (Calendar) sessionFactory.getCurrentSession().
                                         createQuery("from Calendar where date = :date").
                                         setDate("date", date).uniqueResult();
        if(calendar == null){
            calendar = new Calendar();
            calendar.setDate(date);
            return calendar;
        }else{
            calendar.getFinalTables().clear();
            return add(calendar);
        }
    }

    public List<Date> getDatesFromDate(Date date){
        return sessionFactory.getCurrentSession().createSQLQuery("select date from calendar").list();
    }
}
