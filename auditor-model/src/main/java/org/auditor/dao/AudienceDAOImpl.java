package org.auditor.dao;

import org.auditor.entity.Audience;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class AudienceDAOImpl extends CommonDAOImpl<Audience> implements AudienceDAO {

    @Autowired
    public AudienceDAOImpl(Audience entity){
        super(entity);
    }

    @Override
    public List<Audience> list() {
        Criteria criteria = getCriteria();
        criteria.addOrder(Order.asc("numberAudience"));
        return new ArrayList<>(new LinkedHashSet<Audience>(criteria.list()));
    }
}