package org.auditor.dao;

import org.auditor.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by almagnit on 15.01.14.
 */

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class TeacherDAOImpl extends CommonDAOImpl<Teacher> implements TeacherDAO {

    @Autowired
    public TeacherDAOImpl(Teacher entity) {
        super(entity);
    }

}
