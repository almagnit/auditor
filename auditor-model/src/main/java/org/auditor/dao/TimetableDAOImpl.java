package org.auditor.dao;

import org.auditor.entity.Timetable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class TimetableDAOImpl extends CommonDAOImpl<Timetable> implements TimetableDAO {

    @Autowired
    public TimetableDAOImpl(Timetable entity){
        super(entity);
    }

    public List<Timetable> listByDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return listByDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK)-1);
    }

    public List<Timetable> listByDayOfWeek(int dayOfWeek){
        return sessionFactory.getCurrentSession().
                createQuery("from Timetable where day = :dayOfWeek order by lesson").
                setInteger("dayOfWeek", dayOfWeek).list();
    }



}
