package org.auditor.dao;

import org.auditor.entity.CommonEntity;
import com.sencha.gxt.data.shared.SortDir;
import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public abstract class CommonDAOImpl<T extends CommonEntity> implements CommonDAO<T>{

    public  T entity;

    @Autowired
    public  SessionFactory sessionFactory;

    protected CommonDAOImpl() {
    }

    protected CommonDAOImpl(T entity) {
        this.entity = entity;
    }

    public  void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public  Criteria getCriteria(){
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(entity.getClass());
        criteria.setCacheable(true);
        return criteria;
    }

    public T createEntity(){
        return entity;
    }

    public T add(T bean) {
        sessionFactory.getCurrentSession().saveOrUpdate(bean);
        return bean;
    }

    @Override
    public void delete(T bean) {
        sessionFactory.getCurrentSession().delete(bean);
    }

    @Override
    public void delete(Long id) {
        String deleteById  = "delete from "+entity.getClass().getSimpleName()+" where id= :classId";
        sessionFactory.getCurrentSession().createQuery(deleteById).setLong("classId", id).executeUpdate();
    }

    public  List<T> list() {
        List list = getCriteria().list();
        return list;
    }

    public  int count(){
        return Integer.parseInt(getCriteria().setProjection(Projections.rowCount()).uniqueResult()+"");
    }

    public  T findByKey(String key, Serializable value) {
        return (T)getCriteria().add(Restrictions.eq(key, value)).uniqueResult();
    }

    public  T findById(Serializable id) {
        return (T)sessionFactory.getCurrentSession().get(entity.getClass(), id);
    }

    public  T loadById(Serializable id){
        return (T)sessionFactory.getCurrentSession().load(entity.getClass(), id);
    }

    public  T update(T bean) {
        sessionFactory.getCurrentSession().saveOrUpdate(bean);
        return bean;
    }

    public  List<Predicate> condition(CriteriaBuilder cb, Root<T> r, List<FilterConfigBean> filterConfig){
        List<Predicate> predicates = new ArrayList<Predicate>();
        return predicates;
    }


    public  List<T> list(int offset, int limit) {
        Criteria listBean = getCriteria();
        listBean.setFirstResult(offset);
        listBean.setMaxResults(limit);
        return listBean.list();
    }

//    @Secured("hasAuthority('ROLE_ADMIN')")
    public  List<T> paginate(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        Criteria listBean = getCriteria();
        listBean.setFirstResult(offset);
        listBean.setMaxResults(limit);
        for(FilterConfigBean filter : filterConfig){
            if("contains".equals(filter.getComparison())){
                listBean.add(Restrictions.like(filter.getField(), filter.getValue()));
            }
            if("gt".equals(filter.getComparison())){
                listBean.add(Restrictions.gt(filter.getField(), Integer.valueOf(filter.getValue())));
            }
            if("lt".equals(filter.getComparison())){
                listBean.add(Restrictions.lt(filter.getField(), Integer.valueOf(filter.getValue())));
            }
            if("eq".equals(filter.getComparison())){
                listBean.add(Restrictions.eq(filter.getField(), filter.getValue()));
            }
            if("on".equals(filter.getComparison())){

            }
            if("after".equals(filter.getComparison())){
                //TODO filter for "after" case
            }
            if("before".equals(filter.getComparison())){
                //TODO filter for "before" case
            }
        }
        if(sortInfo.size() == 0)
            listBean.addOrder(Order.asc("id"));
        else{
            if(sortInfo.get(0).getSortDir() == SortDir.ASC)
                listBean.addOrder(Order.asc(sortInfo.get(0).getSortField()));
            if(sortInfo.get(0).getSortDir() == SortDir.DESC)
                listBean.addOrder(Order.asc(sortInfo.get(0).getSortField()));
        }
        return listBean.list();
    }

}
