package org.auditor.dao;

import org.auditor.entity.Calendar;

import java.util.Date;
import java.util.List;

public interface CalendarDAO extends CommonDAO<Calendar> {

    public Calendar findByDate(Date date);
    public List<Date> getDatesFromDate(Date date);

}
