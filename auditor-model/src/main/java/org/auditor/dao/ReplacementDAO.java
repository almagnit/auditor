package org.auditor.dao;

import org.auditor.entity.Replacement;

import java.util.Date;
import java.util.List;

public interface ReplacementDAO extends CommonDAO<Replacement> {

    public List<Replacement> listByDate(Date date);

}
