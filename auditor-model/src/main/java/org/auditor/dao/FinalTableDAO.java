package org.auditor.dao;

import org.auditor.entity.FinalTable;

public interface FinalTableDAO extends CommonDAO<FinalTable> {
}
