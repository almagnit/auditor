package org.auditor.dao;

import org.auditor.entity.Teacher;

/**
 * Created by almagnit on 15.01.14.
 */
public interface TeacherDAO extends CommonDAO<Teacher> {}
