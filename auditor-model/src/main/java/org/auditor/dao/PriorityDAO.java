package org.auditor.dao;

import org.auditor.entity.Priority;

public interface PriorityDAO extends CommonDAO<Priority> {
}
