package org.auditor.dao;

import org.auditor.entity.FinalTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class FinalTableDAOImpl extends CommonDAOImpl<FinalTable> implements FinalTableDAO {

    @Autowired
    public FinalTableDAOImpl(FinalTable entity){
        super(entity);
    }

}
