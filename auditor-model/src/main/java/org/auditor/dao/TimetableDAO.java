package org.auditor.dao;

import org.auditor.entity.Timetable;

import java.util.Date;
import java.util.List;

public interface TimetableDAO extends CommonDAO<Timetable> {

    public List<Timetable> listByDate(Date date);
    public List<Timetable> listByDayOfWeek(int dayOfWeek);

}
