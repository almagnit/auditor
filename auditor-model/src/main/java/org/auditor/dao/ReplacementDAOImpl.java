package org.auditor.dao;

import org.auditor.entity.Calendar;
import org.auditor.entity.Replacement;
import org.auditor.entity.Timetable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class ReplacementDAOImpl extends CommonDAOImpl<Replacement> implements ReplacementDAO{

    @Autowired
    public ReplacementDAOImpl(Replacement entity) {
        super(entity);
    }

    @Override
    public List<Replacement> listByDate(Date date) {
        return sessionFactory.getCurrentSession().
                createQuery("from Replacement where date = :date").
                setDate("date", date).list();
    }
}
