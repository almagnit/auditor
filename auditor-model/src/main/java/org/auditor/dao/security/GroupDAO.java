package org.auditor.dao.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.entity.security.Group;

import java.util.List;

public interface GroupDAO extends SecurityDAO<Group> {
    public List<Group> paginate(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);
}
