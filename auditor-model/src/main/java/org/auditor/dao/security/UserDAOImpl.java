package org.auditor.dao.security;


import org.auditor.entity.security.User;
import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */

@Repository
@Transactional(value = "transactionManager", propagation=Propagation.REQUIRED)
public class UserDAOImpl extends SecurityDAOImpl<User> implements UserDAO {

    @Autowired
    public UserDAOImpl(User entity) {
        super(entity);
    }

    @Override
    public List<User> paginate(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return super.paginate(offset, limit, filterConfig, sortInfo);
    }

    @Override
    public User findByUsername(String username) {
        return (User) getCriteria().add(Restrictions.eq("username", username)).uniqueResult();
    }
}
