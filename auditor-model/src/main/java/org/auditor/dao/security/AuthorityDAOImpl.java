package org.auditor.dao.security;


import org.auditor.entity.security.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author almagnit@gmail.com
 *
 */
@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class AuthorityDAOImpl extends SecurityDAOImpl<Authority> implements AuthorityDAO {

    @Autowired
    public AuthorityDAOImpl(Authority entity) {
        super(entity);
    }
}
