package org.auditor.dao.security;

import org.auditor.entity.security.Authority;

/**
 * @author almagnit@gmail.com
 *
 */
public interface AuthorityDAO extends SecurityDAO<Authority> {}
