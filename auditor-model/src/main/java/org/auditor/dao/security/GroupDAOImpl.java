package org.auditor.dao.security;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.auditor.entity.security.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class GroupDAOImpl extends SecurityDAOImpl<Group> implements GroupDAO {

    @Autowired
    public GroupDAOImpl(Group entity) {
        super(entity);
    }

    @Override
    public List<Group> paginate(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo) {
        return super.paginate(offset, limit, filterConfig, sortInfo);
    }

}
