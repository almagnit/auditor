package org.auditor.dao.security;

import org.auditor.entity.security.User;
import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;

import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */
public interface UserDAO extends SecurityDAO<User> {

    public List<User> paginate(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);

    public User findByUsername(String username);
}
