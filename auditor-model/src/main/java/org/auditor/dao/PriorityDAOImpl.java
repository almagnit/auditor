package org.auditor.dao;

import org.auditor.entity.Priority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
public class PriorityDAOImpl extends CommonDAOImpl<Priority> implements PriorityDAO {

    @Autowired
    public PriorityDAOImpl(Priority entity){
        super(entity);
    }

    @Override
    public void delete(Priority bean) {
        sessionFactory.getCurrentSession().clear();
        super.delete(bean);
    }

    @Override
    public Priority add(Priority bean) {
        sessionFactory.getCurrentSession().saveOrUpdate(bean);
        return bean;
    }

}
