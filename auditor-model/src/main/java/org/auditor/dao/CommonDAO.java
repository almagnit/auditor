package org.auditor.dao;

import com.sencha.gxt.data.shared.SortInfoBean;
import com.sencha.gxt.data.shared.loader.FilterConfigBean;
import org.hibernate.Criteria;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

/**
 * @author almagnit@gmail.com
 *
 */

public interface CommonDAO<T> {

    T add(T bean);

    T update(T bean);

    void delete(T bean);

    void delete(Long id);

    T findByKey(String key, Serializable value);

    T findById(Serializable id);

    T loadById(Serializable id);

    Criteria getCriteria();

    T createEntity();

    List<T> list();

    List<T> list(int offset, int limit);

    List<T> paginate(int offset, int limit, List<FilterConfigBean> filterConfig, List<SortInfoBean> sortInfo);

    List<Predicate> condition(CriteriaBuilder cb, Root<T> r, List<FilterConfigBean> filterConfig);

    int count();

}
