package org.auditor;

import org.auditor.dao.AudienceDAO;
import org.auditor.dao.PriorityDAO;
import org.auditor.dao.ReplacementDAO;
import org.auditor.dao.TimetableDAO;
import org.auditor.entity.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GeneratorFinalTable {

    private PriorityDAO priorityDAO;
    private TimetableDAO timetableDAO;
    private AudienceDAO audienceDAO;
    private List<FinalTable> finalTables =
            new ArrayList<FinalTable>();
    private ReplacementDAO replacementDAO;
    private Date date;

    private Calendar calendar;

    public GeneratorFinalTable(Date date){
        this.date = date;
    }

    public GeneratorFinalTable(PriorityDAO priorityDAO, TimetableDAO timetableDAO, AudienceDAO audienceDAO, ReplacementDAO replacementDAO, Date date){
        this.priorityDAO = priorityDAO;
        this.timetableDAO = timetableDAO;
        this.audienceDAO = audienceDAO;
        this.replacementDAO = replacementDAO;
        this.date = date;
    }

    public PriorityDAO getPriorityDAO(){
        return priorityDAO;
    }
    public void setPriorityDAO(PriorityDAO priorityDAO){
        this.priorityDAO = priorityDAO;
    }

    public TimetableDAO getTimetableDAO(){
        return timetableDAO;
    }
    public void setTimetableDAO(TimetableDAO timetableDAO){
        this.timetableDAO = timetableDAO;
    }

    public AudienceDAO getAudienceDAO(){
        return audienceDAO;
    }
    public void setAudienceDAO(AudienceDAO audienceDAO){
        this.audienceDAO = audienceDAO;
    }

    public Date getDate(){
        return date;
    }
    public void setDate(Date date){
        this.date = date;
    }

    public List<FinalTable> getFinalTables(){
        return finalTables;
    }
    public void setFinalTables(List<FinalTable> finalTables){
        this.finalTables = finalTables;
    }

    public void setReplacementDAO(ReplacementDAO replacementDAO){
        this.replacementDAO = replacementDAO;
    }
    public ReplacementDAO getReplacementDAO(){
        return replacementDAO;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public void alignmentAudience(){

        List<Priority> priorities = new ArrayList<Priority>();
        List<Audience> audiences = audienceDAO.list();
        List<Audience> audiencesCopy = new ArrayList<Audience>();
        List<CommonTimetable> timetables = new ArrayList<CommonTimetable>();
        List<CommonTimetable> timetableCopy = new ArrayList<CommonTimetable>();

        boolean replacementTeacher;

        for (Timetable timetable : timetableDAO.list()){
            replacementTeacher = false;
            if(timetable.getDay() == date.getDay()) {

                for(Replacement replacement : replacementDAO.list()){
                    if(replacement.getDate().equals(date) && timetable.getLesson() == replacement.getLesson()){
                        if(replacement.getGroup().getId() == timetable.getGroup().getId()){
                            System.out.println("ttttttttttttt");
                            timetables.add(replacement);
                            timetableCopy.add(replacement);
                            replacementTeacher = true;
                            break;
                        }
                    }
                }
                if(!replacementTeacher){
                    timetables.add(timetable);
                    timetableCopy.add(timetable);
                }
            }
        }

        for (Priority priority : priorityDAO.list()){
            for (CommonTimetable timetable : timetables){
                if(priority.getTeacher().getId() == timetable.getTeacher().getId()){
                    priorities.add(priority);
                    break;
                }
            }
        }

        Priority priorityMax;
        List<Priority> prioritiesTeacher = new ArrayList<Priority>();
        boolean deletePriority;

        for(int i=1; i<=5; i++){

            audiencesCopy.clear();
            for(Audience audience : audiences){
                audiencesCopy.add(audience);
            }
            timetableCopy.clear();
            for(CommonTimetable timetable : timetables){
                timetableCopy.add(timetable);
            }

            for(CommonTimetable timetable : timetables){
                if(timetable.getLesson() == i){
                    prioritiesTeacher = timetable.getTeacher().getPriorities();
                    while(prioritiesTeacher.size() != 0){

                        deletePriority = false;
                        priorityMax = prioritiesTeacher.get(0);

                        for(Priority priority : prioritiesTeacher){
                            if(priorityMax.getPriority() < priority.getPriority()){
                                priorityMax = priority;
                            }
                        }

                        for(CommonTimetable timetableTeacher : timetables){
                            for(Priority priority : timetableTeacher.getTeacher().getPriorities()){
                                if(timetable.getLesson() == timetableTeacher.getLesson() &&
                                        timetable.getTeacher().getId() != timetableTeacher.getTeacher().getId()){
                                    if(priority.getAudience().getId() == priorityMax.getAudience().getId()){
                                        if(priority.getPriority() > priorityMax.getPriority()){
                                            deletePriority = true;
                                        }
                                    }
                                }
                            }
                        }

                        if(!deletePriority) {
                            /*
                            Calendar calendar = new Calendar();
                            calendar.setDate(date);
                            calendar.setUuid(randomUUID().toString());
                            */
                            FinalTable finalTable = new FinalTable();
                            finalTable.setLesson(timetable.getLesson());
                            finalTable.setGroup(timetable.getGroup());
                            finalTable.setTeacher(timetable.getTeacher());
                            finalTable.setAudience(priorityMax.getAudience());
                            finalTable.setPriority(priorityMax.getPriority());
                            finalTable.setCalendar(calendar);

                            finalTables.add(finalTable);

                            timetableCopy.remove(timetable);
                            audiencesCopy.remove(priorityMax.getAudience());
                            break;
                        }else{
                            prioritiesTeacher.remove(priorityMax);
                            System.out.println("Remove");
                        }
                    }
                }
            }


            for(CommonTimetable timetable : timetableCopy){

                    /*
                    Calendar calendar = new Calendar();
                    calendar.setDate(date);
                    calendar.setUuid(randomUUID().toString());
                    */

                if(timetable.getLesson() == i){

                    FinalTable finalTable = new FinalTable();
                    finalTable.setLesson(timetable.getLesson());
                    finalTable.setGroup(timetable.getGroup());
                    finalTable.setTeacher(timetable.getTeacher());
                    finalTable.setAudience(audiencesCopy.get(0));
                    finalTable.setPriority(0);
                    finalTable.setCalendar(calendar);

                    finalTables.add(finalTable);

                    audiencesCopy.remove(0);
                }
            }
        }
    }

}
