package org.auditor.entity;

import org.auditor.entity.security.Group;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name="timetable")
@Entity
@Component
@Scope(value = "prototype")
public class Timetable extends CommonTimetable{

    private Long id;
    private Teacher teacher;
    private Integer lesson;
    private Integer day;
    private String subject;
    private Group group;

    @Column(name = "id")
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "teacher_id")
    public Teacher getTeacher(){
        return teacher;
    }
    public void setTeacher(Teacher teacher){
        this.teacher = teacher;
    }

    @Column(name = "lesson")
    public Integer getLesson(){
        return lesson;
    }
    public void setLesson(Integer lesson){
        this.lesson = lesson;
    }

    @Column(name = "day")
    public Integer getDay(){
        return day;
    }
    public void setDay(Integer day){
        this.day = day;
    }

    @Column(name = "subject")
    public String getSubject(){
        return subject;
    }
    public void setSubject(String subject){
        this.subject = subject;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "group_id")
    public Group getGroup() {
        return group;
    }


    public void setGroup(Group group) {
        this.group = group;
    }

    @Transient
    public String getLabel(){
        return "CommonEntity";
    }

    @Transient
    public List<CommonEntity> getChildren(){
        return new ArrayList<>();
    }
}
