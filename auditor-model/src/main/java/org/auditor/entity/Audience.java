package org.auditor.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Table(name="audience")
@Entity
@Component
@Scope(value = "prototype")
public class Audience extends CommonEntity implements Comparable<Audience> {

    private Long id;
    private Integer numberAudience;
    private String nameAudience;

    List<Priority> priorities = new ArrayList<Priority>();

    @Column(name = "id")
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "number_audience")
    public Integer getNumberAudience(){
        return numberAudience;
    }
    public void setNumberAudience(Integer numberAudience){
        this.numberAudience = numberAudience;
    }

    @Column(name = "name_audience")
    public String getNameAudience(){
        return nameAudience;
    }
    public void setNameAudience(String nameAudience){
        this.nameAudience = nameAudience;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "priorityId.audience")
    @OrderBy("priority asc")
    public List<Priority> getPriorities() {
        List list = new ArrayList<>(priorities);
        Collections.sort(list);
        return list;
    }
    public void setPriorities(List<Priority> priorities) {
        this.priorities =priorities;
    }

    @Transient
    public String getLabel(){
        return getNameAudience();
    }

    @Transient
    public List<CommonEntity> getChildren(){
        ArrayList<CommonEntity> teachers = new ArrayList<>();
        for(Priority priority : getPriorities()){
            priority.setAudience(this);
            teachers.add(priority);
        }
        return teachers;
    }


    @Override
    public int compareTo(Audience o) {
        return o.numberAudience < this.numberAudience ? -1 : o.numberAudience > this.numberAudience ? 1 : 0;
    }
}
