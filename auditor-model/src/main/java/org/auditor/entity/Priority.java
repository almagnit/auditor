package org.auditor.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "priority") @Entity
@Component @Scope(value = "prototype")
@AssociationOverrides({
        @AssociationOverride(name = "priorityId.audience",
                joinColumns = @JoinColumn(name = "audience_id")),
        @AssociationOverride(name = "priorityId.teacher",
                joinColumns = @JoinColumn(name = "teacher_id")) })
public class Priority extends CommonEntity implements Comparable<Priority> {

    private Integer priority = 0;
    private PriorityId priorityId = new PriorityId();

    @EmbeddedId
    public PriorityId getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(PriorityId priorityId) {
        this.priorityId = priorityId;
    }

    @Column(name = "priority")
    public Integer getPriority() {
        return priority;
    }
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Transient
    public Teacher getTeacher(){
        return priorityId.getTeacher();
    }

    public void setTeacher(Teacher teacher){
        priorityId.setTeacher(teacher);
    }

    @Transient
    public Audience getAudience(){
        return priorityId.getAudience();
    }

    public void setAudience(Audience audience){
        priorityId.setAudience(audience);
    }

    @Transient
    public String getLabel(){
        return getTeacher().getName();
    }

    @Transient
    public List<CommonEntity> getChildren(){
        return new ArrayList<>();
    }

    @Override
    public int compareTo(Priority o) {
        return o.priority < this.priority ? -1 : o.priority > this.priority ? 1 : 0;
    }
}
