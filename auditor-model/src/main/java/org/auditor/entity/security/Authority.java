package org.auditor.entity.security;

import org.auditor.entity.CommonEntity;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author almagnit
 *
 */
@Table(name="authorities")
@Entity
@Component @Scope(value = "prototype")
public class Authority extends CommonEntity {
    private String username;
    private String authority;
    private User user;

    private Set<Group> groups;

    @javax.persistence.Column(name = "username")
    @Id
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @javax.persistence.Column(name = "authority")
    @Basic
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "authority")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToMany(targetEntity = Group.class, fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "group_members",
            joinColumns = {@JoinColumn(name = "username") },
            inverseJoinColumns = { @JoinColumn(name = "group_id") })
    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Authority that = (Authority) o;

        if (authority != null ? !authority.equals(that.authority) : that.authority != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 31 * (username != null ? username.hashCode() : 0);
        result = 31 * result + (authority != null ? authority.hashCode() : 0);
        return result;
    }

    @Transient
    public String getClassName(){
        return this.getClass().getName();
    }

    @Transient
    public String getClassTitle(){
        return "Роль";
    }

    @Transient
    public String getLabel(){
        return "CommonEntity";
    }

    @Transient
    public List<CommonEntity> getChildren(){
        return new ArrayList<>();
    }
}
