package org.auditor.entity.security;

import org.auditor.entity.CommonEntity;
import org.hibernate.annotations.Cascade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author almagnit
 *
 */
@Table(name="users")
@Entity
@Component @Scope(value = "prototype")
public class User extends CommonEntity implements UserDetails{

    private String username;
    private String password;
    private Boolean enabled;
    private Authority authority;
    private Set<Group> groups;

    @javax.persistence.Column(name = "username", updatable = false, unique = true)
    @Id
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @javax.persistence.Column(name = "password")
    @Basic
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @javax.persistence.Column(name = "enabled")
    public Boolean getEnabled() {
        return enabled == null ? false : enabled;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }


    @OneToOne(fetch = FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "username")
    public Authority getAuthority() {
        return authority;
    }

    @Autowired
    public void setAuthority(Authority authority) {
        this.authority = authority;
    }

    @OneToMany(targetEntity = Group.class, fetch = FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinTable(name = "group_members",
            joinColumns = {@JoinColumn(name = "username") },
            inverseJoinColumns = { @JoinColumn(name = "group_id") })
    public Set<Group> getGroups() {
        return groups;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    @Override @Transient
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override @Transient
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override @Transient
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(authority.getAuthority()));
        return authorities;
    }

    @Override @Transient
    public boolean isEnabled() {
        return getEnabled();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (enabled != null ? !enabled.equals(user.enabled) : user.enabled != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (username != null ? !username.equals(user.username) : user.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 31 * (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        return result;
    }

    @Transient
    public String getClassName(){
        return this.getClass().getName();
    }

    @Transient
    public String getClassTitle(){
        return "Роль";
    }

    @Transient
    public String getLabel(){
        return "CommonEntity";
    }

    @Transient
    public List<CommonEntity> getChildren(){
        return new ArrayList<>();
    }
}
