package org.auditor.entity.security;

import org.auditor.entity.CommonEntity;
import org.auditor.entity.Replacement;
import org.auditor.entity.Timetable;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User: almagnit
 */

@Table(name = "training_group")
@Entity
@Component
public class Group extends CommonEntity {

    private Long id;
    private String groupName;
    private List<Timetable> timetables = new ArrayList<Timetable>();
    private List<Replacement> replacements = new ArrayList<Replacement>();

    @Column(name = "id", updatable = false, insertable = false)
    @Id @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="group_name")
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "group")
    @Transient
    public List<Timetable> getTimetables(){
        return timetables;
    }
    public void setTimetables(List<Timetable> timetables){
        this.timetables = timetables;
    }

//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "group")
    @Transient
    public List<Replacement> getReplacements() {
        return replacements;
    }
    public void setReplacements(List<Replacement> replacements) {
        this.replacements = replacements;
    }

    @Transient
    public String getLabel(){
        return "CommonEntity";
    }

    @Transient
    public List<CommonEntity> getChildren(){
        return new ArrayList<>();
    }
}
