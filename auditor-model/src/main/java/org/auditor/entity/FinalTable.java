package org.auditor.entity;

import org.auditor.entity.security.Group;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Table(name="final_table")
@Entity
@Component
@Scope(value = "prototype")
public class FinalTable extends CommonEntity{

    private Long id;
    private Integer lesson;
    private Group group;
    private Teacher teacher;
    private Audience audience;
    private Integer priority;
    private Calendar calendar;


    public FinalTable(){}

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    @Column(name = "lesson")
    public Integer getLesson(){
        return lesson;
    }
    public void setLesson(Integer lesson){
        this.lesson = lesson;
    }

    @ManyToOne
    @JoinColumn(name = "group_id")
    public Group getGroup(){
        return group;
    }
    public void setGroup(Group group){
        this.group = group;
    }

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    public Teacher getTeacher(){
        return teacher;
    }
    public void setTeacher(Teacher teacher){
        this.teacher = teacher;
    }

    @ManyToOne
    @JoinColumn(name = "audience_id")
    public Audience getAudience(){
        return audience;
    }
    public void setAudience(Audience audience){
        this.audience = audience;
    }

    @Column(name = "priority")
    public Integer getPriority(){
        return priority;
    }
    public void setPriority(Integer priority){
        this.priority = priority;
    }

    @ManyToOne
    @JoinColumn(name = "calendar_id")
    public Calendar getCalendar(){
        return calendar;
    }
    public void setCalendar(Calendar calendar){
        this.calendar = calendar;
    }

}
