package org.auditor.entity;

import org.springframework.stereotype.Component;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author almagnit@gmail.com
 *
 */
@Component
public class CommonEntity implements Serializable {

    @Transient
    private Long id;
    @Transient
    private Integer version = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion(){
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Transient
    public String getKey(){
        return UUID.randomUUID().toString();
    }

    public void setKey(String key){}

    @Transient
    public String getLabel(){
        return "CommonEntity";
    }

    public void setLabel(String label){}

    @Transient
    public List<CommonEntity> getChildren(){
        return new ArrayList<>();
    }

    public void setChildren(List<CommonEntity> children){}

    @Transient
    public String getClassName(){
        return this.getClass().getName();
    }

    @Transient
    public String getClassTitle(){
        return "";
    }

    public boolean smartEquals(Object first, Object second) {
        boolean isEquals = false;
        if (first == null && second == null) {
            isEquals = true;
        } else if (first != null) {
            isEquals = first.equals(second);
        }

        return isEquals;
    }

}
