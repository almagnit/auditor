package org.auditor.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name="calendar")
@Entity
@Component
@Scope(value = "prototype")
public class Calendar extends CommonEntity{

    private Long id;
    private Date date;
    private List<FinalTable> finalTables = new ArrayList<FinalTable>();

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    @Column(name = "date")
    public Date getDate(){
        return date;
    }
    public void setDate(Date date){
        this.date = date;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "calendar", cascade=CascadeType.ALL, orphanRemoval = true)
    public List<FinalTable> getFinalTables(){
        return finalTables;
    }
    public void setFinalTables(List<FinalTable> finalTables){
        this.finalTables = finalTables;
    }
}
