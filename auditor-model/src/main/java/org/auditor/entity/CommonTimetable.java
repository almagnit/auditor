package org.auditor.entity;

import org.auditor.entity.security.Group;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CommonTimetable extends CommonEntity {

    protected Long id;
    protected Teacher teacher;
    protected Integer lesson;
    protected Integer day;
    protected String subject;
    protected Group group;
    protected Date date;

    public Long getId() {
        return id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public Teacher getTeacher(){
        return teacher;
    }
    public void setTeacher(Teacher teacher){
        this.teacher = teacher;
    }

    public Integer getLesson(){
        return lesson;
    }
    public void setLesson(Integer lesson){
        this.lesson = lesson;
    }

    public Integer getDay(){
        return day;
    }
    public void setDay(Integer day){
        this.day = day;
    }

    public String getSubject(){
        return subject;
    }
    public void setSubject(String subject){
        this.subject = subject;
    }

    public Group getGroup() {
        return group;
    }
    public void setGroup(Group group) {
        this.group = group;
    }

    public Date getDate(){
        return date;
    }
    public void setDate(Date date){
        this.date = date;
    }

    public String getLabel(){
        return "CommonEntity";
    }

    public List<CommonEntity> getChildren(){
        return new ArrayList<>();
    }

}
