package org.auditor.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by almagnit on 15.01.14.
 */


@Table(name="teachers")
@Entity
@Component
@Scope(value = "prototype")
public class Teacher extends CommonEntity {

    Long id;
    String name;

    Set<Priority> priorities = new HashSet<Priority>();
    List<Timetable> timetables = new ArrayList<Timetable>();
    List<Replacement> replacements = new ArrayList<Replacement>();

    @Column(name = "id")
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "teacher")
    @Transient
    public List<Timetable> getTimetables(){
        return timetables;
    }
    public void setTimetables(List<Timetable> timetables){
        this.timetables = timetables;
    }

//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "teacher")
    @Transient
    public List<Replacement> getReplacements() {
        return replacements;
    }
    public void setReplacements(List<Replacement> replacements) {
        this.replacements = replacements;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "priorityId.teacher", cascade=CascadeType.ALL)
    @OrderBy("priority asc ")
    public List<Priority> getPriorities() {
        return new ArrayList<>(priorities);
    }
    public void setPriorities(List<Priority> priorities) {
        this.priorities = new HashSet<>(priorities);
    }

    @Transient
    public String getLabel(){
        return getName();
    }

    @Transient
    public List<CommonEntity> getChildren(){
        return new ArrayList<>();
    }
}
