INSERT INTO authorities (username, authority) VALUES ('admin', 'ROLE_ADMIN');

INSERT INTO users (username, password, enabled) VALUES ('admin', 'admin', 1);

INSERT INTO audience (number_audience, name_audience) VALUES (1, 'Аудитория 1');
INSERT INTO audience (number_audience, name_audience) VALUES (2, 'Аудитория 2');
INSERT INTO audience (number_audience, name_audience) VALUES (3, 'Аудитория 3');
INSERT INTO audience (number_audience, name_audience) VALUES (4, 'Аудитория 4');
INSERT INTO audience (number_audience, name_audience) VALUES (5, 'Аудитория 5');
INSERT INTO audience (number_audience, name_audience) VALUES (6, 'Аудитория 6');
INSERT INTO audience (number_audience, name_audience) VALUES (7, 'Аудитория 7');
INSERT INTO audience (number_audience, name_audience) VALUES (8, 'Аудитория 8');
INSERT INTO audience (number_audience, name_audience) VALUES (9, 'Аудитория 9');
INSERT INTO audience (number_audience, name_audience) VALUES (10, 'Аудитория 10');
INSERT INTO audience (number_audience, name_audience) VALUES (11, 'Аудитория 11');
INSERT INTO audience (number_audience, name_audience) VALUES (12, 'Аудитория 12');
INSERT INTO audience (number_audience, name_audience) VALUES (13, 'Аудитория 13');
INSERT INTO audience (number_audience, name_audience) VALUES (14, 'Аудитория 14');
INSERT INTO audience (number_audience, name_audience) VALUES (15, 'Аудитория 15');
INSERT INTO audience (number_audience, name_audience) VALUES (16, 'Аудитория 16');
INSERT INTO audience (number_audience, name_audience) VALUES (17, 'Аудитория 17');
INSERT INTO audience (number_audience, name_audience) VALUES (18, 'Аудитория 18');
INSERT INTO audience (number_audience, name_audience) VALUES (19, 'Аудитория 19');
INSERT INTO audience (number_audience, name_audience) VALUES (20, 'Аудитория 20');
INSERT INTO audience (number_audience, name_audience) VALUES (21, 'Аудитория 21');
INSERT INTO audience (number_audience, name_audience) VALUES (22, 'Аудитория 22');
INSERT INTO audience (number_audience, name_audience) VALUES (23, 'Аудитория 23');
INSERT INTO audience (number_audience, name_audience) VALUES (24, 'Аудитория 24');
INSERT INTO audience (number_audience, name_audience) VALUES (25, 'Аудитория 25');
INSERT INTO audience (number_audience, name_audience) VALUES (26, 'Аудитория 26');
INSERT INTO audience (number_audience, name_audience) VALUES (27, 'Аудитория 27');
INSERT INTO audience (number_audience, name_audience) VALUES (28, 'Аудитория 28');
INSERT INTO audience (number_audience, name_audience) VALUES (29, 'Аудитория 29');
INSERT INTO audience (number_audience, name_audience) VALUES (30, 'Аудитория 30');
INSERT INTO audience (number_audience, name_audience) VALUES (31, 'Аудитория 31');
INSERT INTO audience (number_audience, name_audience) VALUES (32, 'Аудитория 32');
INSERT INTO audience (number_audience, name_audience) VALUES (33, 'Аудитория 33');
INSERT INTO audience (number_audience, name_audience) VALUES (34, 'Аудитория 34');
INSERT INTO audience (number_audience, name_audience) VALUES (35, 'Аудитория 35');
INSERT INTO audience (number_audience, name_audience) VALUES (36, 'Аудитория 36');
INSERT INTO audience (number_audience, name_audience) VALUES (37, 'Аудитория 37');
INSERT INTO audience (number_audience, name_audience) VALUES (38, 'Аудитория 38');
INSERT INTO audience (number_audience, name_audience) VALUES (39, 'Аудитория 39');
INSERT INTO audience (number_audience, name_audience) VALUES (40, 'Аудитория 40');

INSERT INTO priority (teacher_id, audience_id, priority) VALUES (1, 1, 1000);
INSERT INTO priority (teacher_id, audience_id, priority) VALUES (56, 1, 999);
INSERT INTO priority (teacher_id, audience_id, priority) VALUES (57, 2, 1000);
INSERT INTO priority (teacher_id, audience_id, priority) VALUES (58, 2, 999);
INSERT INTO priority (teacher_id, audience_id, priority) VALUES (59, 2, 998);

INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(51, 1, 1, 2, 'Англійська мова', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(52, 1, 2, 2, 'Англійська мова', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(53, 1, 3, 2, 'Англійська мова', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(54, 1, 1, 3, 'Англійська мова', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(55, 1, 2, 3, 'Англійська мова', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(56, 2, 1, 1, 'Історія України', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(57, 2, 2, 1, 'Історія України', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(58, 2, 3, 1, 'Історія України', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(59, 2, 2, 2, 'Історія України', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(60, 2, 3, 2, 'Історія України', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(61, 2, 1, 3, 'Основи патентознавства', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(62, 2, 2, 3, 'Основи патентознавства', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(63, 2, 3, 3, 'Історія України', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(64, 2, 2, 5, 'Історія України', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(65, 2, 3, 5, 'Історія України', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(66, 3, 1, 1, 'Архітектура компютерів', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(67, 3, 2, 1, 'Бази даних', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(68, 3, 3, 1, 'Бази даних', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(69, 3, 3, 2, 'Бази даних', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(70, 3, 1, 3, 'Бази даних', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(71, 3, 2, 3, 'Архітектура компютерів', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(72, 3, 3, 3, 'Роз-ка застос.кл-серв.архіт.', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(73, 3, 4, 3, 'Бази даних', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(74, 3, 1, 4, 'Роз-ка застос.кл-серв.архіт.', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(75, 3, 2, 4, 'Роз-ка застос.кл-серв.архіт.', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(76, 3, 3, 4, 'Архітектура компютерів', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(77, 3, 1, 5, 'Роз-ка застос.кл-серв.архіт.', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(78, 3, 2, 5, 'Бази даних', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(79, 3, 3, 5, 'Архітектура компютерів', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(80, 3, 4, 5, 'Роз-ка застос.кл-серв.архіт.', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(81, 4, 1, 1, 'Світова література', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(82, 4, 3, 2, 'Укр.мова за проф.спрям', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(83, 4, 2, 3, 'Укр.мова за проф.спрям', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(84, 4, 3, 4, 'Українська мова', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(85, 4, 3, 5, 'Українська мова', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(86, 19, 1, 1, 'Основи інформатики', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(87, 19, 2, 1, 'Астрономія', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(88, 19, 4, 1, 'Основи інформатики', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(89, 19, 1, 2, 'Основи інформатики', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(90, 19, 2, 2, 'Фізика', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(91, 19, 1, 3, 'Екологія', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(92, 19, 3, 3, 'Основи інформатики', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(93, 19, 1, 4, 'Прикладне програмне забезпечення', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(94, 19, 2, 4, 'Основи інформатики', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(95, 19, 3, 4, 'Основи інформатики', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(96, 19, 1, 5, 'Основи інформатики', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(97, 19, 2, 5, 'Фізика', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(98, 19, 4, 5, 'Основи інформатики', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(99, 20, 1, 1, 'Основи програмної інженерії', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(100, 20, 3, 1, 'Основи програмної інженерії', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(101, 20, 1, 2, 'Основи програмної інженерії', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(102, 20, 2, 2, 'Прикладне програмне забезпечення', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(103, 20, 3, 2, 'Математичний аналіз', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(104, 20, 4, 2, 'Математика', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(105, 20, 2, 3, 'Математика', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(106, 20, 1, 4, 'Математика', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(107, 20, 3, 4, 'Основи програмної інженерії', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(108, 22, 1, 1, 'Українська мова', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(109, 22, 2, 1, 'Українська література', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(110, 22, 3, 1, 'Українська мова', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(111, 22, 1, 2, 'Українська література', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(112, 22, 3, 2, 'Українська мова', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(113, 22, 1, 4, 'Українська література', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(114, 22, 2, 4, 'Українська література', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(115, 22, 3, 4, 'Українська література', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(116, 22, 4, 4, 'Українська література', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(117, 22, 1, 5, 'Українська література', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(118, 22, 2, 5, 'Українська література', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(119, 22, 3, 5, 'Українська література', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(120, 22, 4, 5, 'Українська мова', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(121, 23, 2, 1, 'Осн.констр і технологія вир', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(122, 23, 1, 2, 'Інженерна та комп графіка', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(123, 23, 2, 2, 'Комп’ютерна графіка', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(124, 23, 2, 4, 'Комп’ютерна графіка', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(125, 23, 3, 4, 'Контроль якості РЕА', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(126, 23, 1, 5, 'Комп’ютерна графіка', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(127, 23, 2, 5, 'Осн.констр і технологія вир', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(128, 23, 3, 5, 'Інженерна та комп графіка', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(129, 23, 4, 5, 'Комп’ютерна графіка', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(130, 24, 1, 1, 'Фізика', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(131, 24, 2, 1, 'Математика', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(132, 24, 3, 1, 'Фізика', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(133, 24, 1, 3, 'Астрономія', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(134, 24, 2, 3, 'Математика', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(135, 24, 3, 3, 'Фізика', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(136, 24, 2, 4, 'Фізика', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(137, 24, 3, 4, 'Астрономія', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(138, 24, 1, 5, 'Математика', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(139, 24, 2, 5, 'Математика', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(140, 24, 3, 5, 'Фізика', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(141, 25, 1, 1, 'Будова і обслуговування верстатів', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(142, 25, 2, 1, 'Верстати з ПУ і РТК', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(143, 25, 3, 1, 'ТКМ', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(144, 25, 1, 2, 'Будова і обслуговування верстатів', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(145, 25, 2, 2, 'Верстати з ПУ і РТК', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(146, 25, 3, 2, 'Основи технології машинобудування', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(147, 25, 2, 3, 'Будова і обслуговування верстатів', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(148, 25, 1, 4, 'Будова і обслуговування верстатів', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(149, 25, 2, 4, 'Основи технології машинобудування', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(150, 25, 3, 4, 'Будова і обслуговування верстатів', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(151, 25, 4, 4, 'ТКМ', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(152, 25, 1, 5, 'Верстати з ПУ і РТК', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(153, 25, 2, 5, 'ООМ та інструмент', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(154, 26, 2, 1, 'Світова література', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(155, 26, 1, 2, 'Світова література', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(156, 26, 2, 2, 'Світова література', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(157, 27, 4, 1, 'Економічний аналіз', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(158, 27, 1, 2, 'Менеджмент', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(159, 27, 2, 2, 'Економічна теорія', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(160, 27, 3, 2, 'Економічний аналіз', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(161, 27, 1, 3, 'Економічний аналіз', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(162, 27, 3, 3, 'Економічна теорія', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(163, 27, 1, 4, 'Маркетинг', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(164, 27, 2, 4, 'Економічний аналіз', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(165, 27, 3, 4, 'Менеджмент', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(166, 27, 1, 5, 'Маркетинг', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(167, 27, 2, 5, 'Економічна теорія', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(168, 28, 3, 1, 'Економіка промисловості', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(169, 28, 3, 2, 'Економіка,орг.і план.в-ва', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(170, 28, 3, 3, 'Економіка,орг.і план.в-ва', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(171, 28, 1, 5, 'Економіка,орг.і план.в-ва', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(172, 29, 1, 1, 'Організація комп’ютерних мереж', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(173, 29, 2, 1, 'Організація комп’ютерних мереж', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(174, 29, 3, 1, 'Основи інтернет', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(175, 29, 1, 2, 'Інформаційні технології', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(176, 29, 2, 2, 'ОС', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(177, 29, 3, 2, 'Організація комп’ютерних мереж', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(178, 29, 1, 3, 'ОС', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(179, 29, 3, 3, 'Інтернет-технології', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(180, 29, 1, 4, 'Організація комп’ютерних мереж', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(181, 29, 2, 4, 'ОС', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(182, 29, 4, 4, 'Інтернет-технології', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(183, 29, 1, 5, 'Будова та експлуат. ЕОМ', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(184, 29, 2, 5, 'Організація комп’ютерних мереж', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(185, 29, 3, 5, 'Організація комп’ютерних мереж', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(186, 29, 4, 5, 'ОС', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(187, 31, 1, 1, 'Нарисна геометрія', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(188, 31, 2, 1, 'Електропривод', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(189, 31, 3, 1, 'Будова систем ПУ', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(190, 31, 4, 1, 'Осн.дискретної автоматики', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(191, 31, 1, 2, 'Електропривод', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(192, 31, 2, 2, 'Комп’ютерна графіка', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(193, 31, 3, 2, 'Будова систем ПУ', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(194, 31, 1, 3, 'Електропривод', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(195, 31, 3, 3, 'Осн.дискретної автоматики', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(196, 31, 1, 4, 'Будова систем ПУ', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(197, 31, 2, 4, 'Комп’ютерна графіка', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(198, 31, 3, 4, 'Електропривод', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(199, 31, 1, 5, 'Комп’ютерна графіка', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(200, 31, 2, 5, 'Електропривод', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(201, 31, 3, 5, 'Інформаційні устрої верстатів', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(202, 31, 4, 5, 'Комп’ютерна графіка', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(203, 32, 1, 2, 'Біологія', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(204, 32, 2, 2, 'Біологія', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(205, 32, 1, 3, 'Біологія', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(206, 32, 2, 3, 'Біологія', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(207, 32, 2, 4, 'Хімія', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(208, 32, 2, 5, 'Хімія', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(209, 32, 3, 5, 'Хімія', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(210, 34, 1, 1, 'компютерна схемотехніка', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(211, 34, 1, 2, 'ЕОМ і мікропроцесори', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(212, 34, 2, 2, 'САПР', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(213, 34, 1, 3, 'ЕОМ і мікропроцесори', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(214, 34, 2, 3, 'САПР', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(215, 34, 2, 4, 'компютерна схемотехніка', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(216, 34, 2, 5, 'компютерна схемотехніка', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(217, 35, 1, 2, 'Вища математика', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(218, 35, 3, 2, 'Математика', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(219, 35, 1, 3, 'Дискретна математика', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(220, 35, 4, 3, 'Дискретна математика', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(221, 35, 1, 4, 'Дискретна математика', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(222, 35, 3, 4, 'Математика', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(223, 35, 1, 5, 'Математика', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(224, 35, 2, 5, 'Дискретна математика', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(225, 35, 3, 5, 'Вища математика', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(226, 38, 1, 1, 'Радіопередавальні пристрої', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(227, 38, 2, 1, 'Технологія в-ва РЕА', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(228, 38, 3, 1, 'Проектування радіопр. пристрої', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(229, 38, 4, 1, 'Сигнали та процеси в РТ', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(230, 38, 1, 2, 'Схемотехніка рад. пристроїв', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(231, 38, 2, 2, 'Радіопередавальні пристрої', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(232, 38, 1, 3, 'Схемотехніка рад. пристроїв', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(233, 38, 2, 3, 'Сигнали та процеси в РТ', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(234, 38, 3, 3, 'Проектування РЕА', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(235, 38, 1, 4, 'Основи ремонту РЕА', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(236, 38, 2, 4, 'Проектування РЕА', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(237, 38, 3, 4, 'Проектування радіопр. пристрої', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(238, 38, 1, 5, 'Технологія в-ва РЕА', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(239, 38, 2, 5, 'Проектування РЕА', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(240, 38, 3, 5, 'Технологія в-ва РЕА', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(241, 39, 1, 2, 'Англійська мова', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(242, 39, 2, 2, 'Англійська мова', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(243, 39, 3, 2, 'Англійська мова', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(244, 39, 1, 3, 'Англійська мова', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(245, 39, 2, 3, 'Англійська мова', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(246, 39, 3, 3, 'Англійська мова', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(247, 40, 3, 1, 'Обєктно орієн.програм', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(248, 40, 2, 3, 'Обєктно орієн.програм', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(249, 40, 3, 3, 'Обєктно орієн.програм', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(250, 40, 1, 4, 'Основи программування', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(251, 40, 2, 4, 'Обєктно орієн.програм', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(252, 40, 1, 5, 'Основи программування', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(253, 41, 1, 1, 'Фізичне виховання', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(254, 41, 2, 1, 'Фізичне виховання', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(255, 41, 1, 2, 'Фізкультура і здоров’я', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(256, 41, 3, 2, 'Фізичне виховання', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(257, 41, 1, 3, 'Фізичне виховання', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(258, 41, 3, 3, 'Фізкультура і здоров’я', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(259, 41, 1, 4, 'Фізкультура і здоров’я', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(260, 41, 2, 4, 'Фізичне виховання', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(261, 41, 3, 4, 'Фізкультура і здоров’я', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(262, 42, 1, 1, 'Фізичне виховання', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(263, 42, 3, 1, 'Захист вітчизни', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(264, 42, 2, 2, 'Фізичне виховання', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(265, 42, 2, 3, 'Захист вітчизни', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(266, 42, 1, 4, 'Захист вітчизни', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(267, 42, 3, 4, 'Фізичне виховання', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(268, 43, 1, 3, 'Англійська мова', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(269, 43, 2, 3, 'Англійська мова', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(270, 43, 3, 3, 'Англійська мова', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(271, 43, 2, 4, 'Англійська мова', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(272, 43, 3, 4, 'Англійська мова', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(273, 43, 2, 5, 'Англійська мова', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(274, 44, 1, 3, 'Промислова електроніка', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(275, 44, 3, 3, 'Основи теорії кіл', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(276, 44, 1, 4, 'Матеріалознавство', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(277, 44, 2, 4, 'Електронні прилади', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(278, 44, 3, 5, 'Теор.осн.електротехніки', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(279, 45, 2, 1, 'Фінансовий облік', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(280, 45, 3, 1, 'Податкова система', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(281, 45, 4, 1, 'Контроль і ревізія', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(282, 45, 1, 3, 'Податкова система', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(283, 45, 2, 3, 'Контроль і ревізія', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(284, 45, 3, 3, 'Фінансовий облік', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(285, 45, 4, 3, 'Контроль і ревізія', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(286, 45, 3, 5, 'Контроль і ревізія', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(287, 46, 2, 1, 'Основи телебачення', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(288, 46, 1, 5, 'Основи телебачення', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(289, 49, 1, 1, 'Українська література', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(290, 49, 2, 1, 'Українська література', 14);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(291, 49, 1, 3, 'Англійська мова', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(292, 49, 2, 3, 'Англійська мова', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(293, 49, 3, 3, 'Англійська мова', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(294, 49, 2, 4, 'Англійська мова', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(295, 49, 3, 4, 'Англійська мова', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(296, 49, 2, 5, 'Англійська мова', 17);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(297, 49, 3, 5, 'Українська література', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(298, 51, 1, 2, 'Інженерна та комп графіка', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(299, 51, 3, 5, 'Інженерна та комп графіка', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(300, 52, 1, 1, 'Облік в бюдж.орган.', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(301, 52, 2, 1, 'Інформаційні системи в обліку', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(302, 52, 3, 1, 'Інформаційні системи в обліку', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(303, 52, 1, 2, 'Основи аудиту', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(304, 52, 2, 2, 'Основи аудиту', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(305, 52, 3, 2, 'Інформаційні системи в обліку', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(306, 52, 1, 4, 'Облік в бюдж.орган.', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(307, 52, 3, 4, 'Інформаційні системи в обліку', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(308, 52, 1, 5, 'Основи аудиту', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(309, 52, 2, 5, 'Основи аудиту', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(310, 53, 1, 1, 'WEB-технології', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(311, 53, 2, 1, 'Конструювання програмного забезпечення', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(312, 53, 3, 1, 'Конструювання програмного забезпечення', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(314, 53, 1, 2, 'WEB-технології', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(315, 53, 2, 2, 'WEB-технології', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(316, 53, 3, 2, 'Конструювання програмного забезпечення', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(317, 53, 1, 3, 'WEB-дизайн', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(318, 53, 2, 3, 'WEB-дизайн', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(321, 53, 3, 3, 'Конструювання програмного забезпечення', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(322, 53, 1, 4, 'WEB-дизайн', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(323, 53, 2, 4, 'WEB-дизайн', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(324, 53, 3, 4, 'Конструювання програмного забезпечення', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(325, 53, 4, 4, 'WEB-технології', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(326, 53, 1, 5, 'WEB-технології', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(327, 53, 2, 5, 'WEB-технології', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(328, 53, 3, 5, 'Конструювання програмного забезпечення', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(329, 54, 2, 2, 'Основи автоматизаціївиробництва і ПР', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(330, 54, 1, 3, 'Основи автоматизаціївиробництва і ПР', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(331, 54, 2, 4, 'Технічна механіка', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(332, 54, 1, 5, 'Технічна механіка', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(333, 55, 1, 1, 'Соціологія', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(334, 55, 2, 1, 'Всесвітня історія', 15);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(335, 55, 3, 1, 'Всесвітня історія', 19);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(336, 55, 1, 2, 'Соціологія', 8);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(337, 55, 3, 2, 'Всесвітня історія', 5);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(338, 55, 1, 4, 'Соціологія', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(339, 55, 3, 4, 'Всесвітня історія', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(340, 55, 3, 5, 'Соціологія', 20);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(341, 56, 1, 1, 'Нарисна геометрія', 18);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(342, 56, 2, 1, 'Основи охорони праці', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(343, 56, 1, 2, 'Основи охорони праці', 4);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(344, 56, 2, 2, 'Основи охорони праці', 7);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(345, 56, 3, 2, 'Основи охорони праці', 12);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(346, 56, 2, 3, 'Безпека життєдіяльності', 13);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(347, 56, 3, 3, 'Основи охорони праці', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(348, 56, 2, 4, 'Основи охорони праці', 16);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(349, 56, 3, 5, 'МСП', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(350, 58, 2, 1, 'Ціноутворення', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(351, 58, 3, 1, 'Основи підприємництва', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(352, 58, 2, 2, 'Страхування', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(353, 58, 3, 2, 'РПС', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(354, 58, 1, 3, 'Страхування', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(355, 58, 2, 3, 'Фінанси підприємств', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(356, 58, 3, 3, 'Основи підприємництва', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(357, 58, 2, 4, 'Страхування', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(358, 58, 3, 4, 'Економіка підприємств', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(359, 58, 1, 5, 'Ціноутворення', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(360, 58, 2, 5, 'Страхування', 3);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(361, 58, 3, 5, 'Фінанси підприємств', 6);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(362, 59, 2, 1, 'Математика', 21);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(363, 59, 2, 2, 'Математика', 9);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(364, 59, 1, 3, 'Математика', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(365, 59, 1, 4, 'Математика', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(366, 59, 1, 5, 'Математика', 11);
INSERT INTO timetable(id, teacher_id, lesson, day, subject, group_id) VALUES
(367, 59, 2, 5, 'Вища математика', 9);

INSERT INTO replacement(id, teacher_id, lesson, date, subject, group_id) VALUES
(3, 59, 1, '2014-02-17', 'Математика', 11);

INSERT INTO training_group(id, group_name) VALUES
(3, '1БО-11');
INSERT INTO training_group(id, group_name) VALUES
(4, '1РПЗ-10');
INSERT INTO training_group(id, group_name) VALUES
(5, '1РПЗ-13');
INSERT INTO training_group(id, group_name) VALUES
(6, '2БО-11');
INSERT INTO training_group(id, group_name) VALUES
(7, '2РПЗ-10');
INSERT INTO training_group(id, group_name) VALUES
(8, '2РПЗ-13/11');
INSERT INTO training_group(id, group_name) VALUES
(9, 'БО-12');
INSERT INTO training_group(id, group_name) VALUES
(10, 'БО-12з/11');
INSERT INTO training_group(id, group_name) VALUES
(11, 'БО-13');
INSERT INTO training_group(id, group_name) VALUES
(12, 'КВОРП-10');
INSERT INTO training_group(id, group_name) VALUES
(13, 'КВОРП-11');
INSERT INTO training_group(id, group_name) VALUES
(14, 'КВОРП-12');
INSERT INTO training_group(id, group_name) VALUES
(15, 'КВОРП-13');
INSERT INTO training_group(id, group_name) VALUES
(16, 'ОВ-10');
INSERT INTO training_group(id, group_name) VALUES
(17, 'ОВ-11');
INSERT INTO training_group(id, group_name) VALUES
(18, 'ОВ-12');
INSERT INTO training_group(id, group_name) VALUES
(19, 'ОВ-13');
INSERT INTO training_group(id, group_name) VALUES
(20, 'РПЗ-11');
INSERT INTO training_group(id, group_name) VALUES
(21, 'РПЗ-12');
INSERT INTO training_group(id, group_name) VALUES
(22, 'РПЗ-13з/11');

INSERT INTO teachers(id, name) VALUES
(1, 'Іванова Т.В..');
INSERT INTO teachers(id, name) VALUES
(2, 'Іонова І.В.');
INSERT INTO teachers(id, name) VALUES
(3, 'Анікашин Д.І.');
INSERT INTO teachers(id, name) VALUES
(4, 'Бєлошенко К.В.');
INSERT INTO teachers(id, name) VALUES
(18, 'Бабенко Т.О.');
INSERT INTO teachers(id, name) VALUES
(19, 'Балабат О.В.');
INSERT INTO teachers(id, name) VALUES
(20, 'Бессарабова А.М.');
INSERT INTO teachers(id, name) VALUES
(21, 'Богданова О.М.');
INSERT INTO teachers(id, name) VALUES
(22, 'Васильєва О.Д.');
INSERT INTO teachers(id, name) VALUES
(23, 'Воронін М.Г.');
INSERT INTO teachers(id, name) VALUES
(24, 'Ворошилова В.І.');
INSERT INTO teachers(id, name) VALUES
(25, 'Гайтерова Л.А.');
INSERT INTO teachers(id, name) VALUES
(26, 'Горожеєва З.З.');
INSERT INTO teachers(id, name) VALUES
(27, 'Гусєва А.О.');
INSERT INTO teachers(id, name) VALUES
(28, 'Дерев''янко Л.В.');
INSERT INTO teachers(id, name) VALUES
(29, 'Зінченко К.Л.');
INSERT INTO teachers(id, name) VALUES
(30, 'ЗінченкоТ.О.');
INSERT INTO teachers(id, name) VALUES
(31, 'Кізілова І.С.');
INSERT INTO teachers(id, name) VALUES
(32, 'Ковальчук В.Ю.');
INSERT INTO teachers(id, name) VALUES
(33, 'Козел А.Д.');
INSERT INTO teachers(id, name) VALUES
(34, 'Козел Н.Б.');
INSERT INTO teachers(id, name) VALUES
(35, 'Кондратенко М.Є.');
INSERT INTO teachers(id, name) VALUES
(36, 'Корецький В.Д.');
INSERT INTO teachers(id, name) VALUES
(37, 'Кравченко А.М.');
INSERT INTO teachers(id, name) VALUES
(38, 'Курочкіна Г.П.');
INSERT INTO teachers(id, name) VALUES
(39, 'Лапицька М.Я.');
INSERT INTO teachers(id, name) VALUES
(40, 'Ларін О.В.');
INSERT INTO teachers(id, name) VALUES
(41, 'Матвєєв Р.В.');
INSERT INTO teachers(id, name) VALUES
(42, 'Матерновських П.А.');
INSERT INTO teachers(id, name) VALUES
(43, 'Моісеєнко Л.В.');
INSERT INTO teachers(id, name) VALUES
(44, 'Павленко В.С.');
INSERT INTO teachers(id, name) VALUES
(45, 'Пашевіна О.М.');
INSERT INTO teachers(id, name) VALUES
(46, 'Погорелов В.О.');
INSERT INTO teachers(id, name) VALUES
(47, 'Помазанов С.В.');
INSERT INTO teachers(id, name) VALUES
(48, 'Помазанова А.В');
INSERT INTO teachers(id, name) VALUES
(49, 'Радюхіна Т.');
INSERT INTO teachers(id, name) VALUES
(50, 'Ревенкова В.Ю.');
INSERT INTO teachers(id, name) VALUES
(51, 'Рефляк В.Г.');
INSERT INTO teachers(id, name) VALUES
(52, 'Рожко О.Л.');
INSERT INTO teachers(id, name) VALUES
(53, 'Ряднов С.П.');
INSERT INTO teachers(id, name) VALUES
(54, 'Сіверченко О.В.');
INSERT INTO teachers(id, name) VALUES
(55, 'Сіроусов О.К');
INSERT INTO teachers(id, name) VALUES
(56, 'Слєпцова Н.І');
INSERT INTO teachers(id, name) VALUES
(57, 'Стародубова Г.');
INSERT INTO teachers(id, name) VALUES
(58, 'Тактарова Ю.Є.');
INSERT INTO teachers(id, name) VALUES
(59, 'Шевченко Н.М.');
INSERT INTO teachers(id, name) VALUES
(60, 'Щербаков Л.С.');
