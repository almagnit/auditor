CREATE TABLE
  users
(
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  enabled  BIT DEFAULT 1
);

CREATE TABLE
  authorities
(
  username  VARCHAR(50) NOT NULL,
  authority VARCHAR(50)
);

CREATE TABLE training_group (
  id         BIGINT AUTO_INCREMENT PRIMARY KEY,
  group_name VARCHAR(50) COLLATE utf8_bin
);

CREATE TABLE group_members (
  group_id BIGINT NOT NULL,
  username VARCHAR(50));

CREATE TABLE group_authorities (
  group_id  BIGINT NOT NULL,
  authority VARCHAR(50));


CREATE TABLE acl_sid (
  id        BIGINT AUTO_INCREMENT PRIMARY KEY,
  principal BIT NOT NULL,
  sid       VARCHAR(500)
);

CREATE TABLE acl_class (
  id    BIGINT AUTO_INCREMENT PRIMARY KEY,
  class VARCHAR(500)
);

CREATE TABLE acl_object_AUTO_INCREMENT (
  id                       BIGINT AUTO_INCREMENT PRIMARY KEY,
  object_id_class          BIGINT NOT NULL,
  object_id_AUTO_INCREMENT BIGINT NOT NULL,
  parent_object            BIGINT,
  owner_sid                BIGINT NOT NULL,
  entries_inheriting       BIT
);

CREATE TABLE acl_entry (
  id                        BIGINT AUTO_INCREMENT PRIMARY KEY,
  acl_object_AUTO_INCREMENT BIGINT  NOT NULL,
  ace_order                 INT     NOT NULL,
  sid                       BIGINT  NOT NULL,
  mask                      INTEGER NOT NULL,
  granting                  BIT     NOT NULL,
  audit_success             BIT     NOT NULL,
  audit_failure             BIT
);

CREATE TABLE audience (
  id              BIGINT AUTO_INCREMENT PRIMARY KEY,
  number_audience INT(11)          NOT NULL,
  name_audience   VARCHAR(50)
                  COLLATE utf8_bin NOT NULL
);
CREATE TABLE priority (
  teacher_id      BIGINT,
  audience_id     BIGINT,
  priority        INT(11)          NOT NULL
);
CREATE TABLE timetable (
  id           BIGINT AUTO_INCREMENT PRIMARY KEY,
  teacher_id BIGINT,
  lesson       INT(11)          NOT NULL,
  day          INT(11)          NOT NULL,
  subject      VARCHAR(80)
               COLLATE utf8_bin NOT NULL,
  group_id BIGINT
);
CREATE TABLE teachers (
  id   BIGINT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(50)
       COLLATE utf8_bin NOT NULL
);

CREATE TABLE replacement (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  teacher_id BIGINT NOT NULL,
  lesson INT(11) NOT NULL,
  date DATE,
  subject VARCHAR(80)
          COLLATE utf8_bin NOT NULL,
  group_id BIGINT
);


CREATE TABLE final_table(
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  lesson INT(11) NOT NULL,
  group_id BIGINT NOT NULL,
  teacher_id BIGINT NOT NULL,
  audience_id BIGINT NOT NULL,
  priority INT(11) NOT NULL,
  calendar_id BIGINT
);

CREATE TABLE calendar (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  date DATE
)
