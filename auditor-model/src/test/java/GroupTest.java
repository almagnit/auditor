import org.auditor.dao.security.GroupDAO;
import org.auditor.entity.Timetable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(locations = {"classpath:/spring/spring-model.xml"})
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@RunWith(SpringJUnit4ClassRunner.class)
public class GroupTest {

    @Autowired
    GroupDAO groupDAO;

    @Test
    public void someTest(){

        for(Timetable timetable : groupDAO.list().get(0).getTimetables()){
            System.out.println("Урок " + timetable.getSubject());
        }

    }



}
