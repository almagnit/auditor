import org.auditor.dao.AudienceDAO;
import org.auditor.entity.Audience;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(locations = {"classpath:/spring/spring-model.xml"})
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@RunWith(SpringJUnit4ClassRunner.class)
public class AudienceTest {

    @Autowired
    AudienceDAO audienceDAO;

    @Test
    public void someTest(){
        for(Audience audience : audienceDAO.list()){
//            audienceDAO.delete(audience);
            System.out.println(audience.getNameAudience());
        }

    }

}
