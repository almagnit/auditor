import org.auditor.dao.AudienceDAO;
import org.auditor.dao.PriorityDAO;
import org.auditor.dao.TeacherDAO;
import org.auditor.entity.Priority;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(locations = {"classpath:/spring/spring-model.xml"})
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@RunWith(SpringJUnit4ClassRunner.class)
public class PriorityTest {

    @Autowired
    PriorityDAO priorityDAO;
    @Autowired
    TeacherDAO teacherDAO;
    @Autowired
    AudienceDAO audienceDAO;
    @Autowired
    SessionFactory sessionFactory;

    @Test
    public void listTest(){
        Priority p = priorityDAO.list().get(2);
        Priority priority = new Priority();
        priority.setTeacher(teacherDAO.list().get(7));
        priority.setPriority(111);
        priority.setAudience(p.getAudience());
        priorityDAO.add(priority);
    }

    @Test
    public void deleteTest(){
        Priority entity = priorityDAO.list().get(0);
        entity.getTeacher().getPriorities().remove(entity);
        entity.getAudience().getPriorities().remove(entity);
        sessionFactory.getCurrentSession().merge(entity.getTeacher());
        sessionFactory.getCurrentSession().merge(entity.getAudience());
        priorityDAO.delete(entity);
    }
}
