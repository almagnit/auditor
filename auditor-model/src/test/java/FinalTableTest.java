import org.auditor.GeneratorFinalTable;
import org.auditor.dao.*;
import org.auditor.entity.Calendar;
import org.auditor.entity.FinalTable;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@ContextConfiguration(locations = {"classpath:/spring/spring-model.xml"})
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional(value = "transactionManager", propagation= Propagation.REQUIRED)
@RunWith(SpringJUnit4ClassRunner.class)
public class FinalTableTest {

    @Autowired
    AudienceDAO audienceDAO;

    @Autowired
    PriorityDAO priorityDAO;

    @Autowired
    TimetableDAO timetableDAO;

    @Autowired
    ReplacementDAO replacementDAO;

    @Autowired
    FinalTableDAO finalTableDAO;

    @Autowired
    CalendarDAO calendarDAO;

    @Test
    public void someTest(){


        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String dateInString = "7-03-2014";
        Date date = null;

        try {
            date = formatter.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println(date);


        Calendar calendar = new Calendar();
        calendar.setDate(date);


        boolean flag = false;

        if(calendarDAO.list().size() == 0) {
            calendarDAO.add(calendar);
            flag = true;
        }

        System.out.println(calendarDAO.list().get(0).getDate());

        GeneratorFinalTable finalTable = new GeneratorFinalTable(priorityDAO, timetableDAO, audienceDAO, replacementDAO, calendarDAO.list().get(0).getDate());

        finalTable.setCalendar(calendarDAO.list().get(0));


        finalTable.alignmentAudience();

        boolean flag1 = false;
        if(finalTableDAO.list().size() == 0){
            flag1 = true;
        }

        for (FinalTable finalTableBean : finalTable.getFinalTables()){
            System.out.println(finalTableBean.getTeacher().getId()+ " " +
                finalTableBean.getLesson()+ " " +
                finalTableBean.getAudience().getNumberAudience() + " " +
                finalTableBean.getGroup().getGroupName() + " " +
                finalTableBean.getPriority() + " " +
                finalTableBean.getCalendar().getDate()
            );
            if(flag1 == true)
                finalTableDAO.add(finalTableBean);
        }

    }


}
